# Remove system library from Kokkos::LIBDL. This is a hack that was necessary
# to compile on macOS with MacPorts-provided installations of Kokkos and
# clang-17. Otherwise the injected system header search path was used before
# the header search paths for clang-17, such that the wrong C headers were
# found eventually.
get_property(libdl_dirs TARGET Kokkos::LIBDL PROPERTY INTERFACE_INCLUDE_DIRECTORIES)
foreach(dir ${libdl_dirs})
  if(dir MATCHES "/Library/Developer/CommandLineTools/SDKs/.*.sdk/usr/include")
    message(STATUS "`${dir}` is found in the Kokkos-provided header search paths."
      " Will remove to ensure that this does not take precedence over the"
      " standard compiler toolchain header search paths.")
  else()
    list(APPEND new_libdl_dirs "${dir}")
  endif()
endforeach()
set_property(TARGET Kokkos::LIBDL PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${new_libdl_dirs}")
