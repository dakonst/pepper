set(VCL_ENABLED FALSE)

# NOTE: For now there is a compilation issue when trying to compile with VCL
# support. This is specific to the main/Kokkos variant of Pepper.
message(STATUS "This build will not use VCL support (currently only available in the \"native\" variant).")
return()

if (NOT PEPPER_VCL_DISABLED)

  if (CUDA_FOUND)

    message(STATUS "This build will not use vector instructions (CUDA is used instead).")

  elseif(CMAKE_SYSTEM_PROCESSOR MATCHES .*arm.*)

    # NOTE: to use vector instructions on arm architectures, the VCL manual
    # suggests to use the Highway library at https://github.com/google/highway
    message(STATUS "This build will not use vector instructions (not supported for ${CMAKE_SYSTEM_PROCESSOR}).")

  else()

    set(VCL_ENABLED TRUE)
    message(STATUS "This build will use vector instructions.")

    # make VCL available
    FetchContent_Declare(
      vcl
      GIT_REPOSITORY https://github.com/vectorclass/version2.git
      GIT_TAG v2.02.01
      )
    FetchContent_MakeAvailable(vcl)
    include_directories(BEFORE SYSTEM "${vcl_SOURCE_DIR}")

  endif()

else()

  message(STATUS "This build will not use VCL support (disabled by user).")

endif()
