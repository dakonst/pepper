FetchContent_Declare(
  mini
  GIT_REPOSITORY https://github.com/pulzed/mINI.git
  GIT_TAG 0.9.14
  )
FetchContent_MakeAvailable(mini)
include_directories(BEFORE SYSTEM "${mini_SOURCE_DIR}/src")
