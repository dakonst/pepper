# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Fixed

- [main] Always disable VCL support, since trying to enable it currently causes
  compilation issues.

## [1.1.1-kokkos] [1.1.1-native] - 2024-06-06

### Added

- A manual guide on which processes are supported by Pepper,
  and how to select them.

### Fixed

- [main] Fixed a compiler error when using Kokkos' CUDA device support.

## [1.1.0-kokkos] [1.1.0-native] - 2024-05-28

Version 1.1.0 comes in a portable main Kokkos-based variant
and a "native" variant
that only supports serial execution on CPU
and parallel execution on CUDA devices using native CUDA bindings.
We prefix items in the added/fixed/changed lists below with "[main]" or
"[native]", if they are only relevant for the respective variant.

### Added

- Add the option to bias the phase space e.g. to create more events in the
  tails of distributions. This can be enabled using the `enhance_function`
  option in the `[phase_space]` settings. Possible values are `Zpt`,
  `Max(Htp2,Zpt2)` and `Max(Htp2,ttpt2)`. Phase-space biasing is currently
  only supported by the `Chili(basic)` phase-space generator.
- Add basic support to use Pepper as a library, with API to calculate matrix
  elements. So far this is a working proof-of-principle, but is not yet
  optimised for speed etc. Later releases will expand on this, e.g. to allow
  using Pepper for on-device training of neural networks etc.
- An HDF5 phase space file reader has been added, this can be enabled by
  specifying the `points_file` option in the `[phase_space]` settings.
- The helicity configuration can now be fixed using the `fixed_helicities`
  option in the `[main]` settings section. This option was previously in the
  `[dev]` section, and not very useful for users because the helicities were
  assigned in an unexpected way. This is not rectified and the option should
  behave as expected.
- More details have been added to the "Getting started" tutorials.
- More details have been added to the "Running Pepper for the first time"
  tutorial (previously called "Create your first event sample").
- [main] Pepper's configuration now prints additional helpful details when it
  fails because Kokkos is not found.

### Fixed

- The detection of the LHAPDF package has been improved.
- Compilations and runtime issues when LHAPDF was not found have been fixed.
- Tests can now be run without recalculating colour factors when Pepper
  has not been installed; this is achieved by falling back to the colour factor
  data files in the build directory if they have not been installed yet.
- Fix parsing of process strings that are not purely made of jets.
- Fix compilation error when using CUDA and external Chili in the native
  variant.
- Fix weight handling if phase-space is generated on the host, but the PDF are
  evaluated on the device.
- Fix configuration issue when certain CMake variables ended up being empty.
- Fix phase space compilation if HDF5 is not found.
- Improve finding the right python executable to be used for running tests.
- Fix a CMake check for availability of an MPI library; this might fix CMake
  configuration in situations where MPI is not found (but not explicitly
  disabled).
- Fix configurations on CMake versions before v3.24.
- HighFive headers are not erroneously installed any longer.
- Fix small issues in shell snippets listed in the Getting Started tutorials in
  the manual, and streamline these tutorials.
- Improve portability of finding the Thrust dependency for the "native"
  variant.

### Changed

- Reduce LHAPDF version requirement from 6.5.4 to 6.5.

## [1.0.1-kokkos] [1.0.1-native] - 2023-11-26

Version 1.0.1 comes in a portable main Kokkos-based variant
and a "native" variant
that only supports serial execution on CPU
and parallel execution on CUDA devices using native CUDA bindings.

### Added

- Correct Pepper release paper Inspire TexKey as used in the
  `pepper_citations.tex` file written out after a run
- Summary of the most important switches after the configuration
- New logo
- Detailed build instructions and bootstrap scripts; this was in particular
  lacking before for the Kokkos variant

### Fixed

- Remove accidental writeout of a `vegas.txt` file into the working directory
- Fix typos in manpage and command line help output
- Add missing colour factors in the downloaded process data

### Changed

- Simplify process specification (#34)
    - The `process_file` setting is deprecated. It can still be used, but is
      now equivalent to the process setting. This also means that process
      files can now also be specified on the cmd line using `-p|--process`.
    - It is still possible to directly specify the process filename, e.g.
      "z2j.csv". However, it is now also possible, and preferred, to give a
      specification that follows the [OpenLoops conventions](https://openloops.hepforge.org/process_library_ol.php?repo=public&version=2). For the above
      example, this would be "ppzjj", which is then parsed and transformed
      to the correct Pepper filename before trying to read the file.
    - There is no longer a default process.
- Change default of `phasespace.optimisation.n_steps` to 7 and
  `phasespace.integration.n_nonzero_min` to 45000
- Use CMake's FetchContent method instead of git submodules to fetch external
  codes during the configuration (#36). This allows us to generate working tarballs.

## [1.0.0-kokkos] [1.0.0-native] - 2023-11-13

Version 1.0.0 comes in a portable main Kokkos-based variant
and a "native" variant
that only supports serial execution on CPU
and parallel execution on CUDA devices using native CUDA bindings.

### Added

- Citation file (CITATIONS.cff) and command line flag `--cite` to print it (#18)
- Writeout of a `pepper_citations.tex` file that can be uploaded to
  [bibliography-generator](https://inspirehep.net/bibliography-generator) to
  generate a bibliography for the given setup
- `H_TM^2/2` and `m_t^2` scale setters
- Max epsilon setting (!104)
- Output of remaining time (#29)
- Support enabling clang tidy
- Add run stats to hdf5 writeout (!111)
- Timers for event filtering and pulling
- Copyright notices, acknowledgements, GPLv3 license and MCnet guidelines

### Fixed

- Properly sync VEGAS grids for t-channel integrator "Chili(mild)" across MPI
  ranks
- On-device performance when using the t-channel integrator "Chili(mild)" (#17)
- Colour factor generation via FORM when using MPI (#13)
- Bug when CuPDF.h is found while CUDA itself is not found
- Bug in colour factor generation for `g g -> t tb g g g g g` (!94)
- Wrong HDF5 database size calculation
- Flavour selection when some partonic fluxes are zero (#23)
- Scale writeout when generating events on device
- Compilation with Thrust v2.0.1 (#26)
- Race condition in on-device production of Drell-Yan events (#24)
- Fix helicity survey dependence on the phase space point (3929facc7)
- Integer overflow for optimisation/integration phases with large nonzero event
  numbers (approx. > 2 billion)
- Distribution of no. of trials over the nonzero events that are written out
  (this is now done statistically) (#32)

### Removed

- Obsolete GPU workflow topic guide (#11)
- Unused `selection_weights_ignored` setting

### Changed

- The short-form command line options -c|-C are not longer associated with the
  long-form --color|--no-color flags.
- Synchronise particle properties with the used PDF set
- Event generation output is now updated instead of appended (if colored output
  is enabled) (#29)

### Performance

- Improve performance when using HDF5 and on-device event generation (!90)
- Improve t-channel integrator "Chili(mild)" by using smarter variable offsets (!109)


## [0.0.3] - 2023-09-28

This is the first version which is fully usable to generate complete
input for Sherpa and Pythia. It should therefore be close to a public
release now, barring of course some optimisation improvements and
bugfixes that might become necessary towards working to the release.

### Added

- Support for using host-only Chili when otherwise running on device
- Beam energy setting
- Compatibility of HDF5 output with the Sherpa reader
- Full PDF and beam information in HepMC3 output
- Cross section information in HDF5 output (in the "/generatedResult" field)
- LHEF event output
- Leading colour configuration information in event output
- VEGAS-optimised t-channel (plus one s-channel) integrator "Chili(mild)" with
  CUDA support
- Support for CUDA LHAPDF
- Environment variable PEPPER_DEVICE_ID for setting the CUDA device used
- Nested internal timing diagnostics
- H_Tp^2/2 and H_T^2/2 scale definitions
- Manual guide on reusing cached results
- Manual tutorial "Getting started"
- Manual reference on Runcard options

### Fixed

- Do MPI sum before updating helicity selection weights
- Enable Chili max eta cuts for jets
- Only stop optimisation/integration when all processes have been sampled at
  least once
- Fix read-in of selection weights
- Fix too low output precision of cached results
- Fix bug when returning a zero standard deviation/variance if the number of
  trials is one
- Fix non positive definite standard deviation (and hence selection weights)
- Use correct scale information in HDF5 output
- Improve heuristics to set beams in HDF5 output
- Increase FORM max. term number to fix colour factor generation bug
- Fix moving FORM-generated files to a across filesystem
- Fix crash in HDF5 output for weighted events
- Fill dummy cross sections for auxiliary weights to suppress new HepMC3
  warnings
- Fix integer overflow bug when MPI summing MC event counters for many ranks
- Remove dummy-event zero counting in HDF5 output, which broke the Sherpa
  readin
- Fix crash in HDF5 output when all events are zero
- Correct reported LHEH5 version string from 2.0.0 to 2.0.1
- Fix GPU device selection for MPI use
- Fix accidental correlation in the random flavour channel selection
- Fill correct scales and couplings in various output formats

### Changed

- Scale min. number of nonzero events per optimisation/integration step with
  the number of helicity configuration and divide by the number of MPI ranks
  for easier usage
- Make CMake configuration output a bit more verbose
- Improve output when zero events are requested

### Performance

- Add CPU vector instructions for some vertices
- Improve performance of resetting particle information in the recursion
- Remove unnecessary D->H copies of particle information
- Remove redundant helicity selection weight updates
- Make H->D copy of random numbers for FORCE_HOST_RNG=1 faster
- Evaluate Z/photon currents simultaneously
- Improve performance of the momentum storage handling in the Chili interface
- Use minimal storage for matrix elements
- Cache the non-zero helicity configuration, which speeds up initialisation of
  subsequent runs in particular on the device

## [0.0.2] - 2023-03-29

This is the first version capable of dynamic scale setting, which should
allow us to generate practically useful results now.

Important bugfixes include the calculation of phi (which affected the
application of cuts and thus physics results), fixing the unweighting
for processes with more than process flavour group, and properly
enabling the cache of Chili results (which also includes a fix on the
Chili side).

## [0.0.1] - 2023-03-11

This is the first version capable of generating physical (parton-level)
LHC events, with the last building block being the proper symmetrisation
of the initial state.

[Unreleased]: https://gitlab.com/spice-mc/pepper/-/compare/1.0.1-kokkos...main
[1.1.1-kokkos]: https://gitlab.com/spice-mc/pepper/-/compare/1.1.0-kokkos...1.1.1-kokkos
[1.1.1-native]: https://gitlab.com/spice-mc/pepper/-/compare/1.1.0-native...1.1.1-native
[1.1.0-kokkos]: https://gitlab.com/spice-mc/pepper/-/compare/1.0.1-kokkos...1.1.0-kokkos
[1.1.0-native]: https://gitlab.com/spice-mc/pepper/-/compare/1.0.1-native...1.1.0-native
[1.0.1-kokkos]: https://gitlab.com/spice-mc/pepper/-/compare/1.0.0-kokkos...1.0.1-kokkos
[1.0.1-native]: https://gitlab.com/spice-mc/pepper/-/compare/1.0.0-native...1.0.1-native
[1.0.0-kokkos]: https://gitlab.com/spice-mc/pepper/-/compare/0.0.3...1.0.0-kokkos
[1.0.0-native]: https://gitlab.com/spice-mc/pepper/-/compare/0.0.3...1.0.0-native
[0.0.3]: https://gitlab.com/spice-mc/pepper/-/compare/0.0.2...0.0.3
[0.0.2]: https://gitlab.com/spice-mc/pepper/-/compare/0.0.1...0.0.2
[0.0.1]: https://gitlab.com/spice-mc/pepper/-/compare/0.0.0...0.0.1
