pepper(1) -- efficiently generate simulated parton-level events
===============================================================

## SYNOPSIS

`pepper` [-h | -v | --cite] [-q | -V | -d] [--color | --no-color]
         [-pbn] [-D <uid>] [-s <seed>] [-e <collision_energy>]
         [-S[O <helicity_summing_options>]]
         [-zwc] [-Q[l] | -o <event_output_file>]
         [-P <phasespace_generator>]
         [<ini-file>]

## DESCRIPTION

pepper(1) is a portable program that generates parton-level events at a
high-energy physics collider such as the LHC.

## OPTIONS

### POSITIONAL OPTIONS

* <ini-file>:
  Use the options given in <ini-file>
  It must use pepper(1)'s ini file format.
  Options given on the command line take higher precedence,
  overriding corresponding options in <ini-file>
  Defaults to "pepper.ini", if it exists.

### MAIN OPTIONS

* `-h`, `--help`:
  Display short help message and exit.

* `-h`, `--version`:
  Display version information and exit.

* `-p`, `--process` <process>:
  Generate events for the given <process>. This can be a single partonic
  channel, e.g. "d u -> d u", a comma-separated list of partonic channels, e.g.
  "d u -> d u, d c -> d c", or a process specification, e.g. "ppjj" or "ppzjjj".

* `-b`, `--batch-size` <batch_size>:
  Set the number of events per batch.
  This is only relevant for tuning the performance,
  especially on a device with a high degree of parallelism (e.g. GPU).
  Defaults to "1".

* `-n`, `--n-batches` <number_of_batches>:
  Set the number of event batches to generate.
  pepper(1) generates <batch_size> times <number_of_batches> trial events.
  The number of non-zero events depends on the phase-space efficiency
  and the unweighting efficiency.
  Defaults to "1".

* `-q`, `--quiet`:
  Set quiet mode, suppressing most output.
  
* `-V`, `--verbose`:
  Set verbose mode, displaying additional non-essential information.

* `-d`, `--debug`:
  Set debug mode, displaying additional non-essential and debugging information.

* `--no-color`:
  Always disable the using colors in the output.

* `--color`:
  Always enable the using colors in the output.

* `-D`, `--device` <device_uid>:
  Select the GPU with the given <device_uid>.
  This is ignored when pepper(1) is configured without GPU support.
  Defaults to "0".

* `-s`, `--seed` <seed>:
  Set the random seed for the Monte-Carlo simulation.

* `-e`, `--collision-energy` <energy>:
  Set the collision energy in GeV. Each incoming beam/particle
  will carry half of the given value.

* `-S`, `--helicity-summing`:
  Use helicity summing instead of helicity sampling.

* `-O`, `--helicity-summing-options` <options>:
  Set options for helicity summing.
  This is ignored if `--helicity-summing` is not used.
  <options> can be the letters "c", "p" or a combination of both.
  Using "c" enabled cached helicity summing.
  Using "p" enables using parity symmetry; this is only valid
  for processes that preserve this symmetry,
  and will be ignored for processes that do not preserve it.
  Defaults to "".

### EVENT GENERATION OPTIONS

* `-Q`, `--output-disabled`:
  Disable event output.

* `-l`, `--lhef`:
  Use LHEF instead of HepMC3 formatted event output. This is ignored when
  specifying `-o|--output <file>`. In this case, the used format depends on the
  extension of `<file>`.

* `-o`, `--output` <file>:
  Set event output path. The output format is automatically set to match the
  file extension. When `-o pepper_events.hepmc3.gz` is used, pepper(1) will
  write events in the (compressed) HepMC3 format. The allowed extensions are
  `.hepmc3[.gz]`, `.lhef[.gz]`, `.hdf5` and `.debug`. For some formats,
  pepper(1) must have been configured to support them.

* `-z`, `--zero-weight-output-disabled`:
  Disable outputting events with a zero weight.

* `-w`, `--unweighting-disabled`:
  Disable unweighting events prior to outputting them.

* `-c`, `--leading-colour-output-disabled`:
  Disable leading colour flow information output.

### PHASESPACE OPTIONS

* `-P`, `--psi` <phasespace_integrator>:
  Select phasespace integrator.
  Defaults to "Rambo".

## EXAMPLES

Generate 1000 gluon scattering trial events:

      $ pepper -p "g g -> g g" -n 1000

## SEE ALSO

* Sherpa(1) 

## ENVIRONMENT

The following environment variables affect the execution of pepper(1):

* NO_COLOR | BG2_NO_COLOR:
  If either of these is set to "1", pepper(1) will not use color in its output,
  unless explicitly told so by the use of the `--color` option.
* TERM:
  If this is set to "dumb", pepper(1) will not use color in its output,
  unless explicitly told so by the use of the `--color` option.
* PEPPER_DATA_PATH:
  The search path used by pepper(1) to find colour factor files.

## AUTHOR

* Enrico Bothmann <enrico.bothmann@uni-goettingen.de>
* Max Knobbe <max.knobbe@uni-goettingen.de>
* Taylor Childers
* Joshua Isaacson
* Stefan Höche
* Walter Giele

## REPORTING BUGS

* [GitLab issues](https://gitlab.com/spice-mc/pepper/-/issues)
