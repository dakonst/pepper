# Processes

Before discussing the physics processes currently supported by Pepper,
we stress that all processes are currently available at leading order only.

## LHC processes

Pepper's focus is on those physics processes at the LHC
which have high cross sections
and appear as backgrounds in almost all physics analyses.
Therefore, on the simulation side,
large high-fidelity samples are required for these processes,
and their production takes up most of the event generation computing resources.
Currently, Pepper can generate events for the following processes:

| Process                     | Base specifier | Max. additional jets | $\alpha_{\text{ew}}$ order | $\alpha_\text{s}$ order |
| --------------------------- | -------------- | -------------------- | -------- | ---------- |
| Drell-Yan production        | ppee           | 5 (= ppeejjjjj)      | 2        | 0$\ldots$5 |
| Top-antitop pair production | pptt           | 4 (= ppttjjjj)       | 0        | 2$\ldots$6 |
| Dijet production            | ppjj           | 4 (= ppjjjjjj)       | 0        | 2$\ldots$6 |

On request, we can consider to add support for additional processes.
In particular W boson production is a process that we can easily add on-demand.

The "Base specifier" gives the corresponding value one can use to select
the process using the `[main]` section
of the runcard (see [](../reference/runcard_options_main.md)),
or by the `-p|--process` command line option
(see [](../reference/cmd_line_interface.md)).

Additional `j` characters can be appended to add additional final-state jets.
The current maximum number of jets are given in the table above.
This restriction is purely technical.
Please contact us if you need to generate events with higher jet multiplicities,
and we will try to add the required process data files.

```{note}
Jets are understood to be QCD-only jets;
as Pepper is a parton-level event generator, this simply means
that a jet is an (anti-)quark or a gluon which passes all the jet cuts.
In particular, photons are not included in the jet definition.
```

As an example, to select Drell-Yan production
in association with 4 additional jets,
add the line `process = ppeejjjj` in the `[main]` section of the runcard,
or use `pepper --process ppeejjjj` on the command line.

## Partonic channels

All of the above assumes that you want to generate events
for physical processes,
for which all contributing partonic channels are summed over.

If you need to generate events for a single partonic channel instead,
you can specify this by giving a whitespace-separated list of particles
using `->` to separate initial- and final-state particles.
An example would be `d db -> u ub`. More than one
channel can be specified by giving them as a comma-separated list,
e.g. `d db -> u ub, d db -> c cb, ...`.
As whitespace is involved,
you need to use quotation marks on the command line,
e.g. `pepper --process "d db -> u ub"`.

## Partonic channel files

No contents yet.
