# Introduction

Pepper
(**P**ortable **E**ngine for the **P**roduction of **P**arton-level **E**vent **R**ecords)
is an efficient parton-level event generator
to simulate high-energy physics at colliders such as the LHC.
It is scalable from a laptop
to a Leadership Class HPC Facility.
Pepper's mascot is a racing honey bee,
because both Pepper and a bee hive embrace parallelism and efficiency.

```{admonition} The Kokkos and native variants of Pepper
Pepper versions come in two variants.
The main or Kokkos variant uses [Kokkos](https://github.com/kokkos/kokkos)
to provide cross-platform support with parallel algorithm implementations.
The native variant allows to either compile natively for running serially on a CPU,
or to compile using the Nvidia CUDA API for supported GPU.
Below you can find tutorials for getting started for both variants.
```

```{tableofcontents}
```
