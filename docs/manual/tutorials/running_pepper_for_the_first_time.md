# Running Pepper for the first time

If you followed one of the Getting Started tutorials,
`<prefix>/bin` is now in your shell's `PATH` environment variable,
where `<prefix>` denotes the path
to the installation directory used for Pepper.

In that case, your shell can find the `pepper` command.
You can confirm this by displaying Pepper's help:
```bash
pepper -h
```

If you have not set up your `PATH`,
you must specify the path to the `pepper` command:
```bash
<prefix>/pepper -h
```

Command line arguments and/or a settings file can be used
to specify event generation parameters,
i.e. which process to simulate, which collision energy to use etc.
It is usually a good idea to use a settings file (also called runcard)
for the baseline settings to use, and to use command line arguments only
to override individual settings.
Pepper will automatically look for a runcard
with the filename `pepper.ini` in the current working directory,
but you can also specify a custom runcard path:
```bash
pepper <runcard_path>
```

For your first run, create an empty directory
and copy one of Pepper's example runcards as a starting point:
```bash
mkdir my_first_pepper_run
cd my_first_pepper_run
cp <prefix>/share/pepper/examples/pp_jj.ini pepper.ini
```
This is a runcard to simulate dijet production at the LHC.
If you have a look inside the file,
you will only find a few setting lines.
Indeed, Pepper will rely on many physical and technical default settings.
To find out more,
you can inspect `<prefix>/share/pepper/examples/defaults.ini`.
In this example runcard,
all settings are specified,
and explicitly set to their respective defaults.

If run for the first time for a given setup,
Pepper must first do various optimizations
to prepare for the actual event generation.
The results of this optimization are written
into the subdirectory `pepper_cache`,
such that the optimization can be reused
in subsequent runs.

```{admonition} Keeping event generation directories separate
Pepper uses different caches for different processes,
but otherwise it is the responsibility of the user
to make sure that a given cache is still valid
after changing event generation settings.
In doubt you might want to remove cached results first
to regenerate them in the next run,
or use a different working directory altoether.
For more detailed information,
see [](../guides/reusing_cached_results.md).
```

Let's run the optimization,
but skip event generation for now
by requesting zero event batches (`-n 0`):
```bash
pepper -n 0
```
The output should be similar to the following:
```{code-block}
:emphasize-lines: 4,7,9,14,15,37
   \\//   _ _
  (°)))>- _-_-   Pepper v1.0.1 (2e8d565)
    ```
Determine paths ...
Runcard: "pepper.ini"
Data: "<prefix>/share/pepper"
Prepare parallel execution ...
None (single-threaded execution on CPU)
Initialise processes and event generation ...
Process: [ppjj]
PDF: NNPDF30_nlo_as_0118/0
 -> t mass = 173.07
 -> Z mass = 91.2
Run helicity configuration optimisation ...
Optimise event sampling ...
Step 1/7: 1.24905e+08 +- 1.49473e+06 pb (1.19669 %)
300019 of 382100 events are non-zero (78.5185 %)
0s elapsed, about 10s left
Step 2/7: 1.26256e+08 +- 725303 pb (0.574472 %)
732108 of 854400 events are non-zero (85.6868 %)
0s elapsed, about 8s left
Step 3/7: 1.25959e+08 +- 441795 pb (0.350746 %)
1354253 of 1506700 events are non-zero (89.8821 %)
1s elapsed, about 7s left
Step 4/7: 1.26563e+08 +- 296133 pb (0.23398 %)
2250130 of 2436300 events are non-zero (92.3585 %)
2s elapsed, about 6s left
Step 5/7: 1.2709e+08 +- 210056 pb (0.165281 %)
3540155 of 3770500 events are non-zero (93.8909 %)
4s elapsed, about 5s left
Step 6/7: 1.27613e+08 +- 155363 pb (0.121745 %)
5397765 of 5687900 events are non-zero (94.8991 %)
6s elapsed, about 3s left
Step 7/7: 1.28007e+08 +- 118417 pb (0.0925082 %)
8072689 of 8446600 events are non-zero (95.5732 %)
9s elapsed
Optimise event unweighting ...
Step 1/3: 1.28265e+08 +- 66734.3 pb (0.0520285 %)
2700043 of 2775900 events are non-zero (97.2673 %)
3s elapsed, about 7s left
Step 2/3: 1.2842e+08 +- 47225.3 pb (0.0367742 %)
5400113 of 5551300 events are non-zero (97.2765 %)
7s elapsed, about 3s left
Step 3/3: 1.28407e+08 +- 38560.2 pb (0.0300298 %)
8100128 of 8326500 events are non-zero (97.2813 %)
10s elapsed
Skipping event generation: no events requested
```
Note that we have highlighted the status lines
of the output that denote the various run stages.
First, Pepper prints its cute bee banner, along with versioning information.
Then it prints the various input files and paths it has used for this run.
After that, parallel execution information is printed
(here, we just run on a single CPU thread, so it prints "None").
This is followed by some basic setup information, such as the process generated
and the PDF used
(along with any particle masses taken from the PDF information).
Finally, we go through three optimization stages.
The first one determines which non-zero helicity configurations exist.
The second one optimises the event sampling.
And the third and last one optimises the event unweighting.
The event sampling and unweighting optimisations go through a number of steps.
For each one, the current cross section result is printed.

Let's generate some events next.
Run
```bash
pepper -n 100 -Q
```
to generate 100 event batches:
```{code-block}
:emphasize-lines: 4,7,9,14
   \\//   _ _
  (°)))>- _-_-   Pepper v1.0.1 (2e8d565)
    ```
Determine paths ...
Runcard: "pepper.ini"
Data: "/Users/eno/.local/share/pepper"
Prepare parallel execution ...
None (single-threaded execution on CPU)
Initialise processes and event generation ...
Process: [ppjj]
PDF: NNPDF30_nlo_as_0118/0
 -> t mass = 173.07
 -> Z mass = 91.2
Begin generating 100 event batch(es) ...
1.32027e+08 +- 3.30267e+06 pb (2.50151 %)
1378 of 10000 events are non-zero (13.78 %)
0s elapsed
 -> Time per event: 1.7e-06s (2.11765e+09 events per hour)
```
We have suppressed event output using `-Q` which is why we only get the total
cross section of the generated event sample as a result here.
In addition, timing information is printed, such as the number of events
generated per hour.
