# Main options

The `[main]` sections is used to select the process
and set some basic behaviour.
Let's look at a simple example:

```ini
[main]
process = d db -> u ub
batch_size = 100
n_batches = 10
verbosity = debug
random_seed = 1234
```

Available options are:
- `process`: This can one of `ppjj`, `ppee` or `pptt`. Appending the character
  `j` $n$ times will add $n$ additional final-state jets to the process.
  Alternatively, a partonic channel can be specified by giving a
  whitespace-separated list of particles using `->` to separate initial- and
  final-state particles. An example would be `d db -> u ub`. More than one
  channel can be specified by giving them as a comma-separated list,
  e.g. `d db -> u ub, d db -> c cb, ...`
  - Default: There is no default. This option must be set explicitly,
    either in the runcard or by using the `-p|--process` command line argument.
- `batch_size`: The number of events processed simultaneously as a so-called batch.
  This is particularly important for tuning performance on GPU.
  - Default: `1`
- `n_batches`: The Number of event batches to be generated in the simulation.
  - Default: `0`
- `verbosity`: The verbosity of Pepper.
  - Default: `info`
  - Options: `error`, `warn`, `info`, `verbose`, `debug`
- `color`: The colourfulness of Pepper's output.
  - Default: `automatic`
  - Options: `automatic`, `always`, `never`
- `device`: The device ID when running on machines equipped with multiple CUDA devices.
  - Default: `0`
- `seed`: The random seed used in the simulation.
  (Or rather, the offset from the internally used default random seed.)
  - Default: `0`
- `collision_energy`: The centre-of-mass collision energy in GeV units.
  - Default: `14000`
- `helicity_summing`: Set to `true` to use helicity summing instead of sampling
  - Default: `false`
- `helicity_integrator`: Set to `false` to disable improved helicity sampling.
   This Option is ignored when helicities are summed.
  - Default: `true`
- `helicity_summing_options`:
   Flags that steer the use of symmetries to accelerate helicity summing.
   Adding the flag `c` enables caching results of the recursion's final reduction step,
   while adding `p` enables the use of parity symmetry (if possible).
  - Default: `cp`
  - Options: `c`, `p`, `cp`
- `mu2`: The squared scale, setting both the factorisation and renormalisation scales.
  - Default: `m_Z^2`
  - Options: `m_Z^2` (squared Z mass), `H_Tp^2` (the squared sum of the transverse Z mass and the transverse jet momenta)
- `fixed_alpha_s`: Set to a positive number, e.g. `0.118`,
  to use a fixed strong coupling with that value.
  If this is not set, a running strong coupling will be determined using the LHAPDF6 library.
  - Default: not set

## Specialised batch size options

The two subsections `[main.gpu]` and `[main.cpu]`
can be used to set the batch size depending on the device type used, like so:

```ini
[main.cpu]
batch_size = 100

[main.gpu]
batch_size = 1024
```

These batch size settings take precedence over the batch size within `[main]`,
if the given device is used.
Thus, the same runcard can be used for different architectures.
