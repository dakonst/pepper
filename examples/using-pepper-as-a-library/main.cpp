#include <pepper/pepper.h>
#include <pepper/vec4.h>
#include <cstdlib>
#include <iostream>
#include <map>
#include <vector>

using namespace Pepper;

// test phase space points
const std::vector<std::vector<Vec4>> phase_space_points {
  {
    {-7000, 0, 0, -7000},
    {-7000, 0, 0, 7000},
    {6999.99999999996271,  5889.16289634903751,  1312.21386630352526,  3549.06398214329283},
    {6999.9999999999809,  -5889.16289634900113, -1312.21386630352208, -3549.06398214331102}
  },
  {
    {-7000, 0, 0, -7000},
    {-7000, 0, 0, 7000},
    {7000, -1113.78420770327079, -5691.41954312666257, -3920.10565199290022},
    {7000,  1113.78420770327079,  5691.41954312665803,  3920.10565199290068}
  },
};

// test |ME|^2 results calculated with Comix
const std::vector<double> me2_results_comix {
    77.42359737943518,
    95.51685246612145,
};

// This code is adapted from (also see the discussion therein):
// https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition
template <typename T>
bool almost_equal(T a, T b, T max_diff = 1e-16, T max_rel_diff = 1e-12)
{
  // Check if the numbers are really close -- needed
  // when comparing numbers near zero.
  T diff = std::abs(a - b);
  if (diff <= max_diff)
    return true;

  a = std::abs(a);
  b = std::abs(b);
  T largest = (b > a) ? b : a;

  return static_cast<bool>(diff <= largest * max_rel_diff);
}

int main(int argc, char* argv[])
{
  Initialization_settings pepper_init_settings {argc, argv};
  pepper_init_settings.set_process("g g -> g g");
  initialize(pepper_init_settings);
  std::atexit(finalize);

  bool incorrect_result_found {false};

  for (size_t i {0}; i < phase_space_points.size(); i++) {

    const double result {me2(phase_space_points[i])};
    std::cout << "point " << i << ": |ME2| = " << result << '\n';
    if (!almost_equal(result, me2_results_comix[i], 1e-6)) {
      incorrect_result_found = true;
      std::cerr << "ERROR: Incorrect result.\n";
    }

  }

  if (incorrect_result_found)
    return EXIT_FAILURE;

  return EXIT_SUCCESS;
}
