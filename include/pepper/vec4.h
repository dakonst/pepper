// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2024 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_PUBLIC_VEC4_H
#define PEPPER_PUBLIC_VEC4_H

#include <array>
#include <ostream>

namespace Pepper {

struct Vec4 : std::array<double, 4> {
  Vec4() : std::array<double, 4> {} {};
  Vec4(double e, double px, double py, double pz)
      : std::array<double, 4> {e, px, py, pz}
  {
  }
  friend std::ostream& operator<<(std::ostream& o, const Vec4& v)
  {
    return o << "(" << v[0] << ", " << v[1] << ", " << v[2] << ", " << v[3] << ")";
  }
};

}

#endif
