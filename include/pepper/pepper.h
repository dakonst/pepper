// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2024 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_PEPPER_H
#define PEPPER_PEPPER_H

#include <memory>
#include <string>
#include <vector>
#include <experimental/propagate_const>

namespace Pepper {

class Initialization_settings;
struct Vec4;

std::string version();

void initialize(int& argc, char** argv);
void initialize(Initialization_settings&);
void finalize();

double me2(const std::vector<Vec4>& momenta);

class Initialization_settings {
public:

  friend void initialize(int& argc, char** argv);
  friend void initialize(Initialization_settings&);
  friend void finalize();

  Initialization_settings(int& argc, char** argv);
  ~Initialization_settings();
  int& argc();
  char** argv();
  void disable_mpi_initialization();
  void disable_kokkos_initialization();
  void set_process(std::string);
private:
  struct Impl;
  std::unique_ptr<Impl> pimpl;
};

}

// TODO: import additional public API headers

#endif
