###########################################################
# Usage: bash scripts/build_pepper.sh <install-dir>       #
#   Ensure you have your environment setup with proper    #
#   compilers and drivers for your target architecture.   #
###########################################################
# Disclaimer: this is meant to give an example of how to  #
#   build a pepper environment, but it is not failproof.  #
###########################################################

# install in provided path or in current path
export TARGET_DIR=$1
if [ "$#" -ne 1 ]; then
   export TARGET_DIR=$(pwd -LP)
fi
START_DIR=$(pwd -LP)
echo Installing in path: $TARGET_DIR

mkdir -p $TARGET_DIR

cd "$TARGET_DIR" || exit 1

export LOGDIR=$TARGET_DIR/build_logs
mkdir -p $LOGDIR

##############
## SETTINGS ##
##############


# Compiler settings (choose one):
# A) generic GCC
CC=$(which gcc)
CXX=$(which g++)
# B) possible alternative on AMD systems:
# CC=$(which hipcc)
# CXX=$(which hipcc)
# C) possible alternative on Cray systems:
# CC=$(which cc)
# CXX=$(which CC)
# D) possible alternative on Intel systems:
# CC=$(which icx)
# CXX=$(which icpx)


# MPI Related settings
MPI_ENABLED=0
MPI_CC=NONE
MPI_CXX=NONE
if which mpicxx > /dev/null 2>&1; then
   echo Enabling MPI
   MPI_ENABLED=1
   # Choose one:
   # A) Standard MPI install
   MPI_CC=$(which mpicc)
   MPI_CXX=$(which mpicxx)
   # B) possible alternative on Cray systems:
   # MPI_CC=$(which cc)
   # MPI_CXX=$(which CC)
fi

# KOKKOS Related settings
KOKKOS_TAG=4.1.00
KOKKOS_BUILD=Release
KOKKOS_URL=https://github.com/kokkos/kokkos.git

# Enable Sofware Framework (choose one):
# A) Enable CUDA
# KOKKOS_ENABLED=Kokkos_ENABLE_CUDA
# B) Enable HIP
# KOKKOS_ENABLED=Kokkos_ENABLE_HIP
# C) Enable OpenMP
KOKKOS_ENABLED=Kokkos_ENABLE_OPENMP
# more available on Kokkos website

# Enable Architecture (choose one):
# A) NVidia H100
# KOKKOS_ARCH_FLAG=Kokkos_ARCH_HOPPER90
# B) NVidia A100
# KOKKOS_ARCH_FLAG=Kokkos_ARCH_AMPERE80
# C) NVidia V100
# KOKKOS_ARCH_FLAG=Kokkos_ARCH_VOLTA70
# D) AMD MI250
# KOKKOS_ARCH_FLAG=Kokkos_ARCH_VEGA90A
# E) AMD MI100
# KOKKOS_ARCH_FLAG=Kokkos_ARCH_VEGA908
# F) Intel Skylake
KOKKOS_ARCH_FLAG=Kokkos_ARCH_SKX
# more available on Kokkos website

# Some need extra flags needed for some software frameworks
NO_EXTRA_FLAGS=
CUDA_EXTRA_FLAGS="-DKokkos_ENABLE_CUDA_LAMBDA=On \
                  -DKokkos_ENABLE_CUDA_CONSTEXPR=On"
HIP_EXTRA_FLAGS="-DCMAKE_CXX_COMPILER=$CXX \
                 -DCMAKE_CXX_FLAGS=\"--gcc-toolchain=/soft/compilers/gcc/12.2.0/x86_64-suse-linux\""
# Set this to HIP_EXTRA_FLAGS or CUDA_EXTRA_FLAGS 
#   or NO_EXTRA_FLAGS depending on your build
EXTRA_FLAGS=$NO_EXTRA_FLAGS

# select packages to install (!= 0 to enable)
INSTALL_FORM=1
INSTALL_HDF5=1
INSTALL_LHAPDF=1

# HDF5 Related settings
HDF5_VERSION=1_14_2

# FORM Related settings
FORM_VERSION=v4.3.1

# PEPPER Related settings
PEPPER_VERSION=main



# sometimes autoconf is not available so try installing it
INSTALL_AUTOCONF=0
if ! which autoconf > /dev/null && ([ "$INSTALL_LHAPDF" -ne 0 ] || [ "$INSTALL_HDF5" -ne 0 ] || [ "$INSTALL_FORM" -ne 0 ]); then
   INSTALL_AUTOCONF=1
fi
AUTOCONF_VERSION=2.71



# function for checking exit status
check_exit_status() {
   EXIT_STATUS=$?
   COMMAND_NAME=$1
   
   if [ $EXIT_STATUS -ne 0 ]; then
      echo "$COMMAND_NAME failed with exit status: $EXIT_STATUS"
      exit $EXIT_STATUS
   fi
}

####################
## install Kokkos ##
####################
echo Installing Kokkos ARCH=$KOKKOS_ARCH_FLAG
{
   git clone $KOKKOS_URL -b $KOKKOS_TAG
   check_exit_status "kokkos git clone"

   cd kokkos
   
   cmake -S . -B build/kokkos-$KOKKOS_TAG/$KOKKOS_BUILD \
   -DCMAKE_INSTALL_PREFIX=install/kokkos-$KOKKOS_TAG/$KOKKOS_BUILD \
   -DCMAKE_BUILD_TYPE=$KOKKOS_BUILD \
   -DCMAKE_CXX_STANDARD=17 \
   -D$KOKKOS_ARCH_FLAG=ON \
   -D$KOKKOS_ENABLED=ON \
   $EXTRA_FLAGS
   check_exit_status "kokkos cmake"
   
   make -C build/kokkos-$KOKKOS_TAG/$KOKKOS_BUILD -j install
   check_exit_status "kokkos make"

   echo "export KOKKOS_HOME=$PWD/install/kokkos-$KOKKOS_TAG/$KOKKOS_BUILD" > setup.sh
   echo "export CMAKE_PREFIX_PATH=\$KOKKOS_HOME/lib64/cmake/Kokkos:\$CMAKE_PREFIX_PATH" >> setup.sh
   echo "export CPATH=\$KOKKOS_HOME/include:\$CPATH" >> setup.sh
   echo "export PATH=\$KOKKOS_HOME/bin:\$PATH" >> setup.sh
   echo "export LD_LIBRARY_PATH=\$KOKKOS_HOME/lib64:\$LD_LIBRARY_PATH" >> setup.sh
   source setup.sh
} 1> $LOGDIR/kokkos.stdout.txt 2> $LOGDIR/kokkos.stderr.txt


cd "$TARGET_DIR" || exit 1

#######################
## install Autoconf  ##
#######################
if [ "$INSTALL_AUTOCONF" -ne 0 ]; then
   echo Installing Autoconf $AUTOCONF_VERSION
   {
      wget https://ftp.gnu.org/gnu/autoconf/autoconf-$AUTOCONF_VERSION.tar.gz
      check_exit_status "autoconf wget"
      tar xf autoconf-$AUTOCONF_VERSION.tar.gz
      cd autoconf-$AUTOCONF_VERSION
      ./configure --prefix=$PWD/install
      check_exit_status "autoconf configure"
      make -j install
      check_exit_status "autoconf make"
      echo "export AUTOCONF_PATH=$PWD/install" > setup.sh
      echo "export PATH=\$PATH:\$AUTOCONF_PATH/bin" >> setup.sh
      source setup.sh
   } 1> $LOGDIR/autoconf.stdout.txt 2> $LOGDIR/autoconf.stderr.txt
fi

cd "$TARGET_DIR" || exit 1

#######################
## install HDF5      ##
#######################
if [ "$INSTALL_HDF5" -ne 0 ]; then
   echo Installing HDF5 $HDF5_VERSION
   {
      git clone https://github.com/HDFGroup/hdf5.git -b hdf5-$HDF5_VERSION
      check_exit_status "hdf5 git clone"
      cd hdf5
      if [ "$MPI_ENABLED" -ne 0 ]; then
         ./configure --prefix=$PWD/install --enable-parallel --enable-parallel-tools
      else
         ./configure --prefix=$PWD/install CC=$CC CXX=$MPI_CXX
      fi
      check_exit_status "hdf5 configure"
      make -j install
      check_exit_status "hdf5 make"
      echo "export HDF5_PATH=$PWD/install" > setup.sh
      echo "export PATH=\$PATH:\$HDF5_PATH/bin" >> setup.sh
      echo "export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:\$HDF5_PATH/lib" >> setup.sh
      source setup.sh
   } 1> $LOGDIR/hdf5.stdout.txt 2> $LOGDIR/hdf5.stderr.txt
fi

cd "$TARGET_DIR" || exit 1

#######################
## install LHAPDF    ##
#######################
if [ "$INSTALL_LHAPDF" -ne 0 ]; then
   echo Installing LHAPDF kokkos_version
   {
      git clone https://gitlab.com/hepcedar/lhapdf.git -b kokkos_version
      check_exit_status "lhapdf git clone"
      cd lhapdf
      autoreconf -i --force
      check_exit_status "lhapdf autoreconf"
      if [ "$MPI_ENABLED" -ne 0 ]; then
         ./configure --prefix=$PWD/install --disable-python --enable-mpi
      else
         ./configure --prefix=$PWD/install --disable-python
      fi
      check_exit_status "lhapdf configure"
      make -j install
      check_exit_status "lhapdf make"
      wget https://lhapdfsets.web.cern.ch/current/NNPDF30_nlo_as_0118.tar.gz -O- | tar xz -C $TARGET_DIR/lhapdf/install/share/LHAPDF

      echo "export LHAPDF_PATH=$PWD/install" > setup.sh
      echo "export PKG_CONFIG_PATH=\$PKG_CONFIG_PATH:\$LHAPDF_PATH/lib/pkgconfig" >> setup.sh
      source setup.sh
   } 1> $LOGDIR/lhapdf.stdout.txt 2> $LOGDIR/lhapdf.stderr.txt
fi

cd "$TARGET_DIR" || exit 1


#######################
## install form      ##
#######################
if [ "$INSTALL_FORM" -ne 0 ]; then
   echo Installing form $FORM_VERSION
   {
      git clone https://github.com/vermaseren/form.git -b $FORM_VERSION
      check_exit_status "form git clone"
      cd form
      autoreconf -i
      ./configure --prefix=$PWD/install CC=$CC CXX=$CXX
      check_exit_status "form configure"
      make -j install
      check_exit_status "form make"
      echo "export FORM_PATH=$PWD/install" > setup.sh
      echo "export PATH=\$PATH:\$FORM_PATH/bin" >> setup.sh
      source setup.sh
   } 1> $LOGDIR/form.stdout.txt 2> $LOGDIR/form.stderr.txt
fi


cd "$TARGET_DIR" || exit 1

######################
## install Pepper   ##
######################
echo Installing pepper $PEPPER_VERSION
{
   git clone https://gitlab.com/spice-mc/pepper.git -b $PEPPER_VERSION
   check_exit_status "pepper git clone"
   cd pepper
   sed -i 's\option(BUILD_SHARED_LIBS "Build using shared libraries" ON)\\g' src/CMakeLists.txt
   cmake -S . -B build/kokkos-$KOKKOS_TAG/$KOKKOS_BUILD \
      -DCMAKE_INSTALL_PREFIX=install/kokkos-$KOKKOS_TAG/$KOKKOS_BUILD \
      -DCMAKE_BUILD_TYPE=$KOKKOS_BUILD \
      -DCMAKE_CXX_COMPILER=$CXX \
      -DPEPPER_VCL_DISABLED=1 \
      $EXTRA_FLAGS
   check_exit_status "pepper cmake"

   make -C build/kokkos-$KOKKOS_TAG/$KOKKOS_BUILD -j install
   check_exit_status "pepper make"
   echo "export PEPPER_PATH=$PWD/install/kokkos-$KOKKOS_TAG/$KOKKOS_BUILD" > setup.sh
   echo "export PATH=\$PATH:\$PEPPER_PATH/bin" >> setup.sh
   source setup.sh
} 1> $LOGDIR/pepper.stdout.txt 2> $LOGDIR/pepper.stderr.txt

cd "$TARGET_DIR" || exit 1

# create setup file for package
setup_file="setup.sh"
> "$setup_file"

# Loop through all declared variables
declare -p | while read -r line; do
    # Check if the line declares a function
    if [[ $line =~ ^declare\ -[a-zA-Z]*f ]]; then
        continue # Skip functions
    fi

    # Extract variable name and value
    if [[ $line =~ ^declare\ -[a-zA-Z]*x\ ([a-zA-Z_][a-zA-Z0-9_]*)= ]]; then
        var_name=${BASH_REMATCH[1]}
        var_value=${!var_name}
        echo "export $var_name='$var_value'" >> "$setup_file"
    fi
done

# Make the script executable (optional)
chmod +x "$setup_file"
