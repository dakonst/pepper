// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023-2024 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef HDF5_PHASE_SPACE_READER_H
#define HDF5_PHASE_SPACE_READER_H

#include "pepper/config.h"

#include "phase_space.h"
#include "highfive/H5File.hpp"
#include "highfive/H5PropertyList.hpp"

class Flavour_process;
struct Settings;

class Hdf5_phase_space_reader : public Phase_space_generator {
public:
  Hdf5_phase_space_reader(const Flavour_process&, const Settings&);
  ~Hdf5_phase_space_reader() override {}

  void fill_momenta_and_weights(Event_handle&) const override;
  void update_weights(Event_handle&) const override {}

  bool is_host_only() const override { return true; }
  bool allows_optimisation() const override { return false; }

  // for now, the implementation assumes that there is a single file for a
  // single flavour process
  bool allows_multiple_flavour_processes() const override { return false; }

private:
  std::unique_ptr<HighFive::File> file;
  HighFive::DataTransferProps transfer_props;
  std::vector<std::vector<double> > events, particles, pinfo;
  mutable int n_events_read {0};
};

#endif
