// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "hepmc3_writer.h"
#include "citations.h"
#include "event_handle.h"
#include "fill_leading_colour_information.h"
#include "flavour_process.h"
#include "paths.h"
#include "pdf_and_alpha_s.h"
#include "process.h"
#include "utilities.h"
#include "zstr.hpp"

#include "HepMC3/GenParticle.h"
#include "HepMC3/GenVertex.h"
#include <random>

enum class Hepmc3_particle_status {
  undefined = 0,
  undecayed_physical = 1,
  decayed_physical = 2,
  documentation = 3,
  incoming_beam = 4
};
// According to the HepMC3 manual, the "documentation" status is "[o]ften
// used to indicate in/out particles [of the] hard process". We follow this
// convention for the in particles. For the out particles, we use
// "undecayed_physical", as we do not yet support decays in Pepper. Moreover,
// the numbers 11-200 are reserved for generator-dependent usage, but for now we
// have no need for additional status codes.

using namespace Paths;
using namespace HepMC3;

Hepmc3_writer::Hepmc3_writer(const std::string& _filepath, const Settings& s,
                             const SM_parameters& sm)
    : leading_colour_flow_output_enabled {s.leading_colour_flow_output_enabled},
      e_cms {s.e_cms},
      ew_alpha {sm.ew.alpha()},
      filepath {path_with_rank_info(_filepath)}
{
  INFO("Events will be written in the HepMC3 Asciiv3 format to \"",
       path_with_rank_pattern(_filepath), "\"");
}

Hepmc3_writer::Hepmc3_writer(std::ostream& _stream, const Settings& s,
                             const SM_parameters& sm)
    : leading_colour_flow_output_enabled {s.leading_colour_flow_output_enabled},
      e_cms {s.e_cms},
      ew_alpha {sm.ew.alpha()},
      stream {&_stream}
{
  INFO("Events will be written in the HepMC3 Asciiv3 format to stdout");
}

void Hepmc3_writer::initialise(const Process& proc,
                               const Pdf_and_alpha_s& pdf_and_alphas)
{
  Citations::register_citation("HepMC v3", "Buckley:2019xhk");

  colours.resize(proc.n_ptcl());
  anticolours.resize(proc.n_ptcl());

  assert(stream != nullptr || !filepath.empty());
  if (stream != nullptr) {
    initialise_ascii_writer(*stream);
  }
  else if (!filepath.empty()) {
    if (case_insensitive_ends_with(filepath, ".gz")) {
      zstream = std::unique_ptr<std::ostream>(new zstr::ofstream(filepath));
      initialise_ascii_writer(*zstream);
    }
    else {
      initialise_ascii_writer(filepath);
    }
  }
  initialised = true;

  // determine beam particles via a heuristic approach
  if (pdf_and_alphas.pdf_enabled()) {
    // PDF use is enabled, for now we assume that this means that we are doing
    // proton-proton collisions
    beams[0] = 2212;
    beams[1] = 2212;
  }
  else if (proc.n_procs() == 1) {
    // no PDF is used, but there is only a single process, so let's use its
    // initial-state particles as pure partonic beams in this case
    beams[0] = proc[0][0];
    beams[1] = proc[0][1];
  }

  // create PDF info
  gen_pdf_info = std::make_shared<HepMC3::GenPdfInfo>();
  for (int p {0}; p < 2; ++p) {
    gen_pdf_info->pdf_id[p] = pdf_and_alphas.lhapdf_id();
  }
}

void Hepmc3_writer::finalise(const MC_result&)
{
  ascii_writer.reset();
  gen_run_info.reset();
}

void Hepmc3_writer::write_n(const Event_handle& evt, int n,
                            const MC_result& mc_result, long n_nonzero,
                            long n_trials, const Process& proc)
{
  // To ensure proper normalization of the total cross section, the "trials"
  // needs to be evenly distributed over all the events in the batch.
  // Otherwise, when events are rejected the total cross section will be modified.
  size_t trials_add = n_trials / n_nonzero;
  std::vector<size_t> trials_remainder(n_nonzero, 0);
  for(size_t i = 0; i < n_trials-trials_add*n_nonzero; ++i) {
    trials_remainder[i] = 1;
  }
  std::random_device device;
  std::mt19937 gen(device());
  std::shuffle(trials_remainder.begin(), trials_remainder.end(), gen);

  size_t remainder_idx = 0;
  const int batch_size {evt.host.batch_size};
  for (int i {0}; i < n; ++i) {

    if (evt.host.w(i) == 0.0)
      continue;

    GenEvent gen_evt {gen_run_info, Units::GEV, Units::MM};
    gen_evt.set_event_number(n_written);

    const auto n_evt_trials = trials_add + trials_remainder[remainder_idx++];

    gen_evt.weights() = {evt.host.w(i) * evt.host.me2(i),
                         static_cast<double>(n_evt_trials)};

    // If there are beams, add them and their vertices (which will connect each
    // beam with their respective incoming parton of the hard scattering
    // vertex). NOTE: In HepMC3.2.6, it matters if the beams and beam vertices
    // are set up before or after the hard scattering below; the HepMC3 Asciiv3
    // reader seems to be sensitive on the ordering of the objects and crashes
    // if the beams are set up last.
    std::array<GenVertexPtr, 2> beam_vertices;
    for (int p {0}; p < 2; ++p) {
      if (beams[p] != NO_PTCL) {

        // create beam vertex
        beam_vertices[p] = std::make_shared<GenVertex>();

        // create beam particle and add it to the vertex
        FourVector hepmc3_mom {0.0, 0.0, ((p == 0) ? -1.0 : 1.0) * e_cms / 2.0,
                               e_cms / 2.0};
        GenParticlePtr beam_ptcl {std::make_shared<GenParticle>(
            hepmc3_mom, beams[p],
            static_cast<int>(Hepmc3_particle_status::incoming_beam))};

        // connect objects and add them to the event
        beam_vertices[p]->add_particle_in(beam_ptcl);
        gen_evt.add_vertex(beam_vertices[p]);

      }
    }

    if (leading_colour_flow_output_enabled) {
      fill_leading_colour_information(evt, i, proc, colours, anticolours);
    }

    // Add a hard scattering (or "ME") vertex and its incoming and outgoing
    // particles; if there are beam vertices, we are also connect the incoming
    // particles to them.
    GenVertexPtr gen_me_vertex {std::make_shared<GenVertex>()};
    gen_evt.add_vertex(gen_me_vertex);
    for (int p {0}; p < evt.host.n_ptcl; ++p) {
      const bool is_incoming {p < 2};

      // create particle
      FourVector hepmc3_mom {evt.host.px(i, p), evt.host.py(i, p), evt.host.pz(i, p),
                             evt.host.e(i, p)};
      if (is_incoming) {
        hepmc3_mom *= -1.0;
      }
      Ptcl_num ptcl_num {proc.current_flavour_process().unmapped_ptcl(evt.host.flav_channels(i), p)};
      int status {static_cast<int>(
          (is_incoming) ? Hepmc3_particle_status::documentation
                        : Hepmc3_particle_status::undecayed_physical)};
      GenParticlePtr ptcl {
          std::make_shared<GenParticle>(hepmc3_mom, ptcl_num, status)};

      // add to the vertex or vertices
      if (is_incoming) {
        gen_me_vertex->add_particle_in(ptcl);
        if (beam_vertices[p])
          beam_vertices[p]->add_particle_out(ptcl);
      }
      else {
        gen_me_vertex->add_particle_out(ptcl);
      }
      if (leading_colour_flow_output_enabled) {
        int colour {colours[p]};
        int anticolour {anticolours[p]};
        if (is_incoming)
          std::swap(colour, anticolour);
        // NOTE: attributes should be set after adding the particles to the
        // event
        ptcl->add_attribute("flow1", std::make_shared<IntAttribute>(colour));
        ptcl->add_attribute("flow2",
                            std::make_shared<IntAttribute>(anticolour));
      }
    }

    // add current cross section
    if (i == batch_size - 1) {
      std::shared_ptr<GenCrossSection> cross_section =
          std::make_shared<GenCrossSection>();
      // note that we fill xs, err = -1.0 for auxiliary weights
      cross_section->set_cross_section(
          std::vector<double> {mc_result.mean(), -1.0},
          std::vector<double> {mc_result.stddev(), -1.0}, mc_result.n_trials,
          mc_result.n_nonzero);
      gen_evt.add_attribute("GenCrossSection", cross_section);
    }

    // add PDF information
    if (beams[0] != NO_PTCL) {
      for (int p {0}; p < 2; ++p) {
        gen_pdf_info->parton_id[p] =
            proc.current_flavour_process().unmapped_ptcl(evt.host.flav_channels(i), p);
      }
#if KOKKOS_LHAPDF_FOUND
      WARN_ONCE("The PDF x*f(x) info is not avaible for HepMC writeout when "
                "CUDA LHAPDF is used.");
#else
      gen_pdf_info->xf[0] = evt.host.xfx1(i);
      gen_pdf_info->xf[1] = evt.host.xfx2(i);
#endif
      gen_pdf_info->x[0] = evt.host.x1(i);
      gen_pdf_info->x[1] = evt.host.x2(i);
      gen_pdf_info->scale = evt.host.mu2(i);
      gen_evt.set_pdf_info(gen_pdf_info);
    }

    ascii_writer->write_event(gen_evt);

    n_written++;
  }
}

