// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_SCALE_SETTER_H
#define PEPPER_SCALE_SETTER_H

#include <memory>
#include <string>

class Event_handle;

/// Abstract class defining the interface for setting scales.
class Scale_setter {
public:
  virtual ~Scale_setter() = default;

  /// Calculate (potentially phase-space dependent) scales for each event in a
  /// batch.
  virtual void fill_scales(Event_handle&) const = 0;

  /// Indicate if the scale is fixed, i.e. not phase-space dependent.
  /// Override in derived classes and return true if this is the case.
  /// This information can then be used by users for optimisations.
  /// fixed_mu2 should only be used if is_fixed returns true.
  virtual bool is_fixed() const { return false; }
  virtual double fixed_mu2() const { return -1.0; }
};

std::unique_ptr<Scale_setter> create_scale_setter(const std::string& spec);

/// Implement mu2 = const.
class Fixed_scale_setter : public Scale_setter {
public:
  Fixed_scale_setter(double mu2);
  void fill_scales(Event_handle&) const override;
  bool is_fixed() const override { return true; }
  double fixed_mu2() const override { return mu2; }

private:
  double mu2;
};

/// Implement mu2 = H_T'^2 = (m_perp_ll + sum_jets p_perp_jet)^2.
class Htp2_scale_setter : public Scale_setter {
public:
  void fill_scales(Event_handle&) const override;
};

/// Implement mu2 = H_T'^2 / 2 = (m_perp_ll + sum_jets p_perp_jet)^2 / 2.
class Htp2_2_scale_setter : public Scale_setter {
public:
  void fill_scales(Event_handle&) const override;
};

/// Implement mu2 = H_T^2 / 2 = (sum_jets p_perp_jet)^2 / 2.
class Ht2_2_scale_setter : public Scale_setter {
public:
  void fill_scales(Event_handle&) const override;
};

/// Implement mu2 = H_{T,M}^2 / 2 = (sum_ptcls m_perp_ptcl)^2 / 2.
class Htm2_2_scale_setter : public Scale_setter {
public:
  void fill_scales(Event_handle&) const override;
};

#endif
