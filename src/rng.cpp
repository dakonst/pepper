// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "rng.h"
#include "event_handle.h"
#include "mpi.h"

KOKKOS_INLINE_FUNCTION int ri(double r, int min, int max)
{
  const double diff {static_cast<double>(max - min)};
  return (int)((diff + 1.0) * r) + min;
}


void Rng::fill(Event_handle& evt)
{
#if !FORCE_HOST_RNG
  fill_d(evt.device._r);
#else
  fill_d(evt.host._r);
  evt.push_random_numbers_to_device();
#endif
}

Rng::Rng(int _seed_offset) : 
#if !FORCE_HOST_RNG
    random_pool(_seed_offset + Mpi::rank()),
#endif
    seed_offset(_seed_offset + Mpi::rank())
{ 
  DEBUG("Rng::seed_offset = ",seed_offset);
  reset(); 
}

Rng::~Rng()
{
  using namespace Timing;
  register_duration({Task::Event_generation, Task::Rng}, fill_d_duration);
}

// It should be sufficient to use subsequent seeds, cf.
// http://dx.doi.org/10.5194/ars-12-75-2014
void Rng::reset() { 
  gen.seed(seed_offset);
#if !FORCE_HOST_RNG
  random_pool = Kokkos::Random_XorShift64_Pool<>(seed_offset);
#endif
}

double Rng::d() { return dis(gen); }
