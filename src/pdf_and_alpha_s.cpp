// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "pdf_and_alpha_s.h"

#include "pepper/config.h"

#include "citations.h"
#include "event_handle.h"
#include "process.h"
#include "settings.h"
#include "sm.h"
#include "timing.h"
#include "utilities.h"

#if KOKKOS_LHAPDF_FOUND
#include "compiler_warning_macros.h"
PEPPER_SUPPRESS_COMMON_WARNINGS_PUSH
#include "LHAPDF/KKPDF.h"
PEPPER_SUPPRESS_COMMON_WARNINGS_POP
#include "pdf_and_alpha_s_kernel.h"
#endif

#include <cassert>

void validate_and_sync_particle_properties_with_pdf_info(const Settings& s)
{
  if (s.pdf_disabled || s.pdfset == "dummy")
    return;
#if LHAPDF_FOUND
  INFO("PDF: ", s.pdfset, "/", s.pdfmember);
  LHAPDF::PDFInfo pdfinfo {s.pdfset, s.pdfmember};
  if (pdfinfo.has_key("MTop"))
    sm_properties::getInstance().set_mass(TOP_QUARK, pdfinfo.get_entry_as<double>("MTop"));
  if (pdfinfo.has_key("MZ"))
    sm_properties::getInstance().set_mass(Z, pdfinfo.get_entry_as<double>("MZ"));
  if (pdfinfo.has_key("NumFlavors")) {
    const int n_flav {pdfinfo.get_entry_as<int>("NumFlavors")};
    if (n_flav != 5) {
      ERROR("The selected PDF set ", s.pdfset, " has ", n_flav,
            " quark flavours, but Pepper only supports 5. Abort.");
      abort();
    }
  }
  if (pdfinfo.has_key("Particle")) {
    const int p {pdfinfo.get_entry_as<int>("Particle")};
    if (p != 2212) {
      ERROR("The selected PDF set ", s.pdfset, " is for \"", p,
            "\" particles, but Pepper only supports 2212 (protons). Abort.");
      abort();
    }
  }
  if (pdfinfo.has_key("Reference")) {
    std::string label {s.pdfset};
    replace_all(label, "_", "\\_");
    Citations::register_citation(label, pdfinfo.get_entry("Reference"));
  }
#endif
}

Pdf_and_alpha_s::Pdf_and_alpha_s(const Settings& s, const SM_parameters& sm)
    : g_em {sm.ew.g()},
      fixed_alpha_s {s.fixed_alpha_s},
      _pdf_enabled {!s.pdf_disabled && !s.me2_only}
{
  if (!_pdf_enabled && fixed_alpha_s != -1.0) {
    INFO("PDF: disabled");
    return;
  }
#if LHAPDF_FOUND
  if (s.pdfset != "dummy")
    Citations::register_citation("LHAPDF v6", "Buckley:2014ana");
#if KOKKOS_LHAPDF_FOUND
  if (s.pdfset == "dummy") {
    ERROR("CUDA runs do not yet support dummy PDFs. Abort.");
    abort();
  }
  LHAPDF::setVerbosity(0);
  kk_pdf = new KKPDF(s.pdfset, s.pdfmember);
  gridpdf  = std::make_unique<LHAPDF::GridPDF>(s.pdfset, s.pdfmember);
  Kokkos::resize(Kokkos::WithoutInitializing, xfx_products, s.batch_size);
  Kokkos::resize(Kokkos::WithoutInitializing, abs_xfx_products, s.batch_size);
#else
  xfx1.resize(13);
  xfx2.resize(13);
  if (s.pdfset == "dummy") {
    enable_dummy_pdf();
  }
  else {
    // since LHAPDF output can not be redirected to cerr, we can not allow it
    LHAPDF::setVerbosity(0);
    gridpdf = std::make_unique<LHAPDF::GridPDF>(s.pdfset, s.pdfmember);
  }
#endif
#else
  // without LHAPDF, the best we can do is to allow using a dummy PDF
  if (s.pdfset == "dummy") {
    xfx1.resize(13);
    xfx2.resize(13);
    enable_dummy_pdf();
  }
  else {
    WARN("Disabling PDFs and using fixed alpha_s = 0.118,"
         " because LHAPDF is not available.");
    _pdf_enabled = false;
    fixed_alpha_s = 0.118;
  }
#endif
}

Pdf_and_alpha_s::~Pdf_and_alpha_s()
{
  using namespace Timing;
  register_duration({Task::Event_generation, Task::Pdf_and_alpha_s},
                    fill_pdf_duration + update_couplings_duration);
  register_duration({Task::Event_generation, Task::Pdf_and_alpha_s, Task::Pdf},
                    fill_pdf_duration);
  register_duration({Task::Event_generation, Task::Pdf_and_alpha_s, Task::Alpha_s},
                    update_couplings_duration);
}

void Pdf_and_alpha_s::enable_dummy_pdf()
{
  dummy_pdf_enabled = true;
  VERBOSE("PDF: dummy, i.e. constant f(x, Q^2) = 1.0");
  if (fixed_alpha_s == -1.0) {
    WARN("A dummy PDF must be paired with a fixed value for alpha_s."
         " Will use alpha_s = 0.118.");
    fixed_alpha_s = 0.118;
  }
}

bool Pdf_and_alpha_s::updates_weights_on_device() const
{
#if KOKKOS_LHAPDF_FOUND
  // if running with LHAPDF Kokkos support, we will always update weights on
  // the device
  return true;
#endif

  // otherwise, we might still update weights on the device, but only if the
  // only thing we have to do is to apply a constant coupling factor via
  // set_fixed_alpha_s()
  return (fixed_alpha_s != -1.0 && !_pdf_enabled);
}

void Pdf_and_alpha_s::update_weights(Event_handle& evt, Process& proc) const
{
  Kokkos::Profiling::pushRegion("Pdf_and_alpha_s::update_weights");
  Timer timer;

#if !KOKKOS_LHAPDF_FOUND
  // If we update weights on the host, we need to make sure that we pull all the
  // data needed:
  // - If the phase-space generation happens on-device, we need to pull x12 data.
  // - If there are dynamic scales, we need to pull them, too.
  // Note that the random numbers, required for flavour channel sampling, should
  // already have been pulled by Generator. Generator::evaluate_points is also
  // responsible for making sure that the weights are where we need them.
  if (!updates_weights_on_device()) {
    if (_pdf_enabled && !proc.ps_generator().is_host_only())
      evt.pull_x12_from_device();
    if (fixed_mu2 == -1.0)
      evt.pull_scales_from_device();
  }
#endif

  // couplings
  if (fixed_alpha_s != -1.0) {
    set_fixed_alpha_s(evt, proc);
  } else {
    update_couplings(evt, proc);
  }
  update_couplings_duration += timer.elapsed();
  timer.reset();

  // Pdfs
  if(_pdf_enabled) {
    update_pdf(evt,proc);
  }

#if !KOKKOS_LHAPDF_FOUND
  // If we update weights on the host, we need to make sure that we push all the
  // data generated.
  // Note that we do not push the flav_channel data, because it anyway is only
  // read on the host at this point, specifically by the event writers, that
  // need to know which initial-state flavours to write out, and by the
  // leading-colour filling, which needs to know the actual unmapped flavour
  // structure.
  // As above, we can ignore the weights, because Generator::evaluate_points is
  // responsible for them.
  if (!updates_weights_on_device()) {
    if (fixed_alpha_s == -1.0)
      evt.push_alpha_s_to_device();
  }
#endif

  fill_pdf_duration += timer.elapsed();
  Kokkos::Profiling::popRegion();
}

void Pdf_and_alpha_s::set_fixed_mu2(double mu2)
{
  fixed_mu2 = mu2;
  VERBOSE("Scales: constant mu^2 = mu_F^2 = mu_R^2 = ", mu2);
#if LHAPDF_FOUND
  if (gridpdf) {
    fixed_alpha_s = gridpdf->alphasQ2(mu2);
    VERBOSE("Strong coupling: constant alpha_s(mu_R^2) = ", fixed_alpha_s);
  }
#endif
}

void Pdf_and_alpha_s::set_fixed_alpha_s(Event_handle& evt, Process& proc) const
{
  const auto& flav_proc {proc.current_flavour_process()};
  // calculate a fixed alpha_s once and apply to all events in the batch
  const double fac {
    flav_proc.coupling_factor(alpha_to_g(fixed_alpha_s), g_em)};
#if KOKKOS_LHAPDF_FOUND
  evt.apply_weight_prefactor(fac);
#else
  if (!updates_weights_on_device()) {
    for (int i=0;i < (int)evt.host.w.extent(0);++i)
      evt.host.w(i) *= fac;
  }
  else {
    evt.apply_weight_prefactor(fac);
  }
#endif
  evt.set_fixed_alpha_s(fixed_alpha_s);
}

void Pdf_and_alpha_s::update_couplings(Event_handle& evt, Process& proc) const
{
#if !LHAPDF_FOUND
  // update_couplings should not have been called if !LHAPDF_FOUND;
  // this should be prevented by setting fixed_alpha_s = 0.118 in the ctor
  (void)evt;
  (void)proc;
  assert(false);
#else
  const auto& flav_proc {proc.current_flavour_process()};
#if KOKKOS_LHAPDF_FOUND
  kk_pdf->alphasQ2(evt.device.mu2, evt.device.alpha_s);
  const auto& _g_em {g_em};
  const auto& _n_lepton_pairs {flav_proc.n_lepton_pairs()};
  RUN_KERNEL(compute_coupling_factor_kernel, evt, _g_em, _n_lepton_pairs);
  Kokkos::fence();
#else
  for (int i {0}; i < evt.host.batch_size; ++i) {
    if (evt.host.w(i) == 0.0)
      continue;

    // get dynamic or fixed mu2 value
    const double mu2 {(fixed_mu2 == -1.0) ? evt.host.mu2(i) : fixed_mu2};

    // evaluate alpha_s and apply
    const double alpha_s {gridpdf->alphasQ2(mu2)};
    const double fac {flav_proc.coupling_factor(alpha_to_g(alpha_s), g_em)};
    evt.host.w(i) *= fac;
    evt.host.alpha_s(i) = alpha_s;
  }
#endif
#endif
}

void Pdf_and_alpha_s::update_pdf(Event_handle& evt,
				 Process& proc) const
{
  const auto& flav_proc {proc.current_flavour_process()};
  const int n_ptcl {flav_proc.n_ptcl()};
  const int n_channels {flav_proc.n_channels()};

#if KOKKOS_LHAPDF_FOUND
  Kokkos::deep_copy(xfx_products,0.0);
  Kokkos::deep_copy(abs_xfx_products,0.0);
  for (int j {0}; j < n_channels; ++j) {
    const auto f1 = flav_proc[j * n_ptcl];
    const auto f2 = flav_proc[j * n_ptcl + 1];
    kk_pdf->xfxQ2(evt.device.x1, evt.device.mu2, evt.device.xfx1, f1);
    kk_pdf->xfxQ2(evt.device.x2, evt.device.mu2, evt.device.xfx2, f2);
    const auto& _xfx_products {xfx_products};
    const auto& _abs_xfx_products {abs_xfx_products};
    RUN_KERNEL(combine_xfxs, evt, _xfx_products);
    RUN_KERNEL(combine_abs_xfxs, evt, _abs_xfx_products);
  }

  const auto& _xfx_products {xfx_products};
  const auto& _abs_xfx_products {abs_xfx_products};
  RUN_KERNEL(apply_pdf_to_weight_and_prepare_flavour_channel_selection, evt,
             _xfx_products, _abs_xfx_products);
  Kokkos::fence();

  for (int j {0}; j < n_channels; ++j) {
    const auto f1 = flav_proc[j * n_ptcl];
    const auto f2 = flav_proc[j * n_ptcl + 1];
    kk_pdf->xfxQ2(evt.device.x1, evt.device.mu2, evt.device.xfx1, f1);
    kk_pdf->xfxQ2(evt.device.x2, evt.device.mu2, evt.device.xfx2, f2);

    // calculate xfx_products[i] -= xfx1[i] * xfx2[i], until it is <= 0.0; at
    // that point, we select the channel
    const auto& _ab_xfx_products {abs_xfx_products};
    RUN_KERNEL(flavour_channel_selection_kernel, evt, _ab_xfx_products, j);
    Kokkos::fence();
  }
#else

  xfx_products.resize(n_channels);

  for (int i {0}; i < evt.host.batch_size; ++i) {
    if (evt.host.w(i) == 0.0)
      continue;

    // get dynamic or fixed mu2 value
    const double mu2 {(fixed_mu2 == -1.0) ? evt.host.mu2(i) : fixed_mu2};

    const double x1 {evt.host.x1(i)};
    const double x2 {evt.host.x2(i)};

    if (x1 == 1.0 && x2 == 1.0)
      continue;

    update_xfx(flav_proc[0], x1, mu2, xfx1);
    update_xfx(flav_proc[1], x2, mu2, xfx2);

    double xfx_products_sum {0.0};
    double abs_xfx_products_sum {0.0};
    for (int j {0}; j < n_channels; ++j) {
      xfx_products[j] = xfx1[pdf_id(flav_proc[j * n_ptcl])] *
                        xfx2[pdf_id(flav_proc[j * n_ptcl + 1])];
      xfx_products_sum += xfx_products[j];
      abs_xfx_products_sum += std::abs(xfx_products[j]);
    }

    evt.host.w(i) *= (xfx_products_sum) / (x1 * x1 * x2 * x2);

    // select flavour process channel according to their PDF weight
    const double r {evt.host.r_flav_proc(i)};
    double selector {0.0};
    for (int j {0}; j < n_channels; ++j) {
      selector += std::abs(xfx_products[j]) / abs_xfx_products_sum;
      if (selector > r) {
        evt.set_flav_channel(i, j);
        evt.host.xfx1(i) = xfx1[pdf_id(flav_proc[j * n_ptcl])];
        evt.host.xfx2(i) = xfx2[pdf_id(flav_proc[j * n_ptcl + 1])];
        break;
      }
    }
  }
#endif
}

void Pdf_and_alpha_s::update_xfx(Ptcl_num p, double x, double mu2,
                                 std::vector<double>& xfx) const
{
  if (dummy_pdf_enabled) {
    std::fill(xfx.begin(), xfx.end(), x);
    return;
  }
#if !LHAPDF_FOUND
  // we should never arrive here if LHAPDF is not used; only a dummy PDF set is
  // allowed when being configured without LHAPDF; this should be ensured in the
  // ctor
  (void)p;
  (void)mu2;
  assert(false);
#else
  assert(gridpdf);
  /// fill xfx vectors in the PDF ID order [-6, -5, ..., -1, 21, 1, ... 5, 6],
  /// i.e. quark PDF values will be at vector index pid+6 and the gluon at
  /// index 6.
  if (p == GLUON) {
    xfx[6] = gridpdf->xfxQ2(GLUON, x, mu2);
  }
  else {
    gridpdf->xfxQ2(x, mu2, xfx);
  }
#endif
}
