// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_EXTERNAL_STATE_BUILDERS_H
#define PEPPER_EXTERNAL_STATE_BUILDERS_H

#include "pepper/config.h"

#include "sm.h"
#include "weyl.h"


// This corresponds to Eq. (36) in arXiv:0808.3674, evaluating the spinor scalar
// product using the Weyl--van-der-Waerden--Formalism as laid down in
// arXiv:hep-ph/9805445 (using a different Gamma matrices convention!)
// or in
// https://de.m.wikipedia.org/wiki/Spinor-Helizitäts-Formalismus
// which uses the same convention as arXiv:0808.3674.
// Note also that this and all expressions in weyl.h are equivalent to the
// implementation in Sherpa in METOOLS::Polarization_Tools and ATOOLS::Spinor.
KOKKOS_INLINE_FUNCTION
CVec4 build_massless_polarisation_vector(const Vec4& p, Helicity h)
{
  // TODO: On device these should live in global memory
  // Auxiliary vector k used to build polarisation vectors.
  // These correspond to the k^\pm defined in arXiv:0808.3674.
  const Vec4 k {1.0, M_SQRT1_2, M_SQRT1_2, 0.0};
  const Weyl k_plus {Helicity::plus, k};
  const Weyl k_minus {Helicity::minus, k};

  if (h == Helicity::plus) {
    Weyl weyl {Helicity::minus, p};
    return CVec4 {k_plus, weyl} / (M_SQRT2 * Complex::conj(k_minus * weyl));
  }
  else {
    Weyl weyl {Helicity::plus, p};
    return CVec4 {weyl, k_minus} / (M_SQRT2 * Complex::conj(k_plus * weyl));
  }
}

// This is equivalent to the implementation in Sherpa in METOOLS::C_Spinor.C
// for the massless case.
KOKKOS_INLINE_FUNCTION
Weyl build_massless_spinor(const Vec4& p, Helicity h, int ptcl)
{
  // NOTE: In an all-outgoing convention, we can only have v_h or \bar{u}_h spinors.

  // This corresponds to Eqs. (33-35) in arXiv:0808.3674.
  Weyl spinor;
  if ((ptcl > 0) ^ (h == Helicity::minus)) {
    // Construct v-(p)
    Weyl weyl {Helicity::plus, p};
    spinor[0] = weyl[0];
    spinor[1] = weyl[1];
  } else {
    // Construct v+(p)
    Weyl weyl {Helicity::minus, p};
    spinor[0] = weyl[1];
    spinor[1] = -weyl[0];
    // Transforming incoming to outgoing fields requires a sign flip of the
    // contravariant (dotted) Weyl spinors, see arXiv:hep-ph/9805445v1.
    if (p[0] < 0.0) {
      spinor[0] *= -1.0;
      spinor[1] *= -1.0;
    }
  }

  if (ptcl > 0) {
    // This is a \bar{u} spinor, so we must first use u+ == v- or u- == v+, and
    // then apply the \bar.
    spinor.conjugate();
  }

  return spinor;
}

KOKKOS_INLINE_FUNCTION
Scl_cvec4 build_massive_spinor(const Vec4& p, Helicity h, int ptcl)
{
  const Vec4 p_hat {(p[0] < 0 ? -1.0 : 1.0) * p.vec3().abs(), p[1], p[2], p[3]};
  Weyl chi {build_massless_spinor(p_hat, h, ptcl)};
  if (ptcl > 0)
    chi.conjugate();

  const int shift {((ptcl > 0) ^ (h == Helicity::minus)) ? 0 : 2};
  const double sign {(ptcl > 0) ? 1.0 : -1.0};
  const double prefac_plus {
      std::sqrt(std::abs((p[0] + p_hat[0]) / (2.0 * p_hat[0])))};
  const double prefac_minus {
      std::sqrt(std::abs((p[0] - p_hat[0]) / (2.0 * p_hat[0])))};

  Scl_cvec4 spinor;
  spinor[0 + shift] = sign * prefac_minus * chi[0];
  spinor[1 + shift] = sign * prefac_minus * chi[1];
  spinor[2 - shift] = prefac_plus * chi[0];
  spinor[3 - shift] = prefac_plus * chi[1];
  if (ptcl > 0) {
    Complex::swap(spinor[0], spinor[2]);
    Complex::swap(spinor[1], spinor[3]);
    spinor.conjugate();
  }
  // TODO: Understand why the following ad-hoc signs are necessary
  if (p[0] < 0) {
    if (h == Helicity::minus) {
      spinor[0] *= -1.0;
      spinor[1] *= -1.0;
    }
    else {
      spinor[2] *= -1.0;
      spinor[3] *= -1.0;
    }
  }
  return spinor;
}

#endif
