// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_T_CHANNEL_INTEGRATOR
#define PEPPER_T_CHANNEL_INTEGRATOR

#include "pepper/config.h"

#include "Kokkos_Core.hpp"

#include "phase_space.h"
#include "settings.h"
#include "paths.h"
#include "phase_space_manipulator.h"
#include "vegas.h"

#include <filesystem>

class Event_handle;
class Flavour_process;
class Phase_space_manipulator;

namespace detail {
  struct Cuts_data;
}

namespace fs = std::filesystem;

class T_channel : public Phase_space_generator {
public:
  T_channel(const Flavour_process&, const Settings&);
  ~T_channel() override {};

  void fill_momenta_and_weights(Event_handle&) const override;
  void update_weights(Event_handle&) const override;

  int n_random_numbers() const override { return 3 * (n_ptcl - 2) - 4 + 2; }

  void add_training_data(Event_handle&) override;
  void optimise() override;
  void stop_optimisation() override;

  bool is_host_only() const override { return false; }

  void write_to_cache(const std::string& procname) override
  {
    fs::path out_path {Paths::cache + "/t_channel/"};
    fs::create_directories({out_path});
    vegas.WriteOut(out_path.string() + procname + ".vegas");
  }

  void read_from_cache(const std::string& procname) override
  {
    fs::path out_path {Paths::cache + "/t_channel/"};
    fs::create_directories({out_path});
    vegas.ReadIn(out_path.string() + procname + ".vegas");
  }

private:
  int n_ptcl;
  int type;
  mutable Vegas vegas;
  bool should_bias_phase_space {false};
  Kokkos::View<detail::Cuts_data> d_data;
  Kokkos::View<detail::Cuts_data,Kokkos::HostSpace> h_data;
  Kokkos::View<double*> d_etmin;
  std::unique_ptr<Phase_space_manipulator> biaser;
  std::vector<double> etmin;
  Kokkos::View<double*> d_ms;
  std::vector<double> ms;
};

#endif
