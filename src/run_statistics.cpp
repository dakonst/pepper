// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "run_statistics.h"
#include <iomanip>

std::map<std::string, int> Run_statistics::detail::occurrences;

void Run_statistics::print_occurrences(std::ostream& out)
{
  // check if there are any entries, otherwise return early
  bool nonzero_found {false};
  for (auto& kv : detail::occurrences) {
    if (kv.second == 0)
      continue;
    nonzero_found = true;
    break;
  }
  if (!nonzero_found)
    return;

  // find appropriate label column width
  size_t label_column_width {0};
  for (auto& kv : detail::occurrences)
    label_column_width = std::max(label_column_width, kv.first.size());
  label_column_width++;

  // print header
  out << std::left;
  out << std::setw(label_column_width) << "Event,"
      << " Number of occurrences\n";

  // print rows
  for (auto& kv : detail::occurrences)
    out << std::setw(label_column_width) << (kv.first + ",") << ' ' << kv.second
        << '\n';
}
