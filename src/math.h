// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_MATH_H
#define PEPPER_MATH_H

#include "pepper/config.h"

#include <cassert>
#include <vector>

#include "Kokkos_Complex.hpp"
namespace Complex = Kokkos;
using namespace std::complex_literals;
namespace Kokkos{
  template<typename T>
  KOKKOS_INLINE_FUNCTION void swap(T& a, T& b){
    auto aa = a;
    a = b;
    b = aa;
  }
}

using C = Complex::complex<double>;

struct MC_result {
  double sum {0.0};
  double sum2 {0.0};
  long n_trials {0};
  long n_nonzero_after_cuts {0};
  long n_nonzero {0};

  double mean() const;
  double efficiency() const;
  double cut_efficiency() const;
  double unweighting_efficiency() const;
  double kish_sample_size() const;

  // MC estimators for the variance/standard deviation of the estimated mean
  double variance() const;
  double stddev() const;

  // MC estimator for the variance/standard deviation of the function being
  // integrated
  double integrand_variance() const;
  double integrand_stddev() const;

  void all_mpi_sum();

  friend bool operator==(const MC_result& lhs, const MC_result& rhs)
  {
    return (lhs.sum == rhs.sum &&
            lhs.sum2 == rhs.sum2 &&
            lhs.n_trials == rhs.n_trials &&
            lhs.n_nonzero_after_cuts == rhs.n_nonzero_after_cuts &&
            lhs.n_nonzero == rhs.n_nonzero);
  }
  friend bool operator!=(const MC_result& lhs, const MC_result& rhs)
  {
    return !(lhs == rhs);
  }

  MC_result& operator+=(const MC_result&);
  friend MC_result operator+(MC_result lhs, const MC_result& rhs)
  {
    return lhs += rhs;
  }

  void clear() {
    sum = 0.0;
    sum2 = 0.0;
    n_trials = 0;
    n_nonzero_after_cuts = 0;
    n_nonzero = 0;
  }

  // (nicely formatted) stream operation, use the proxy returned by raw()
  // instead to print the data on one line, without any formatting
  friend std::ostream& operator<<(std::ostream& os, const MC_result&);

  // a raw printing wrapper; this is never instantiated explicitly, but only
  // used via the public raw() member function defined below
  struct Raw_printer {
    Raw_printer(MC_result& d) : data {d} {}
    friend std::ostream& operator<<(std::ostream& os, const Raw_printer&);
    friend std::istream& operator>>(std::istream& os, Raw_printer);
    MC_result& data;
  };

  // define raw data printing without any formatting, use it like this:
  // println(mc_result.raw());
  Raw_printer raw() { return Raw_printer {*this}; }
};

struct Value_with_error {
  double value {0.0};
  double error {0.0};
  void clear() {
    value = 0.0;
    error = 0.0;
  }
  friend std::ostream& operator<<(std::ostream& os, const Value_with_error& res)
  {
    return os << res.value << " +- " << res.error;
  }
};


template <typename T> KOKKOS_INLINE_FUNCTION T square(T x) { return x * x; }

KOKKOS_INLINE_FUNCTION int factorial(int n) noexcept
{
  assert(n >= 0);
  int ret {1};
  for (int i {1}; i < n + 1; ++i)
    ret *= i;
  return ret;
}

// Cf. https://stackoverflow.com/questions/44718971/calculate-binomial-coffeficient-very-reliably
constexpr inline size_t binom(size_t n, size_t k) noexcept
{
  return
    (        k> n  )? 0 :          // out of range
    (k==0 || k==n  )? 1 :          // edge
    (k==1 || k==n-1)? n :          // first
    (     k+k < n  )?              // recursive:
    (binom(n-1,k-1) * n)/k :       //  path to k=1   is faster
    (binom(n-1,k) * n)/(n-k);      //  path to k=n-1 is faster
}

std::vector<int> revert_perm(const std::vector<int>&);

// Concatenate permutations, allowing for a.size() >= b.size() >= c.size() =
// returned.size(). -1 can be used as a marker in the input permutations to
// denote entries that are permuted nowhere.
std::vector<int> concat_perms(const std::vector<int>& a,
                              const std::vector<int>& b,
                              const std::vector<int>& c);

template <typename T> struct Squared_multiplies {
  KOKKOS_INLINE_FUNCTION
  T operator()(const T& lhs, const T& rhs) const
  {
    return lhs * lhs * rhs * rhs;
  }
};

#endif
