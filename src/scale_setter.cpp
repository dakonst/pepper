// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "scale_setter.h"
#include "event_handle.h"
#include "math.h"
#include "scale_setter_kernel.h"
#include "sm.h"


/// Scale setter factory function {{{

std::unique_ptr<Scale_setter> create_scale_setter(const std::string& spec)
{
  if (spec == "m_Z^2") {
    return std::make_unique<Fixed_scale_setter>(square(sm_properties::getInstance().hmass(Z)));
  }
  if (spec == "m_t^2") {
    return std::make_unique<Fixed_scale_setter>(square(sm_properties::getInstance().hmass(TOP_QUARK)));
  }
  if (spec == "H_Tp^2") {
    return std::make_unique<Htp2_scale_setter>();
  }
  if (spec == "H_Tp^2/2") {
    return std::make_unique<Htp2_2_scale_setter>();
  }
  if (spec == "H_T^2/2") {
    return std::make_unique<Ht2_2_scale_setter>();
  }
  if (spec == "H_TM^2/2") {
    return std::make_unique<Htm2_2_scale_setter>();
  }
  throw std::invalid_argument {"Unknown scale setter: " + spec};
}


/// Fixed scale setter implementation {{{

Fixed_scale_setter::Fixed_scale_setter(double _mu2) : mu2 {_mu2} {}

void Fixed_scale_setter::fill_scales(Event_handle& evt) const
{
  const auto& _mu2 = mu2;
  RUN_KERNEL(fixed_scale_setter_kernel, evt, _mu2);
}


/// H_Tp^2 scale setter implementation {{{

void Htp2_scale_setter::fill_scales(Event_handle& evt) const
{
  RUN_KERNEL(htp2_scale_setter_kernel, evt);
}

/// H_Tp^2/2 scale setter implementation {{{


void Htp2_2_scale_setter::fill_scales(Event_handle& evt) const
{
  RUN_KERNEL(htp2_2_scale_setter_kernel, evt);
}

/// H_T^2/2 scale setter implementation {{{


void Ht2_2_scale_setter::fill_scales(Event_handle& evt) const
{
  RUN_KERNEL(ht2_2_scale_setter_kernel, evt);
}


/// H_{T,M}^2/2 scale setter implementation {{{

void Htm2_2_scale_setter::fill_scales(Event_handle& evt) const
{
  RUN_KERNEL(htm2_2_scale_setter_kernel, evt);
}
