// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "utilities.h"
#include <algorithm>

using namespace std;

bool ends_with(const string& str, const string& end)
{
  if (end.size() > str.size())
    return false;
  return (str.substr(str.size() - end.size()) == end);
}

bool case_insensitive_ends_with(const string& str, const string& end)
{
  if (end.size() > str.size())
    return false;
  return (case_insensitive_equal(str.substr(str.size() - end.size()), end));
}

bool begins_with(const string& str, const string& begin)
{
  if (begin.size() > str.size())
    return false;
  return (str.substr(0, begin.size()) == begin);
}

bool case_insensitive_begins_with(const string& str, const string& begin)
{
  if (begin.size() > str.size())
    return false;
  return (case_insensitive_equal(str.substr(0, begin.size()), begin));
}

size_t replace_all(string& inout, string_view what, string_view with)
{
  size_t count {};
  for (string::size_type pos {};
       inout.npos != (pos = inout.find(what.data(), pos, what.length()));
       pos += with.length(), ++count) {
    inout.replace(pos, what.length(), with.data(), with.length());
  }
  return count;
}

bool case_insensitive_equal(const string& lhs, const string& rhs)
{
  return equal(lhs.begin(), lhs.end(), rhs.begin(), rhs.end(),
               [](char a, char b) { return tolower(a) == tolower(b); });
}

// cf. https://stackoverflow.com/a/217605
void ltrim(std::string& s)
{
  s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
            return !std::isspace(ch);
          }));
}

void rtrim(std::string& s)
{
  s.erase(std::find_if(s.rbegin(), s.rend(),
                       [](unsigned char ch) { return !std::isspace(ch); })
              .base(),
          s.end());
}

// trim from both ends (in place)
void trim(std::string& s)
{
  rtrim(s);
  ltrim(s);
}

bool contains_space(const std::string& s)
{
  return std::find_if(s.begin(), s.end(), [](unsigned char c) {
           return std::isspace(c);
         }) != s.end();
}

std::string to_lowercase(std::string s)
{
  std::transform(s.begin(), s.end(), s.begin(),
                 [](unsigned char c) { return std::tolower(c); });
  return s;
}
