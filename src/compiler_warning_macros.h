// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_COMPILER_WARNING_MACROS_H
#define PEPPER_COMPILER_WARNING_MACROS_H

// NOTE: most of this file has been initially extracted from the doctest header

// =============================================================================
// == COMPILER VERSION =========================================================
// =============================================================================

// ideas for the version stuff are taken from here:
// https://github.com/cxxstuff/cxx_detect

#ifdef _MSC_VER
#define PEPPER_CPLUSPLUS _MSVC_LANG
#else
#define PEPPER_CPLUSPLUS __cplusplus
#endif

#define PEPPER_COMPILER(MAJOR, MINOR, PATCH)                                      \
  ((MAJOR)*10000000 + (MINOR)*100000 + (PATCH))

// GCC/Clang and GCC/MSVC are mutually exclusive, but Clang/MSVC are not because
// of clang-cl...
#if defined(_MSC_VER) && defined(_MSC_FULL_VER)
#if _MSC_VER == _MSC_FULL_VER / 10000
#define PEPPER_MSVC                                                               \
  PEPPER_COMPILER(_MSC_VER / 100, _MSC_VER % 100, _MSC_FULL_VER % 10000)
#else // MSVC
#define PEPPER_MSVC                                                               \
  PEPPER_COMPILER(_MSC_VER / 100, (_MSC_FULL_VER / 100000) % 100,                 \
               _MSC_FULL_VER % 100000)
#endif // MSVC
#endif // MSVC
#if defined(__clang__) && defined(__clang_minor__)
#define PEPPER_CLANG                                                              \
  PEPPER_COMPILER(__clang_major__, __clang_minor__, __clang_patchlevel__)
#elif defined(__GNUC__) && defined(__GNUC_MINOR__) &&                          \
    defined(__GNUC_PATCHLEVEL__) && !defined(__INTEL_COMPILER)
#define PEPPER_GCC PEPPER_COMPILER(__GNUC__, __GNUC_MINOR__, __GNUC_PATCHLEVEL__)
#endif // GCC

#ifndef PEPPER_MSVC
#define PEPPER_MSVC 0
#endif // PEPPER_MSVC
#ifndef PEPPER_CLANG
#define PEPPER_CLANG 0
#endif // PEPPER_CLANG
#ifndef PEPPER_GCC
#define PEPPER_GCC 0
#endif // PEPPER_GCC

// =============================================================================
// == COMPILER WARNINGS HELPERS ================================================
// =============================================================================

#if PEPPER_CLANG
#define PEPPER_PRAGMA_TO_STR(x) _Pragma(#x)
#define PEPPER_CLANG_SUPPRESS_WARNING_PUSH _Pragma("clang diagnostic push")
#define PEPPER_CLANG_SUPPRESS_WARNING(w)                                          \
  PEPPER_PRAGMA_TO_STR(clang diagnostic ignored w)
#define PEPPER_CLANG_SUPPRESS_WARNING_POP _Pragma("clang diagnostic pop")
#define PEPPER_CLANG_SUPPRESS_WARNING_WITH_PUSH(w)                                \
  PEPPER_CLANG_SUPPRESS_WARNING_PUSH PEPPER_CLANG_SUPPRESS_WARNING(w)
#else // PEPPER_CLANG
#define PEPPER_CLANG_SUPPRESS_WARNING_PUSH
#define PEPPER_CLANG_SUPPRESS_WARNING(w)
#define PEPPER_CLANG_SUPPRESS_WARNING_POP
#define PEPPER_CLANG_SUPPRESS_WARNING_WITH_PUSH(w)
#endif // PEPPER_CLANG

#if PEPPER_GCC
#define PEPPER_PRAGMA_TO_STR(x) _Pragma(#x)
#define PEPPER_GCC_SUPPRESS_WARNING_PUSH _Pragma("GCC diagnostic push")
#define PEPPER_GCC_SUPPRESS_WARNING(w) PEPPER_PRAGMA_TO_STR(GCC diagnostic ignored w)
#define PEPPER_GCC_SUPPRESS_WARNING_POP _Pragma("GCC diagnostic pop")
#define PEPPER_GCC_SUPPRESS_WARNING_WITH_PUSH(w)                                  \
  PEPPER_GCC_SUPPRESS_WARNING_PUSH PEPPER_GCC_SUPPRESS_WARNING(w)
#else // PEPPER_GCC
#define PEPPER_GCC_SUPPRESS_WARNING_PUSH
#define PEPPER_GCC_SUPPRESS_WARNING(w)
#define PEPPER_GCC_SUPPRESS_WARNING_POP
#define PEPPER_GCC_SUPPRESS_WARNING_WITH_PUSH(w)
#endif // PEPPER_GCC

#if PEPPER_MSVC
#define PEPPER_MSVC_SUPPRESS_WARNING_PUSH __pragma(warning(push))
#define PEPPER_MSVC_SUPPRESS_WARNING(w) __pragma(warning(disable : w))
#define PEPPER_MSVC_SUPPRESS_WARNING_POP __pragma(warning(pop))
#define PEPPER_MSVC_SUPPRESS_WARNING_WITH_PUSH(w)                                 \
  PEPPER_MSVC_SUPPRESS_WARNING_PUSH PEPPER_MSVC_SUPPRESS_WARNING(w)
#else // PEPPER_MSVC
#define PEPPER_MSVC_SUPPRESS_WARNING_PUSH
#define PEPPER_MSVC_SUPPRESS_WARNING(w)
#define PEPPER_MSVC_SUPPRESS_WARNING_POP
#define PEPPER_MSVC_SUPPRESS_WARNING_WITH_PUSH(w)
#endif // PEPPER_MSVC

// =============================================================================
// == COMPILER WARNINGS ========================================================
// =============================================================================

// define suppressed common warnings for GCC 9
#if PEPPER_GCC >= PEPPER_COMPILER(9, 0, 0)
#define PEPPER_GCC_9_SUPPRESS_COMMON_WARNINGS                                     \
  PEPPER_GCC_SUPPRESS_WARNING("-Wdeprecated-copy")
#else
#define PEPPER_GCC_9_SUPPRESS_COMMON_WARNINGS
#endif
// combined suppressed common warnings for all GCC versions
// NOTE: "-Wunknown-pragmas" only works for GCC 13 or newer, see
// https://gcc.gnu.org/bugzilla/show_bug.cgi?id=53431
#define PEPPER_GCC_SUPPRESS_COMMON_WARNINGS                                       \
  PEPPER_GCC_9_SUPPRESS_COMMON_WARNINGS                                           \
  PEPPER_GCC_SUPPRESS_WARNING("-Wattributes")                                     \
  PEPPER_GCC_SUPPRESS_WARNING("-Wshadow")                                         \
  PEPPER_GCC_SUPPRESS_WARNING("-Wsign-compare")                                   \
  PEPPER_GCC_SUPPRESS_WARNING("-Wunknown-pragmas")                                \
  PEPPER_GCC_SUPPRESS_WARNING("-Wunused-local-typedefs")


// both the header and the implementation suppress all of these,
// so it only makes sense to aggregrate them like so
#define PEPPER_SUPPRESS_COMMON_WARNINGS_PUSH                                      \
  PEPPER_CLANG_SUPPRESS_WARNING_PUSH                                              \
  PEPPER_CLANG_SUPPRESS_WARNING("-Wshadow")                                       \
  PEPPER_CLANG_SUPPRESS_WARNING("-Wdeprecated-copy")                              \
                                                                                  \
  PEPPER_GCC_SUPPRESS_WARNING_PUSH                                                \
  PEPPER_GCC_SUPPRESS_COMMON_WARNINGS                                             \
                                                                                  \
  PEPPER_MSVC_SUPPRESS_WARNING_PUSH                                               \
  /* local declaration of <variable> hides previous declaration at <line> of      \
   * <file> */                                                                    \
  PEPPER_MSVC_SUPPRESS_WARNING(6244)

#define PEPPER_SUPPRESS_COMMON_WARNINGS_POP                                       \
  PEPPER_CLANG_SUPPRESS_WARNING_POP                                               \
  PEPPER_GCC_SUPPRESS_WARNING_POP                                                 \
  PEPPER_MSVC_SUPPRESS_WARNING_POP

#endif
