// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2024 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "hdf5_phase_space_reader.h"
#include "event_handle.h"
#include "mpi.h"
#include "flavour_process.h"
#include "paths.h"
#include "settings.h"
#include "timing.h"

using namespace Paths;
using namespace HighFive;

Hdf5_phase_space_reader::Hdf5_phase_space_reader(const Flavour_process& proc,
                                                 const Settings& s)
    : Phase_space_generator {s.e_cms}
{
  // for now, our implementation assumes that only a single partonic channel
  // exists

  // the input filename is given by the "generator" setting
  const std::string& filename {s.phase_space_settings.generator};

  Timer timer;
#if MPI_FOUND && H5_HAVE_PARALLEL
  INFO("Phase-space points will be read collectively from \"", filename, "\"");
  HighFive::FileAccessProps props;
  props.add(MPIOFileAccess {MPI_COMM_WORLD, MPI_INFO_NULL});
  props.add(MPIOCollectiveMetadata {});
  file = std::make_unique<File>(filename, File::ReadOnly, props);
  transfer_props.add(UseCollectiveIO {});
#else
  INFO("Phase-space points will be read from \"", filename, "\"");
  file = std::make_unique<File>(path_with_rank_info(filename), File::ReadOnly);
#endif
  timer.register_elapsed({Timing::Task::Hdf5_phase_space_points_open});

  // determine event range for this rank
  const size_t n_events_all_ranks {
      file->getDataSet("events").getSpace().getDimensions().front()};
  const int n_ranks {Mpi::n_ranks()};
  const int rank {Mpi::rank()};
  const size_t first_event {rank * n_events_all_ranks / n_ranks};
  size_t last_event {(rank + 1) * n_events_all_ranks / n_ranks - 1};
  if (rank == n_ranks - 1)
    last_event = n_events_all_ranks - 1;
  const size_t n_events {last_event - first_event + 1};

  // in the following, read all events (later we might only read a chunk every
  // now and then, and move the logic to fill_momenta_and_weights accordingly)

  // read all events
  HighFive::DataSet events_data {file->getDataSet("events")};
  std::vector<size_t> eoffsets {first_event, 0};
  std::vector<size_t> ecounts {n_events, 10};
  events_data.select(eoffsets, ecounts).read(events, transfer_props);

  // read all particle information
  HighFive::DataSet particles_data(file->getDataSet("particles"));
  std::vector<size_t> poffsets {(size_t)events.front()[2], 0};
  size_t nps(proc.n_ptcl());
  std::vector<size_t> pcounts {n_events * nps, 13};
  particles_data.select(poffsets, pcounts).read(particles, transfer_props);
}

void Hdf5_phase_space_reader::fill_momenta_and_weights(Event_handle& evt) const
{
  const int batch_size {evt.host.batch_size};
  for (int i {0}; i < batch_size; ++i) {
    const int eidx {i + n_events_read};
    evt.host.w(i) = events[eidx][9];
    for (int p {0}; p < evt.host.n_ptcl; ++p) {
      const int pidx {static_cast<int>(events[eidx][2] - events[0][2] + p)};
      const double prefac {(p < 2) ? -1.0 : 1.0};
      evt.host.px(i, p) = prefac * particles[pidx][6];
      evt.host.py(i, p) = prefac * particles[pidx][7];
      evt.host.pz(i, p) = prefac * particles[pidx][8];
      evt.host.e(i, p) = prefac * particles[pidx][9];
    }
  }
  n_events_read += batch_size;
}
