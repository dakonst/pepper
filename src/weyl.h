// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_WEYL_H
#define PEPPER_WEYL_H


#include "floating_point_comparisons.h"
#include "helicity.h"
#include "vec4.h"
#include <cassert>

struct Weyl : std::array<C, 2> {

  KOKKOS_INLINE_FUNCTION Weyl() {};
  KOKKOS_INLINE_FUNCTION Weyl(Helicity h, const Vec4& p) : representation {h}
  {
    // For h == plus, we have
    //
    //   pi^a = \frac{1}{\sqrt{p^+} ( p^+, p_\perp )
    //
    //  and for h == minus, we have
    //
    //   \tilde{pi}^{\dot a} = \frac{1}{\sqrt{p^+} ( p^+, p_\perp^* )
    //
    // in the notation of
    // https://de.m.wikipedia.org/wiki/Spinor-Helizitäts-Formalismus. Note further
    // that p_\perp / sqrt{p^+} = sqrt{p^-} e^{i \phi} with the azimuthal angle
    // \phi. If the x and y entries are close to zero, the angle is ill-defined,
    // and we set it to zero. This is the same treatment as in Sherpa
    // in ATOOLS::Spinor. The implementation here is equivalent to that.

    // Calculate first entry, which is always sqrt{p^+}.
    const double plus {p.p_plus()};
    const double minus {p.p_minus()};
    const C sqrt_plus {Complex::sqrt(C {plus})};
    (*this)[0] = sqrt_plus;

    const C perp {p[1], p[2]};

    const double max_diff {std::abs(p[0]) * 1e-12};
    if (!almost_zero(perp, max_diff) && !almost_zero(sqrt_plus, max_diff)) {
      if (representation == Helicity::plus)
        (*this)[1] = perp / sqrt_plus;
      else
        (*this)[1] = Complex::conj(perp) / sqrt_plus;
    }
    else {
      (*this)[1] = Complex::sqrt(C {p.p_minus()});
    }

    // Use the same phase convention as in Sherpa.
    // Note that this transformation corresponds to Eq. (1.74) in Henn's book
    // "Scattering Amplitudes in Gauge Theories".
    if (plus < 0.0 || minus < 0.0) {
      if (representation == Helicity::minus) {
        (*this)[0] = C(0,1) * (*this)[0];
        (*this)[1] = C(0,1) * (*this)[1];
      }
      else {
        (*this)[0] = -C(0,1) * (*this)[0];
        (*this)[1] = -C(0,1) * (*this)[1];
      }
    }
  }

  KOKKOS_INLINE_FUNCTION friend C operator*(const Weyl& a, const Weyl& b)
  {
    assert(a.representation == b.representation);
    return a[0] * b[1] - a[1] * b[0];
  }

  KOKKOS_INLINE_FUNCTION void conjugate()
  {
    for (int i {0}; i < 2; ++i)
      (*this)[i] = Complex::conj((*this)[i]);
  }

  Helicity representation {Helicity::unset};
};

#endif
