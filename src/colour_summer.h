// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef BG_COLOUR_SUMMER_H
#define BG_COLOUR_SUMMER_H

#include "colour_factors.h"

class Event_handle;
class Flavour_process;
struct Helicity_settings;

class Colour_summer {
public:
  Colour_summer(const Flavour_process&, int _n_perm, const Settings&);

  void reset_me2(const Event_handle& evt) const;
  void update_me2(const Event_handle& evt, const Helicity_settings& hel_settings,
                  double prefactor) const;

  // select a permutation according to the relative size of the |ME_j|^2, where
  // j enumerates the permuations; upon return, me_real data is replaced with
  // these |me_j|^2 and hence not longer usable
  void select_leading_colour_configuration(const Event_handle&,
                                           const Helicity_settings&) const;

private:
  int n_perm;
  Colour_factors cf;
};

#endif
