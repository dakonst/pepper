// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef GENERATOR_KERNEL_H
#define GENERATOR_KERNEL_H

#include "event_handle.h"
#include "kernel_macros.h"

template<typename event_data>
KOKKOS_INLINE_FUNCTION void unweight_kernel(const event_data& evt, int i,
                                          double w_max)
{
  if (evt.w(i) == 0.0) {
    return;
  }
  double rel_w {evt.w(i) * evt.me2(i) * evt.ps_bias(i) / w_max};
  double sign {rel_w < 0.0 ? -1.0 : 1.0};
  rel_w = std::abs(rel_w);
  if (rel_w > 1.0)
    evt.record_overweight(rel_w);
  if (evt.r_unweighting(i) < rel_w) {
    // TODO: Because w_max and me2 include the selection weight factor from the
    // process selection, we can not set w = w_max / me2, because this factor
    // would cancel out then. However, we can set me2 = w_max / w. Note that
    // this is a pathology of our somewhat awkward w vs. me2 handling. As noted
    // in at least one other todo item, we should probably make sure that w is
    // the full event weight, and me2 only the colour-summed matrix element
    // (helicity sampled/summed), but nothing else. When cleaning it up like
    // this, we should revisit this particular issue.
    evt.me2(i) = sign * std::max(1.0, rel_w) * w_max / evt.w(i) / evt.ps_bias(i);
  }
  else {
    evt.w(i) = 0.0;
  }
}
DEFINE_CUDA_KERNEL_NARGS_1(unweight_kernel, double, w_max);

#endif
