// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "vegas.h"
#include "vegas_kernel.h"
#include "print.h"

#include <iostream>
#include <fstream>
#include <cmath>

Vegas::Vegas(int dim, int ndx, double alpha)
    : m_nd {ndx},
      m_dim {dim},
      m_alpha {alpha},
      d_p_xi(Kokkos::ViewAllocateWithoutInitializing("p_xi"),m_dim,m_nd),
      d_d(Kokkos::ViewAllocateWithoutInitializing("d"),m_dim,m_nd),
      d_hit(Kokkos::ViewAllocateWithoutInitializing("hit"),m_dim,m_nd),
      h_p_xi(Kokkos::create_mirror_view(d_p_xi)),
      h_d(Kokkos::create_mirror_view(d_d)),
      h_hit(Kokkos::create_mirror_view(d_hit))
{

  Kokkos::deep_copy(h_p_xi,1.);
  Kokkos::deep_copy(h_d,0.);
  Kokkos::deep_copy(h_hit,0.);

  std::vector<double> r(m_nd, 1.0);
  for (int i = 0; i < m_dim; i++)
    Rebin(1. / double(m_nd), Kokkos::subview(h_p_xi,i,Kokkos::ALL), r);
  m_nc = pow(double(m_nd), double(m_dim));

  Reset();
}


void Vegas::GeneratePoint(Event_handle& evt)
{
  const auto& p_xi = d_p_xi;
  const auto& nd = m_nd;
  const auto& dim = m_dim;
  RUN_KERNEL(map_random_numbers, evt, p_xi, nd, dim);
  Kokkos::fence(__func__);
}

void Vegas::GenerateWeight(Event_handle& evt) const
{
  const auto& p_xi = d_p_xi;
  const auto& nd = m_nd;
  const auto& dim = m_dim;
  const auto& nc = m_nc;
  RUN_KERNEL(generate_weight, evt, p_xi, nd, dim, nc);
}

void Vegas::AddPoint(Event_handle& evt)
{
  const auto& p_xi = d_p_xi;
  const auto& d = d_d;
  const auto& hit = d_hit;
  const auto& nd = m_nd;
  const auto& dim = m_dim;
  RUN_KERNEL(fill_grids_kernel, evt, p_xi, d, hit, nd, dim);
}

void Vegas::Push_everything_to_device() {
  Kokkos::deep_copy(d_p_xi,h_p_xi);
  Kokkos::deep_copy(d_d,h_d);
  Kokkos::deep_copy(d_hit,h_hit);
}


void Vegas::Pull_everything_from_device() {
  Kokkos::deep_copy(h_p_xi,d_p_xi);
  Kokkos::deep_copy(h_d,d_d);
  Kokkos::deep_copy(h_hit,d_hit);
}


void Vegas::Reset()
{
  Kokkos::deep_copy(h_d,0.);
  Kokkos::deep_copy(h_hit,0.);
  
  Push_everything_to_device();
}

void Vegas::Optimize()
{
  // pull everything required from device
  Pull_everything_from_device();

  auto serialized_vegas_bins {Serialize()};
  Mpi::sum_d(serialized_vegas_bins);

  int total_hits {0};
  if(Mpi::is_main_rank()){

    DeSerialize(serialized_vegas_bins);

    // Check if the vegas has been filled properly
    for (int j=0;j<m_dim;++j) {
      for (int i=0;i<m_nd;++i) {
        total_hits += h_hit(j,i);
      }
    }
    // If there are not enough points in the grids, we
    // wait with the training for another round
    if(total_hits < 200) {
      WARN("Skipping Vegas optimisation: too few points");
    }
    else {
      for (int j=0;j<m_dim;++j) {
        for (int i=0;i<m_nd;++i) {
          if (h_hit(j,i)) h_d(j,i)/=h_hit(j,i);
          if (h_hit(j,i)<2) h_hit(j,i)=2;
        }
      }
      double xo,xn,rc;
      std::vector<double> p_dt(m_dim,0.);
      for (int j=0;j<m_dim;j++) {
        xo=h_d(j,0);
        xn=h_d(j,1);
        h_d(j,0)=(xo+xn)/2.0;
        p_dt[j]=h_d(j,0);
        for (int i=1;i<m_nd-1;i++) {
          rc=xo+xn;
          xo=xn;
          xn=h_d(j,i+1);
          h_d(j,i) = (rc+xn)/3.0;
          p_dt[j] += h_d(j,i);
        }
        h_d(j,m_nd-1)=(xo+xn)/2.0;
        p_dt[j] += h_d(j,m_nd-1);
      }
      std::vector<double> p_r(m_nd);
      for (int j=0;j<m_dim;j++) {
        rc=0.0;
        for (int i=0;i<m_nd;i++) {
          if (h_d(j,i) < p_dt[j]*1.e-10) h_d(j,i)=p_dt[j]*1.e-10;
          p_r[i]=pow((1.0-h_d(j,i)/p_dt[j])/
            (log(p_dt[j])-log(h_d(j,i))),m_alpha);
          rc += p_r[i];
        }
        Rebin(rc/double(m_nd),Kokkos::subview(h_p_xi,j,Kokkos::ALL),p_r);
      }
    }
  }
  Mpi::cast_i(total_hits);
  if(total_hits > 200) {
    Reset();
  }

  Sync_grids();
}

void Vegas::Sync_grids() {
  // After performing the training, the vegas grids should
  // synchronised with all mpi ranks
#if MPI_FOUND
  std::vector<double> x(m_dim*m_nd);
  if(Mpi::is_main_rank()){
    for (int j=0;j<m_dim;++j)
      for (int i=0;i<m_nd;++i)
        x[j*m_nd+i]=h_p_xi(j,i);
  }

  Mpi::cast_d(x);

  for (int j=0;j<m_dim;++j)
    for (int i=0;i<m_nd;++i)
      h_p_xi(j,i)=x[j*m_nd+i];
  Push_grid_to_device();
#endif
}

void Vegas::WriteOut(const std::string &pid)
{
  std::ofstream ofile(pid);
  ofile<<m_dim<<" "<<m_nd<<std::endl;
  ofile.precision(12);
  for (int i=0;i<m_dim;i++) {
    ofile<<"(";
    for (int j=0;j<m_nd;j++) {
      if (j!=0) ofile<<",";
      ofile<<h_p_xi(i,j);
    }
    ofile<<")"<<std::endl;
  }
}

void Vegas::ReadIn(const std::string &pid)
{
  std::ifstream ifile(pid);
  int dim, nd;
  ifile>>dim>>nd;
  if (dim!=m_dim || nd!=m_nd) return;
  std::string buffer;
  getline(ifile,buffer);
  for (int i=0;i<m_dim;++i) {
    getline(ifile,buffer);
    size_t  a=buffer.find("(")+1;
    size_t  b=buffer.find(")");
    char * err;
    buffer=buffer.substr(a,b-a);
    for (int j=0;j<m_nd;++j) {
      size_t c=buffer.find(",");
      h_p_xi(i,j)=strtod(buffer.substr(0,c).c_str(),&err);
      buffer=buffer.substr(c+1);
    }
  }
  Reset();
  Push_grid_to_device();
}

void Vegas::Push_grid_to_device() {
  // TODO: I'm sure this could be done nicely using thrust::vectors
  // but I failed
  Kokkos::deep_copy(d_p_xi,h_p_xi);
}

std::vector<double> Vegas::Serialize() const
{
  std::vector<double> x(2*m_dim*m_nd);
#if MPI_FOUND
  for (int j=0;j<m_dim;++j)
    for (int i=0;i<m_nd;++i) {
      x[(0*m_dim+j)*m_nd+i]=h_d(j,i);
      x[(1*m_dim+j)*m_nd+i]=h_hit(j,i);
    }
#endif
  return x;
}

void Vegas::DeSerialize(std::vector<double> &x)
{
#if MPI_FOUND
  for (int j=0;j<m_dim;++j)
    for (int i=0;i<m_nd;++i) {
      h_d(j,i)  =x[(0*m_dim+j)*m_nd+i];
      h_hit(j,i)=x[(1*m_dim+j)*m_nd+i];
    }
#else
  (void)x;
#endif
}

