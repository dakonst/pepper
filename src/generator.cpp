// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "pepper/vec4.h"

#include "generator.h"
#include "mpi.h"
#include "paths.h"
#include "print.h"
#include "process_integrator.h"
#include "progress.h"
#include "settings.h"
#include "shuffle_z_direction.h"
#include "timing.h"
#include <fstream>
#include <iomanip>

Generator::Generator(Settings& s, const SM_parameters& sm)
    : proc {s, sm},
      rng {s.seed_offset},
      cuts {s.cuts},
      hel_conf {s.helicity_settings, proc.n_ptcl()},
      should_precalculate_all_external_states {hel_conf.settings().treatment ==
                                               Helicity_treatment::summed},
      evt {proc, hel_conf.settings(), s.batch_size,
           proc.ps_generator().n_random_numbers()},
      bg {proc.n_ptcl(), sm},
      me2_only {s.me2_only},
      scale_setter {create_scale_setter(s.scale2_spec)},
      pdf_and_alpha_s {s, sm},
      ps_settings {s.phase_space_settings},
      diagnostic_output_enabled {s.diagnostic_output_enabled}
{
  if (s.batch_size * s.n_batches > 0) {
    event_writer = create_event_writer(s, sm);
    if (event_writer && event_writer->prints_leading_colour_information())
      evt.add_required_nonzero_event_data_components(
          Event_data_component::leading_colour_configuration);
  }

  if (scale_setter->is_fixed())
    pdf_and_alpha_s.set_fixed_mu2(scale_setter->fixed_mu2());
}

Generator::~Generator()
{
  using namespace Timing;
  register_duration({Task::Event_generation, Task::Output},
                    write_events_duration);
  register_duration({Task::Event_generation, Task::Recursion},
                    recursion_duration);
  register_duration({Task::Event_generation, Task::Recursion, Task::ME2_update},
                    me2_update_duration);
  register_duration({Task::Event_generation, Task::Phase_space},
                    phase_space_duration);
  register_duration(
      {Task::Event_generation, Task::Phase_space, Task::Momenta_and_weights},
      fill_momenta_and_weights_duration);
  register_duration(
      {Task::Event_generation, Task::Phase_space, Task::Update_weights},
      phase_space_weights_duration);
}

void Generator::optimise()
{
  find_active_helicity_configurations();
  optimise_event_generation();
}

void Generator::find_active_helicity_configurations()
{
  if (proc.has_optimised_active_helicities())
    return;
  Kokkos::Profiling::pushRegion(__func__);

  proc.start_active_helicity_optimisation();

  VERBOSE("");
  STATUS("Run helicity configuration optimisation ...");

  // Make a copy of the main event loop's helicity settings, as we might need
  // to some adjustments next.
  Helicity_settings hel_settings {hel_conf.settings()};

  if (hel_settings.treatment == Helicity_treatment::fixed) {
    // For now, we only optimise to filter out non-zero helicity
    // configuration. Hence, if a fixed configuration is to be used, there is
    // nothing to do.
    proc.stop_active_helicity_optimisation();
    Kokkos::Profiling::popRegion();
    return;
  }

  if (hel_settings.treatment == Helicity_treatment::sampled)
    // If the main event loop is supposed to sample helicities, we have to
    // switch to summing here to ensure that we test all helicities,
    // even with a single test event.
    hel_settings.treatment = Helicity_treatment::summed;

  // Otherwise, we just use the helicity settings of the main event loop.
  // Note that this is essential for cached summing, because
  // we must test both helicity alternatives before deciding
  // if their *sum* gives zero, only then can we safely disable the
  // configuration.

  rng.reset();
  proc.ps_generator().reset_rng();

  // Use custom objects configured for helicity summing and with batches
  // consisting of a single event only, to find all helicities
  // that are zero anyway.
  Helicity_configurations _hel_conf {hel_settings, proc.n_ptcl()};
  Event_handle _evt {proc, hel_settings, 1,
                   proc.ps_generator().n_random_numbers()};

  const bool was_process_summing_enabled {proc.summing_enabled()};
  proc.set_summing_enabled(true);

  for (int b {0}; b < 1; ++b) {
    // TODO: Accidentally, we found that setting all momenta to zero
    // gives NaN MEs for all helicities that should be enabled.
    // Could this be the most numerically stable way even to find them?
    // You can test this by just commenting out the fill line.
    rng.fill(_evt);

    // We can use the same phase-space point for all flavour channels of our
    // process. Hence let's fill it and check cuts once for all of them.
    proc.reset();
    proc.advance(_evt, rng);
    fill_momenta_and_apply_cuts(_evt);
    const int n_passed = _evt.n_nonzero();
    if (!n_passed) {
      b--;
      continue;
    }

    for (proc.reset(); proc.advance(_evt, rng);) {

      _hel_conf.precalculate_all_external_states(_evt);
      std::vector<double> me2(_hel_conf.n_helicities(), 0.0);
      for (_hel_conf.reset(_evt);
           _hel_conf.advance(_evt, proc.helicity_mask());) {
        proc.reset_me2(_evt);
        perform_colour_sum(_evt, _hel_conf);
        _evt.pull_me2_from_device();
        me2[_evt.host.hel_conf_idx(0)] = _evt.host.me2(0);
      }

      for (size_t i {0}; i < me2.size(); ++i) {
        if (Mpi::is_main_rank() && Msg::verbosity >= Msg::Verbosity::debug) {
          print(MARK_DEBUG, " - ", std::setw(3), i, " (");
          for (int p {0}; p < evt.host.n_ptcl; ++p) {
            print(calculate_helicity(i, p));
          }
          print(") = ", std::setw(12), std::left, me2[i], std::right);
        }
        if (me2[i] == 0.0) {
          // Configure the *proper* helicity configurations object to ignore
          // this helicity configuration since the corresponding colour-summed
          // ME evaluates to zero.
          proc.set_helicity_enabled(i, false);
          if (Mpi::is_main_rank() && Msg::verbosity >= Msg::Verbosity::debug) {
            println(" -> disabled");
          }
        }
        else {
          if (Mpi::is_main_rank() && Msg::verbosity >= Msg::Verbosity::debug) {
            println();
          }
        }
        // NOTE: Do not activate helicity configurations that have already been
        // disabled.  This might be the case when cached_summing is used,
        // because the Helicity_configurations object will have already disabled
        // some of its configs.
      }

      if (proc.n_active_helicities() < 1) {
        ERROR("All helicity configurations gave a vanishing amplitude during "
              "optimisation for `" +
              proc.current_uid() +
              "'. This indicates that this process is invalid. "
              "Will exit.");
        exit(1);
      }

      VERBOSE(proc.current_uid(), " -> ",
              proc.n_active_helicities(), " of ", hel_conf.n_helicities(),
              " helicity configurations are active");
    }
  }

  proc.stop_active_helicity_optimisation();

  // reset everything
  proc.set_summing_enabled(was_process_summing_enabled);
  proc.full_reset();

  VERBOSE("Done");
  Kokkos::Profiling::popRegion();
}

void Generator::fill_rng(Event_handle& _evt)
{
  Kokkos::Profiling::pushRegion(__func__);
  rng.fill(_evt);
#if !KOKKOS_LHAPDF_FOUND
  // In case the KOKKOS LHAPDF version is not used, we should fall back to
  // the CPU LHAPDF version
  if (pdf_and_alpha_s.pdf_enabled()) {
    WARN_ONCE("Pulling random numbers from device due to missing device "
              "implementation.");
    evt.pull_random_numbers_from_device();
  }
#endif
  Kokkos::Profiling::popRegion();
}

void Generator::fill_momenta_and_apply_cuts(Event_handle& _evt)
{
  Kokkos::Profiling::pushRegion(__func__);
  Timer timer;
  proc.ps_generator().fill_momenta_and_weights(_evt);
  fill_momenta_and_weights_duration += timer.elapsed();
  if (proc.ps_generator().is_host_only()) {
    // When Pairing pepper with a host-only PS generator, we need to push
    // the momenta and weights to the device for the remaining processing
    _evt.push_momenta_to_device();
    _evt.push_x12_to_device();
    _evt.push_weights_to_device();
  }
  cuts.fill(_evt);
  phase_space_duration += timer.elapsed();
  Kokkos::Profiling::popRegion();
}

void Generator::optimise_event_generation()
{
  if (proc.is_optimised_for_event_generation())
    return;
  Kokkos::Profiling::pushRegion(__func__);

  VERBOSE("");
  STATUS("Optimise event sampling ...");

  proc.start_optimisation();

  const int batch_size {evt.host.batch_size};
  double n_nonzero_min {ps_settings.optimisation.n_nonzero_min};
  n_nonzero_min = rescaled_n_nonzero_min(n_nonzero_min);

  Progress::Optimisation_observer progress_observer {
      ps_settings.optimisation.n_iter,
      ps_settings.optimisation.n_nonzero_min_growth_factor};
  for (int n_iter {0}; n_iter < ps_settings.optimisation.n_iter ||
                       !proc.training_has_saturated();
       n_iter++) {

    proc.start_iteration();

    while (proc.iter_result.n_nonzero < n_nonzero_min) {
      fill_rng(evt);
      proc.reset_me2(evt);

      for (proc.reset(); proc.advance(evt, rng);) {
        fill_momenta_and_apply_cuts(evt);
        int n_passed = evt.n_nonzero();
        // TODO: Add shortcut in case there are zero events that passed the cuts.
        if (n_passed != 0) {
          evaluate_points(evt);
        }
        proc.update_iter_stats(batch_size, n_passed);

        evt.pull_weights_from_device();
        evt.pull_me2_from_device();

        // get helicities
        evt.pull_active_helicity_configuration_indexes_from_device();
        // auto hels = evt.active_hel_conf_indexes();

        // update running Monte-Carlo sums
        for (int i {0}; i < batch_size; ++i) {
          const double wgt {evt.host.w(i) * evt.host.me2(i)};

          // add helicity
          if (n_passed != 0 &&
              hel_conf.settings().treatment == Helicity_treatment::sampled) {
            int b = i / HELICITY_BLOCK_SIZE;
            proc.add_helicity_training_data_to_flavour_process(evt.host.active_hel_conf_idx(b), wgt);
          }
          proc.update_iter_weights(wgt);
        }

        // supply each ps generator with its training data
        if (!me2_only) {
          proc.ps_generator().add_training_data(evt);
        }
      }
    }

    proc.stop_iteration();

    progress_observer.print_info(proc.result, n_iter + 1);
    print_integration_info(progress_observer.elapsed_s());

    n_nonzero_min *= ps_settings.optimisation.n_nonzero_min_growth_factor;
    if (ps_settings.helicity_integrator_enabled &&
	hel_conf.settings().treatment == Helicity_treatment::sampled)
      proc.optimise_helicity_selection_weights();

  } // end iter loop

  // finalise
  proc.set_selection_weights_from_variances();
  proc.stop_optimisation();

  // reset everything
  proc.full_reset();

  VERBOSE("Done");
  Kokkos::Profiling::popRegion();
}

void Generator::setup_and_optimise_event_unweighting()
{
  if (proc.is_optimised_for_event_unweighting())
    return;
  Kokkos::Profiling::pushRegion(__func__);

  VERBOSE("");
  STATUS("Optimise event unweighting ...");

  Process_integrator integrator{proc, diagnostic_output_enabled};
  integrator.start();

  const int batch_size {evt.host.batch_size};
  double n_nonzero_min {ps_settings.integration.n_nonzero_min};
  n_nonzero_min = rescaled_n_nonzero_min(n_nonzero_min);

  Progress::Optimisation_observer progress_observer {
      ps_settings.integration.n_iter,
      ps_settings.integration.n_nonzero_min_growth_factor};
  for (int n_iter {0};
       n_iter < ps_settings.integration.n_iter || !integrator.has_saturated();
       n_iter++) {

    proc.start_iteration();

    while (proc.iter_result.n_nonzero < n_nonzero_min) {
      fill_rng(evt);
      proc.reset_me2(evt);

      for (proc.reset(); proc.advance(evt, rng);) {
        fill_momenta_and_apply_cuts(evt);
        int n_passed = evt.n_nonzero();
        if (n_passed != 0) {
          evaluate_points(evt);
        }
        proc.update_iter_stats(batch_size, n_passed);

        evt.pull_weights_from_device();
        evt.pull_me2_from_device();
	evt.pull_biases_from_device();

        // update running Monte-Carlo sums and fill weight histogram
        for (int i {0}; i < batch_size; ++i) {
          // TODO: Use w (= phase space weight * me2) instead of w (= phase
          // space weight), such that we do not need to multiply w and me2
          // whenever we want to get the full event weight.
          integrator.add_weight(evt.host.w(i) * evt.host.me2(i) *
                                evt.host.ps_bias(i));
        }
      }
    }

    proc.stop_iteration();

    progress_observer.print_info(proc.result, n_iter + 1);
    print_integration_info(progress_observer.elapsed_s());

    n_nonzero_min *= ps_settings.integration.n_nonzero_min_growth_factor;

  } // end iter loop

  integrator.stop();

  VERBOSE("Done");
  Kokkos::Profiling::popRegion();
}

double Generator::rescaled_n_nonzero_min(double n_nonzero_min) const
{
  Kokkos::Profiling::pushRegion(__func__);
  // rescale n_nonzero_min by the number of distinct "buckets" of the
  // Monte-Carlo
  if (ps_settings.helicity_integrator_enabled &&
      hel_conf.settings().treatment == Helicity_treatment::sampled) {
    const int total_n_active_helicities {proc.total_n_active_helicities()};
    VERBOSE("Rescale n_nonzero_min by total number of distinct flavour and "
            "helicity configurations (",
            total_n_active_helicities, "): ", n_nonzero_min, " -> ",
            n_nonzero_min * total_n_active_helicities);
    n_nonzero_min *= total_n_active_helicities;
  }
  else {
    const int n_procs {proc.n_procs()};
    VERBOSE("Rescale n_nonzero_min by number of distinct flavour configurations (",
            n_procs, "): ", n_nonzero_min, " -> ", n_nonzero_min * n_procs);
    n_nonzero_min *= n_procs;
  }
  // rescale n_nonzero_min by the inverse of the number of ranks
  n_nonzero_min /= Mpi::n_ranks();
  Kokkos::Profiling::popRegion();
  return n_nonzero_min;
}

void Generator::print_integration_info(double elapsed_s) const
{
  Kokkos::Profiling::pushRegion(__func__);
  if (Mpi::is_main_rank()) {
    int idx = 0;
    for (const auto& res : proc.partial_results) {
      double rel_unc {1.0};
      if (res.mean() != 0.0)
	rel_unc = res.stddev() / res.mean();
      if (Msg::verbosity > Msg::Verbosity::info) {
	std::cerr << " -> " << proc[idx].uid() << " " << Msg::green
		  << res.mean() << " +- " << res.stddev() << " pb" << Msg::reset
		  << " (" << rel_unc * 100.0 << " %"
		  << ")\n";
      }
      idx++;
    }
  }

  if (Mpi::is_main_rank() && ps_settings.integration_info_output_enabled) {
    const auto oldprecision = std::cout.precision(15);
    std::cout << "sum_w: " << proc.result.sum
              << ", sum_w2: " << proc.result.sum2
              << ", time elapsed: " << elapsed_s << " s"
              << ", n_nonzero: " << proc.result.n_nonzero
              << " of " << proc.result.n_trials << "\n";
    std::cout.precision(oldprecision);
  }
  Kokkos::Profiling::popRegion();
}

MC_result Generator::generate_batches(int n_batches)
{
  Kokkos::Profiling::pushRegion(__func__);
  reset_timing();

  const int batch_size {evt.host.batch_size};
  MC_result result;

  if (batch_size * n_batches == 0) {
    VERBOSE();
    STATUS("Skipping event generation: no events requested");
    Kokkos::Profiling::popRegion();
    return result;
  }

  VERBOSE();
  STATUS("Begin generating ", n_batches, " event batch(es) ...");

  if (event_writer)
    event_writer->initialise(proc, pdf_and_alpha_s);

  rng.reset();
  proc.ps_generator().reset_rng();

  Kokkos::Profiling::pushRegion(std::string(__func__) + "::batch-loop");
  Progress::Event_generation_observer progress_observer {n_batches, 1.0};
  for (int b {0}, n_zero_batches_since_last_write {0}; b < n_batches; ++b) {

    Kokkos::Profiling::pushRegion(std::string("batch_") + std::to_string(b));
    fill_rng(evt);
    proc.reset_me2(evt);

    int n_passed {0}, n_nonzero {0};

    for (proc.reset(); proc.advance(evt, rng);) {

      fill_momenta_and_apply_cuts(evt);
      n_passed = evt.n_nonzero();
      n_nonzero = n_passed;
      if (n_passed != 0) {
        evaluate_points(evt);
      }
      if (proc.unweighter().enabled()) {
        Kokkos::Profiling::pushRegion("unweight");
        proc.unweighter().unweight(evt);
        n_nonzero = evt.n_nonzero();
        Kokkos::Profiling::popRegion();
      }

      // Update running Monte-Carlo sums
      Kokkos::Profiling::pushRegion("xs_sum");
      result.sum += evt.xs_sum();
      result.sum2 += evt.xs2_sum();
      Kokkos::Profiling::popRegion();
    }

    // Update current Monte-Carlo result
    result.n_trials += batch_size;
    result.n_nonzero_after_cuts += n_passed;
    result.n_nonzero += n_nonzero;

    if (event_writer) {
      Kokkos::Profiling::pushRegion("event_writer");
      Timer timer;

      if (n_nonzero != 0 || event_writer->prints_zero_events() ||
          event_writer->wants_zero_batches()) {
        // We usually only write non-zero events to not waste I/O, but if the
        // event writer wants to print zero-weight events or otherwise process
        // all-zero batches, we pass them on to it.

        if (n_nonzero != 0 &&
            event_writer->prints_leading_colour_information()) {
          Kokkos::Profiling::pushRegion("select_leading_colour_configuration");
          proc.colour_summer().select_leading_colour_configuration(
              evt, hel_conf.settings());
          Kokkos::Profiling::popRegion();
        }

        // TODO: In write_n() etc., we use w*me2, which is a bit weird; So do
        // something like w *= me2 above.

        // As we might only write non-zero events, we need to take into account
        // the zeros by multiplying with n_nonzero / n_batch. Since we might
        // have skipped entire batches with zeros only, we have to take them
        // into account here also, by adding
        // n_zero_batches_since_last_event_output * batch_size to calculate
        // n_batch.
        const int n_trials {(1 + n_zero_batches_since_last_write) * batch_size};
        n_zero_batches_since_last_write = 0;

        // Now the weights are correct and we can pass the event batch data to
        // the write.
        int n {batch_size};
        if (event_writer->prints_zero_events()) {
          Kokkos::Profiling::pushRegion("pulls_from_device");
          evt.pull_weights_from_device();
          evt.pull_me2_from_device();
          evt.pull_momenta_from_device();
          evt.pull_scales_from_device();
          evt.pull_alpha_s_from_device();
          evt.pull_flavour_channels();
          if (event_writer->prints_leading_colour_information()) {
            evt.pull_active_lc_conf_from_device();
          }
          Kokkos::Profiling::popRegion();
        }
        else {
          evt.pull_nonzero_events_from_device();
          n = n_nonzero;
        }
        // symmetrise the initial states if necessary
        if (evt.host.ptcl(0) != evt.host.ptcl(1))
          shuffle_z_direction_n(evt, rng, n);
        Kokkos::Profiling::pushRegion("write_n");
        event_writer->write_n(evt, n, result, n_nonzero, n_trials, proc);
        Kokkos::Profiling::popRegion();
      }
      else {
        // We could not pass an all-zero-weight event batch to the event
        // writer, remember this for a correct weight normalisation later.
        n_zero_batches_since_last_write++;
        if (b == n_batches - 1) {
          // We do not want to end with an all-zero batch, as this would break
          // the normalisation of our event writeout. So, try again.
          VERBOSE("Last batch was zero. Continue to end on a batch with at least one non-zero event");
          n_batches++;
        }
      }
      write_events_duration += timer.elapsed();
      Kokkos::Profiling::popRegion(); // end event-writer
    }
    progress_observer.print_info(result, b + 1);
    Kokkos::Profiling::popRegion(); // end batch_N
  }
  Kokkos::Profiling::popRegion(); // end batch_loop

  Kokkos::Profiling::pushRegion("batch-finalize");

  if (Mpi::n_ranks() > 1) {
    result.all_mpi_sum();
    STATUS("Report event generation results for all nodes ...");
    INFO(result);
  }

  if (event_writer)
    event_writer->finalise(result);

  proc.report_run_statistics();

  if (Msg::verbosity >= Msg::Verbosity::verbose) {
    double ps_eff {1.0};
    if (result.n_trials > 0)
      ps_eff = result.n_nonzero_after_cuts / double(result.n_trials);

    double unw_eff {1.0};
    if (result.n_nonzero_after_cuts > 0)
      unw_eff = result.n_nonzero / double(result.n_nonzero_after_cuts);

    VERBOSE(" -> phase-space efficiency: ", ps_eff * 100.0, " %");
    if (proc.unweighter().enabled()) {
      VERBOSE(" -> unweighting efficiency: ", unw_eff * 100.0, " %");
      VERBOSE(" -> no. of overweight events: ", evt.host.n_overweight());
      VERBOSE(" -> max. overweight: ", evt.host.max_overweight());
    }
    double kish {result.kish_sample_size()};
    VERBOSE(" -> Kish sample size: ", kish, " (",
            kish / result.n_nonzero * 100.0, " %)");
  }

  const double event_gen_time_per_event {progress_observer.elapsed_s() /
                                         (n_batches * evt.host.batch_size)};
  INFO(" -> Time per event: ", event_gen_time_per_event, "s (",
       3600.0 / event_gen_time_per_event, " events per hour)");

  VERBOSE("Done");

  Kokkos::Profiling::popRegion(); // end batch-finalize
  Kokkos::Profiling::popRegion(); // end __func__
  return result;
}

double Generator::me2(const std::vector<Pepper::Vec4>& p)
{
  detail::configure_momentum_storage(evt.host);
  for (int ptcl {0}; ptcl < evt.host.n_ptcl; ptcl++)
    evt.host.set_p(0, ptcl, p[ptcl]);
  evt.push_momenta_to_device();

  evt.host.w(0) = 1.0;
  evt.push_weights_to_device();

  proc.reset_me2(evt);
  for (proc.reset(); proc.advance(evt, rng);) {
    scale_setter->fill_scales(evt);
    pdf_and_alpha_s.update_weights(evt, proc);
    if (should_precalculate_all_external_states) {
      hel_conf.precalculate_all_external_states(evt);
    }
    for (hel_conf.reset(evt); hel_conf.advance(evt, proc.helicity_mask());) {
      if (!should_precalculate_all_external_states) {
        hel_conf.precalculate_external_states(evt);
      }
      perform_colour_sum(evt, hel_conf);
    }
  }

  evt.pull_me2_from_device();
  evt.pull_weights_from_device();
  return evt.host.me2(0) * evt.host.w(0);
}

void Generator::evaluate_points(Event_handle& _evt)
{
  Kokkos::Profiling::pushRegion(__func__);
  Kokkos::Profiling::pushRegion("fill_scales");
  scale_setter->fill_scales(_evt);
  Kokkos::Profiling::popRegion();

  Kokkos::Profiling::pushRegion("update_weights");

  // We need to keep track of where the weights have been updated last, on
  // device or on host, in the following, because both the phase-space weight
  // and the pdf/alphas weights might either be host-only or not, depending on
  // the compile-time configuration and the run-time settings.
  // Note that we don't need to bother whether we actually have a host/device
  // split or not here, because the pull/push functions will just do nothgin if
  // there is only a host to begin with.
  bool last_weight_update_on_device {true};

  if (!me2_only) {
    // make sure the weights are where we need them to be
    if (last_weight_update_on_device && proc.ps_generator().is_host_only()) {
      _evt.pull_weights_from_device();
      last_weight_update_on_device = false;
    }
    // update phase-space weights
    Timer timer;
    proc.ps_generator().update_weights(_evt);
    const double duration {timer.elapsed()};
    phase_space_weights_duration += duration;
    phase_space_duration += duration;
  }

  // make sure the weights are where we need them to be
  if (last_weight_update_on_device &&
      !pdf_and_alpha_s.updates_weights_on_device()) {
    _evt.pull_weights_from_device();
    last_weight_update_on_device = false;
  }
  else if (!last_weight_update_on_device &&
           pdf_and_alpha_s.updates_weights_on_device()) {
    _evt.push_weights_to_device();
    last_weight_update_on_device = true;
  }

  // update PDF and AlphaS weights
  pdf_and_alpha_s.update_weights(_evt, proc);

  // push back weights if they have been updated on the host last
  if (!last_weight_update_on_device) {
    _evt.push_weights_to_device();
  }

  Kokkos::Profiling::popRegion();

  if (should_precalculate_all_external_states) {
    Kokkos::Profiling::pushRegion("precalculate_all_external_states");
    hel_conf.precalculate_all_external_states(_evt);
    Kokkos::Profiling::popRegion();
  }

  for (hel_conf.reset(_evt); hel_conf.advance(_evt, proc.helicity_mask());) {

    if (!should_precalculate_all_external_states) {
      Kokkos::Profiling::pushRegion("precalculate_external_states");
      hel_conf.precalculate_external_states(_evt);
      Kokkos::Profiling::popRegion();
    }

    perform_colour_sum(_evt, hel_conf);
  }
  if (!me2_only &&
      (hel_conf.settings().treatment == Helicity_treatment::sampled)) {
    Kokkos::Profiling::pushRegion("update_helicity_weights");
    proc.update_helicity_weights(_evt);
    Kokkos::Profiling::popRegion();
  }
  Kokkos::Profiling::popRegion(); // end __func__
}

void Generator::perform_colour_sum(Event_handle& _evt,
                                   Helicity_configurations& _hel_conf)
{
  Kokkos::Profiling::pushRegion("Generator::perform_colour_sum");
  Timer timer;
  for (int qcd_perm_idx {0}, perm_idx {0};
       qcd_perm_idx < proc.permutations().n_qcd(); ++qcd_perm_idx) {
    bg.reset_matrix_elements(_evt, _hel_conf, qcd_perm_idx);
    for (int qed_perm_idx {0};
         qed_perm_idx < proc.permutations().n_qed(qcd_perm_idx);
         ++qed_perm_idx, ++perm_idx) {
      bg.prepare(_evt, _hel_conf, proc.permutations(), perm_idx);
      bg.reset_internal_currents(_evt);
      bg.perform(_evt);
      bg.update_matrix_elements(_evt, _hel_conf, proc.permutations(),
                                qcd_perm_idx);
    }
  }
  Timer timer_me2;
  proc.colour_summer().update_me2(_evt, _hel_conf.settings(), prefactor());
  me2_update_duration += timer_me2.elapsed();
  recursion_duration += timer.elapsed();
  Kokkos::Profiling::popRegion();
}

double Generator::prefactor() const
{
  Kokkos::Profiling::pushRegion(__func__);
  double prefac {1.0};
  prefac *= proc.averaging_factor(hel_conf.settings());
  prefac *= proc.final_state_symmetry_factor();
  prefac *= proc.sampling_factor();
  prefac *= hel_conf.sampling_factor(proc.n_active_helicities());
  if (!me2_only) {
    prefac *= proc.ps_generator().flux_factor();
    prefac *= 3.89379656e8; // conversion from natural units to GeV
  }
  if (hel_conf.settings().parity_symmetric_summing())
    prefac *= 2.0;
  if (pdf_and_alpha_s.pdf_enabled())
    prefac *= proc.initial_state_symmetry_factor();
  Kokkos::Profiling::popRegion();
  return prefac;
}

void Generator::reset_timing() const
{
  recursion_duration = 0.0;
  phase_space_duration = 0.0;
  evaluate_points_duration = 0.0;
  fill_momenta_and_weights_duration = 0.0;
  phase_space_weights_duration = 0.0;
  bg.reset_timing();
  cuts.reset_timing();
  proc.ps_generator().reset_timing();
  hel_conf.reset_timing();
  rng.reset_timing();
  pdf_and_alpha_s.reset_timing();
}
