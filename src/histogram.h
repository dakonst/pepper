// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_HISTOGRAM_H
#define PEPPER_HISTOGRAM_H

#include <boost/histogram.hpp>
#include <boost/histogram/make_histogram.hpp>

// adapted from
// https://www.boost.org/doc/libs/master/libs/histogram/doc/html/histogram/getting_started.html#histogram.getting_started.making_classes_that_hold_histogr
namespace detail {
using Weight_histogram_axes_t = std::tuple<boost::histogram::axis::regular<
    double, boost::histogram::axis::transform::log>>;
}
using Weight_histogram = boost::histogram::histogram<detail::Weight_histogram_axes_t>;

inline Weight_histogram make_weight_histogram()
{
  using namespace boost::histogram;
  return make_histogram(
      axis::regular<double, axis::transform::log> {100, 1.0e-4, 1.0e6});
}

#endif
