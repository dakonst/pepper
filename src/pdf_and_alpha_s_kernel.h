// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PDF_AND_ALPHA_S_KERNEL_H
#define PDF_AND_ALPHA_S_KERNEL_H

#include "Kokkos_Core.hpp"
#include "event_data.h"
#include "kernel_macros.h"

struct Combine_xfx_products {
  KOKKOS_INLINE_FUNCTION
  void operator()(double& a, double b, double c) { a += b * c; }
};

struct Combine_abs_xfx_products {
  KOKKOS_INLINE_FUNCTION
  void operator()(double& a, double b, double c) { a += Kokkos::abs(b * c); }
};

template<typename event_data, typename xfx_type>
KOKKOS_INLINE_FUNCTION
void combine_xfxs(const event_data& evt, int i, const xfx_type& xfx_products) {
  xfx_products(i) += evt.xfx1(i)*evt.xfx2(i);
}
DEFINE_CUDA_KERNEL_NARGS_1(combine_xfxs, double*, xfx_products)

template<typename event_data, typename xfx_type>
KOKKOS_INLINE_FUNCTION
void combine_abs_xfxs(const event_data& evt, int i,
                      const xfx_type& abs_xfx_products) {
  abs_xfx_products(i) += Kokkos::abs(evt.xfx1(i)*evt.xfx2(i));
}
DEFINE_CUDA_KERNEL_NARGS_1(combine_abs_xfxs, double*, abs_xfx_products)


template<typename event_data, typename xfx_type, typename abs_xfx_type>
KOKKOS_INLINE_FUNCTION
void apply_pdf_to_weight_and_prepare_flavour_channel_selection(
      const event_data& evt, int i,
      const xfx_type& xfx_prods,
      const abs_xfx_type& abs_xfx_prods)
{
  if (evt.w(i) != 0) {
    // apply weight
    evt.w(i) *= xfx_prods(i) / (evt.x1(i) * evt.x1(i) * evt.x2(i) * evt.x2(i));
    // prepare flavour channel selection
    abs_xfx_prods(i) *= evt.r_flav_proc(i);
    evt.flav_channels(i) = -1;
  }
}
DEFINE_CUDA_KERNEL_NARGS_2(
    apply_pdf_to_weight_and_prepare_flavour_channel_selection, double*,
    xfx_prods, double*, abs_xfx_prods)

template<typename event_data, typename abs_xfx_type>
KOKKOS_INLINE_FUNCTION
void flavour_channel_selection_kernel(const event_data& evt, int i,
                                      abs_xfx_type& abs_xfx_prods, int channel_idx)
{
  if (evt.w(i) != 0 && evt.flav_channels(i) == -1) {
    abs_xfx_prods(i) -= Kokkos::abs(evt.xfx1(i) * evt.xfx2(i));
    if (abs_xfx_prods[i] < 0.0) {
      evt.flav_channels(i) = channel_idx;
    }
  }
}
DEFINE_CUDA_KERNEL_NARGS_2(flavour_channel_selection_kernel, double*, xfx_prods,
                           int, channel_idx)

template<typename event_data>
KOKKOS_INLINE_FUNCTION
void compute_coupling_factor_kernel(const event_data& evt, int i,
                                    const double g_em, const int _n_lepton_pairs)
{
  if(evt.w(i) == 0.0)
    return;

  // directly taken from Flavour_process::coupling_factor
  const double g_s {Kokkos::sqrt(4.0 * M_PI * evt.alpha_s(i))};
  const double fac1 {Kokkos::pow(g_s, 2 * (evt.n_ptcl - 2 * _n_lepton_pairs) - 4)};
  const double fac {fac1*Kokkos::pow(g_em, 4 * _n_lepton_pairs)};

  evt.w(i) *= fac;
}
DEFINE_CUDA_KERNEL_NARGS_2(compute_coupling_factor_kernel, const double, g_em,
                           const int, _n_lepton_pairs)


template<typename event_data>
KOKKOS_INLINE_FUNCTION
void compute_fixed_coupling_factor_kernel(const event_data& evt, int i,
						 double alphas, double g_em,
						 int _n_lepton_pairs)
{
  // directly taken from Flavour_process::coupling_factor
  const double g_s {Kokkos::sqrt(4.0*M_PI*alphas)};
  const double fac1 {Kokkos::pow(g_s, 2 * (evt.n_ptcl() - 2 * _n_lepton_pairs) - 4)};
  const double fac {fac1*Kokkos::pow(g_em, 4 * _n_lepton_pairs)};

  // TODO: remove once LHAPDF is properly debugged
  if(Kokkos::isnan(fac)||Kokkos::isinf(fac)) {
    return;
  }
  if(evt.w(i) != 0)
    evt.w(i) *= fac;

  // TODO: Should add this to device event data
  // evt.alpha_s(i) = alphas[i];
}
DEFINE_CUDA_KERNEL_NARGS_3(compute_fixed_coupling_factor_kernel, double, alphas,
			   double, g_em, int, _n_lepton_pairs)

template<typename event_data>
KOKKOS_INLINE_FUNCTION
void reset_flavour_channel(const event_data& evt, int i)
{
  evt.flav_channels(i) = -1;
}
DEFINE_CUDA_KERNEL_NARGS_0(reset_flavour_channel)

#endif
