// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_KERNEL_MACROS_H
#define PEPPER_KERNEL_MACROS_H

#include "pepper/config.h"

// All of the below is set up to eventually use two macros in client code.
// First, we want to define compute kernels that can run on the host or the
// device like this:
//
//    inline void KOKKOS_INLINE_FUNCTION
//    some_kernel(KERNEL_EVT_DATA_T& evt, int i, Arg1_Type arg1, ..., ArgN_Type argN)
//    {
//      // Do something with evt
//    }
//    DEFINE_KERNEL_NARGS_N(some_kernel, Arg1_Type arg1, ..., ArgN_Type argN)
//
//  Second, we want to use this kernel like this:
//
//    RUN_KERNEL(some_kernel, evt, arg1, ...);
//
//  The above is for running a kernel looping/paralellising over events.
//  There is also the possibility to do that over helicity blocks instead:
//
//    inline void KOKKOS_INLINE_FUNCTION
//    some_kernel(KERNEL_EVT_DATA_T& evt, int block, Arg1_Type arg1, ..., ArgN_Type argN)
//    {
//      // Do something with block
//    }
//    DEFINE_KERNEL_NARGS_N(some_kernel, Arg1_Type arg1, ..., ArgN_Type argN)
//
//  Then one can use that kernel like this:
//
//    RUN_HELICITY_BLOCK_KERNEL(some_kernel, evt, arg1, ...);

// First, we need a mechanism to take in a kernel name and generate a derived
// CUDA kernel function name from it, e.g. my_great_kernel ->
// d_my_great_kernel.
#define GLUE(a, b) a##b
#define PREPEND_D(KERNEL) GLUE(d_, KERNEL)

// Kernels are used either in host or device code, so we need the right type
// name accordingly.
#define KERNEL_EVT_DATA_T Event_handle

// Convert the kernel name to a string.
#define STR(s) #s

// Now, we define a dispatch-over-events macros, which will usually just
// iterate over all events in a for loop, if there is no device available.
// But when CUDA is enabled, we instead take the kernel and parallelise its
// execution on the device.
// NOTE: Using ## between a comma and __VA_ARGS__ in the following macros is a
// non-standard compiler extension, which removes the comma if __VA_ARGS__ is
// empty and thus allows us to pass zero extra arguments to this macro.
// cf. https://en.cppreference.com/w/cpp/preprocessor/replace
#define RUN_KERNEL(KERNEL, EVT, ...)                                           \
  {                                                                            \
    const int batch_size {EVT.host.batch_size};                                \
    const auto& _evt = EVT.device;                                             \
    Kokkos::RangePolicy<Kokkos::DefaultExecutionSpace> policy(0,batch_size);   \
    Kokkos::parallel_for(STR(KERNEL),policy, KOKKOS_LAMBDA(const int& i){         \
      KERNEL(_evt, i, ##__VA_ARGS__);                                          \
    });                                                                        \
  }

// Now, do the same, but to dispatch over helicity blocks.
#define RUN_HELICITY_BLOCK_KERNEL(KERNEL, EVT, ...)                            \
  {                                                                            \
    const int n_helicity_blocks {EVT.host.n_helicity_blocks};                  \
    const auto& _evt = EVT.device;                                             \
    Kokkos::RangePolicy<Kokkos::DefaultExecutionSpace> policy(0,n_helicity_blocks);  \
    Kokkos::parallel_for(STR(KERNEL),policy, KOKKOS_LAMBDA(const int& block){     \
      KERNEL(_evt, block, ##__VA_ARGS__);                                      \
    });                                                                        \
  }

// Finally, we want to auto-generate a CUDA wrapper for a given kernel.
#define DEFINE_CUDA_KERNEL_NARGS_0(KERNEL)
#define DEFINE_CUDA_KERNEL_NARGS_1(KERNEL, ARG1_T, ARG1)
#define DEFINE_CUDA_KERNEL_NARGS_2(KERNEL, ARG1_T, ARG1, ARG2_T, ARG2)
#define DEFINE_CUDA_KERNEL_NARGS_3(KERNEL, ARG1_T, ARG1, ARG2_T, ARG2, ARG3_T, \
                                   ARG3)
#define DEFINE_CUDA_KERNEL_NARGS_4(KERNEL, ARG1_T, ARG1, ARG2_T, ARG2, ARG3_T, \
                                   ARG3, ARG4_T, ARG4)
#define DEFINE_CUDA_KERNEL_NARGS_5(KERNEL, ARG1_T, ARG1, ARG2_T, ARG2, ARG3_T, \
                                   ARG3, ARG4_T, ARG4, ARG5_T, ARG5)
#define DEFINE_CUDA_KERNEL_NARGS_6(KERNEL, ARG1_T, ARG1, ARG2_T, ARG2, ARG3_T, \
                                   ARG3, ARG4_T, ARG4, ARG5_T, ARG5, ARG6_T,   \
                                   ARG_6)
#define DEFINE_CUDA_KERNEL_NARGS_7(KERNEL, ARG1_T, ARG1, ARG2_T, ARG2, ARG3_T, \
                                   ARG3, ARG4_T, ARG4, ARG5_T, ARG5, ARG6_T,   \
                                   ARG_6, ARG7_T, ARG_7)
#define DEFINE_CUDA_KERNEL_NARGS_8(KERNEL, ARG1_T, ARG1, ARG2_T, ARG2, ARG3_T, \
                                   ARG3, ARG4_T, ARG4, ARG5_T, ARG5, ARG6_T,   \
                                   ARG_6, ARG7_T, ARG_7, ARG8_T, ARG_8)
#endif

