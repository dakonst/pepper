// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_HELICITY_CONFIGURATIONS_H
#define PEPPER_HELICITY_CONFIGURATIONS_H

#include "compiler_warning_macros.h"
PEPPER_SUPPRESS_COMMON_WARNINGS_PUSH
#include "bitmask/bitmask.h"
PEPPER_SUPPRESS_COMMON_WARNINGS_POP
#include "helicity.h"
#include "sm.h"
#include <vector>

class Helicity_configurations_mask;

enum class Helicity_treatment {
  sampled,
  summed,
  fixed
};
std::string to_string(Helicity_treatment);

enum class Helicity_summing_option {
  simple = 0,
  cached = 1<<1,
  parity_symmetric = 1<<2
};
BITMASK_DEFINE_MAX_ELEMENT(Helicity_summing_option, parity_symmetric)
using Helicity_summing_options = bitmask::bitmask<Helicity_summing_option>;
Helicity_summing_options to_helicity_summing_options(const std::string&);

struct Helicity_settings {
  Helicity_treatment treatment {Helicity_treatment::sampled};
  Helicity_summing_options summing_options {Helicity_summing_option::simple};
  std::string fixed_helicities {""};
  bool cached_summing() const
  {
    return (treatment == Helicity_treatment::summed) &&
           (summing_options & Helicity_summing_option::cached);
  }
  bool parity_symmetric_summing() const
  {
    return (treatment == Helicity_treatment::summed) &&
           (summing_options & Helicity_summing_option::parity_symmetric);
  }
};

class Event_handle;
class Permutations;
class Flavour_process;

class Helicity_configurations {

public:
  Helicity_configurations(const Helicity_settings&, int n_ptcl);
  ~Helicity_configurations();

  const Helicity_settings& settings() const { return _settings; };

  void reset(Event_handle&);
  bool advance(Event_handle&, const Helicity_configurations_mask&);

  void precalculate_all_external_states(Event_handle&) const;
  void precalculate_external_states(Event_handle&) const;

  int n_helicities() const { return _n_helicities; }
  double sampling_factor(int n_active) const;

  void debug_current(const Event_handle& evt) const;

  void add_training_data(Event_handle& evt);
  void optimise();
  void stop_optimisation() { should_optimise = false; }

  void reset_timing() const { kernel_duration = 0.0; }

  void construct_external_state(Event_handle&,
                                Helicity helicity = Helicity::unset) const;
private:
  Helicity_settings _settings;
  int _n_helicities;
  int fixed_current;
  bool needs_reset;
  bool should_optimise;
  mutable double kernel_duration {0};
};

#endif
