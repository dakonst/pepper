// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_PROCESS_GROUP_H
#define PEPPER_PROCESS_GROUP_H

#include "colour_summer.h"
#include "flavour_process.h"
#include "helicity_configurations.h"
#include "helicity_configurations_mask.h"
#include "permutations.h"
#include "rambo.h"
#include "unweighter.h"
#include "utilities.h"
#include <memory>

struct Rng;

class Process {

public:
  Process(Settings&, const SM_parameters&);

  void update_enabled_helicities(Event_handle&) const;
  void update_helicity_selection_weights(Event_handle&) const;
  void update_particles(Event_handle&) const;
  void update_helicity_weights(Event_handle&) const;

  // selection weights
  void set_selection_weights_from_means();
  void set_selection_weights_from_variances();
  void set_selection_weights_from_weight_maxima();
  void set_selection_weights_from_stream(std::istream&);
  void write_selection_weights_to_stream(std::ostream&);

  void optimise_helicity_selection_weights();

  Phase_space_generator& ps_generator() const
  {
    const int id = (_current != -1) ? _current : 0;
    return *ps_gens[id];
  }
  Phase_space_generator& ps_generator(int i) const { return *ps_gens[i]; }

  const Permutations& permutations() const { return perms[_current]; }
  const Colour_summer& colour_summer() const
  {
    return colour_summers[_current];
  }

  const Unweighter& unweighter() const { return unweighters[_current]; }

  std::string uid() const { return _uid; }
  std::string current_uid() const { return procs[_current].uid(); }

  int n_ptcl() const { return n; };
  int n_lepton_pairs() const { return procs.front().n_lepton_pairs(); };
  int max_n_perm() const;
  int max_n_perm_qcd() const;
  // max. no. of active helicities across all flavour process groups
  int max_n_active_helicities() const;
  // no. of active helicity summed over all flavour process groups
  int total_n_active_helicities() const;
  const Helicity_configurations_mask& helicity_mask() const
  {
    return helicities_masks[_current];
  }
  int n_active_helicities() const
  {
    return helicities_masks[_current].n_enabled();
  }
  void set_helicity_enabled(int idx, bool flag)
  {
    helicities_masks[_current].set_enabled(idx, flag);
  }

  double averaging_factor(const Helicity_settings& s) const
  {
    if (dummy_averaging_factors_enabled)
      return 1.0;
    return procs[_current].averaging_factor(s);
  }
  double initial_state_symmetry_factor() const
  {
    if (dummy_symmetry_factors_enabled)
      return 1.0;
    return procs[_current].initial_state_symmetry_factor();
  }
  double final_state_symmetry_factor() const
  {
    if (dummy_symmetry_factors_enabled)
      return 1.0;
    return procs[_current].final_state_symmetry_factor();
  }
  double sampling_factor() const
  {
    double fac {1.0};
    if (!_summing_enabled) {
      fac *= 1.0 / selection_weights[_current];
    }
    return fac;
  }
  double sampling_factor(int i) const
  {
    double fac {1.0};
    if (!_summing_enabled) {
      fac *= 1.0 / selection_weights[i];
    }
    return fac;
  }

  void reset_me2(Event_handle& evt) const
  {
    colour_summers[_current].reset_me2(evt);
  }

  void full_reset();
  void reset();
  bool advance(Event_handle&, Rng&);

  bool summing_enabled() const { return _summing_enabled; }
  void set_summing_enabled(bool flag) { _summing_enabled = flag; }

  int current() const { return _current; }
  Flavour_process& current_flavour_process() { return procs[_current]; }
  const Flavour_process& current_flavour_process() const { return procs[_current]; }
  int n_procs() const { return _n_procs; }

  const Flavour_process& operator[](int i) { return procs[i]; }
  const Flavour_process& operator[](int i) const { return procs[i]; }

  void set_fixed_momenta(const std::vector<Vec4>& mom)
  {
    for (auto& ps_gen : ps_gens)
      ps_gen->set_fixed_momenta(mom);
  }

  bool has_optimised_active_helicities() const
  {
    return !should_optimise_active_helicities;
  }
  bool is_optimised_for_event_generation() const
  {
    return !should_optimise_event_generation;
  }
  bool is_optimised_for_event_unweighting() const
  {
    return !should_optimise_event_unweighting;
  }
  bool has_partial_results() const
  {
    return !partial_results.empty();
  }
  void start_active_helicity_optimisation();
  void stop_active_helicity_optimisation();
  void start_optimisation();
  void stop_optimisation();
  void start_integration();
  void stop_integration();
  void start_iteration();
  void stop_iteration();
  void update_iter_stats(int n_trials, int n_passed);
  void update_iter_weights(double weight);
  void add_helicity_training_data_to_flavour_process(int,double);
  void report_run_statistics() const;
  bool training_has_saturated() const;
  double expected_efficiency() const { return _expected_efficiency; }

  // optimisation
  // TODO: These member variables should not be exposed. Finish porting
  // integration-specific data to Process_integrator to resolve this.
  MC_result result;
  MC_result iter_result;
  std::vector<MC_result> partial_results;
  std::vector<MC_result> partial_abs_results;
  std::vector<MC_result> partial_iter_results;
  std::vector<MC_result> partial_iter_abs_results;
  std::vector<double> partial_mean_abs_weights;
  std::vector<double> partial_max_weights;
  bool optimising {false};
  bool integrating {false};

  // max epsilon used in the unweighting for this process
  double max_eps {0.001};

private:
  std::string _uid;
  int _n_procs {0};
  int n {0};
  int _current {-1};
  int _last {-1};
  bool needs_reset {false};
  bool should_write_cached_results {true};
  bool dummy_averaging_factors_enabled {false};
  bool dummy_symmetry_factors_enabled {false};

  std::vector<Flavour_process> procs;
  std::vector<Permutations> perms;
  std::vector<Colour_summer> colour_summers;
  std::vector<Helicity_configurations_mask> helicities_masks;
  std::vector<std::unique_ptr<Phase_space_generator>> ps_gens;
  std::vector<Unweighter> unweighters;

  void add_processes_from_file_spec(const Settings&, std::string);
  void add_process_from_partonic_spec(const Settings&, const std::string&);

  // optimisation
  bool should_optimise_active_helicities {true};
  bool should_optimise_event_generation {true};
  bool should_optimise_event_unweighting {true};
  double phasespace_efficiency {1.0};

  // summing and sampling
  bool _summing_enabled {false};
  double _expected_efficiency {1.0};
  std::vector<double> selection_weights;
};

#endif
