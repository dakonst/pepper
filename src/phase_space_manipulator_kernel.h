// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PHASE_SPACE_MANIPULATOR_KERNEL_H
#define PHASE_SPACE_MANIPULATOR_KERNEL_H

#include "Kokkos_Core.hpp"
#include "event_data.h"
#include "kernel_macros.h"

template<typename event_data>
KOKKOS_INLINE_FUNCTION
void zpt_phase_space_bias(event_data& evt, int i)
{
  const double px = evt.px(i,2) + evt.px(i,3);
  const double py = evt.py(i,2) + evt.py(i,3);
  const double zpt = px*px + py*py;
  // following the biasing in:
  // https://inspirehep.net/literature/2146367, Listing 1
  evt.ps_bias(i) = zpt / 400;
}
DEFINE_CUDA_KERNEL_NARGS_0(zpt_phase_space_bias)

template<typename event_data>
KOKKOS_INLINE_FUNCTION
void max_htp_zpt_phase_space_bias(event_data& evt, int i)
{
  double h_tp {0.0};
  Vec4 z_mom {0.0, 0.0, 0.0, 0.0};
  const int n_ptcl {evt.n_ptcl};
  for (int p {2}; p < n_ptcl; p++) {
    if (is_lepton(evt.ptcl(p)))
      z_mom += evt.p(i, p);
    else
      h_tp += evt.p(i, p).p_perp();
  }
  const double htp2 = h_tp * h_tp;
  const double zpt  = z_mom.m_perp2();
  const double bias = (htp2 > zpt) ? htp2 : zpt;
  evt.ps_bias(i) = bias / 400;
}
DEFINE_CUDA_KERNEL_NARGS_0(max_htp_zpt_phase_space_bias)

// corresponds to Sherpa notation:
// VAR { pow ( max ( sqrt ( H_T2 ) - PPerp ( p [2]) - PPerp ( p [3]) ,
//       ( PPerp( p [2]) + PPerp ( p[3]) ) /2) /30.0 ,2) }

template<typename event_data>
KOKKOS_INLINE_FUNCTION
void max_htp_ttpt_phase_space_bias(event_data& evt, int i)
{
  double h_tp {0.0};
  double tt_tp {0.0};
  const int n_ptcl {evt.n_ptcl};
  for (int p {2}; p < n_ptcl; p++) {
    if (evt.ptcl(p) == 6 || evt.ptcl(p) == -6) {
      tt_tp += evt.p(i, p).p_perp();
    }
    else {
      h_tp += evt.p(i, p).p_perp();
    }
  }
  const double htp2  = h_tp * h_tp;
  const double tt_pt2 = tt_tp*tt_tp / 2.0;
  const double bias = (htp2 > tt_pt2) ? htp2 : tt_pt2;
  evt.ps_bias(i) = bias*bias / 30;
}
DEFINE_CUDA_KERNEL_NARGS_0(max_htp_ttpt_phase_space_bias)

#endif
