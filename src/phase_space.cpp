// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023-2024 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "phase_space.h"

#include "pepper/config.h"

#include "print.h"
#include "rambo.h"
#include "t_channel_integrator.h"
#include "settings.h"
#include "utilities.h"
#include <stdexcept>

#if Chili_FOUND
#include "chili_interface.h"
#endif

#if HDF5_FOUND
#include "hdf5_phase_space_reader.h"
#endif


// factory to create PS generators
std::unique_ptr<Phase_space_generator>
create_phase_space_generator(const Settings& s, const Flavour_process& proc)
{
  if (case_insensitive_equal(s.phase_space_settings.generator, "Rambo")) {
    CHECK_WARN_ONCE(s.phase_space_settings.bias == "None",
                    "Rambo is not compatible with phase space biasing. Will "
                    "disable biasing.");
    return std::make_unique<Rambo>(proc, s.e_cms);
  }
  if (case_insensitive_equal(s.phase_space_settings.generator, "Chili")) {
#if !Chili_FOUND
    throw std::invalid_argument {"Can not use Chili as phase space generator "
                                 "because it has not been found"
                                 " at configuration."};
#else
    CHECK_WARN_ONCE(
        s.phase_space_settings.bias == "None",
        "Chili does not support phase space biasing. Will disable biasing.");
    CHECK_WARN_ONCE(
        Kokkos::device_id() == -1,
        "Chili does not support GPU processing. Event generation might be "
        "slow. Consider using \"Chili(basic)\" instead.");
    return std::make_unique<Chili>(proc, s);
#endif
  }
  if (case_insensitive_equal(s.phase_space_settings.generator, "Chili(basic)")) {
    return std::make_unique<T_channel>(proc, s);
  }
  if (case_insensitive_ends_with(s.phase_space_settings.generator, ".hdf5")) {
#if HDF5_FOUND
    return std::make_unique<Hdf5_phase_space_reader>(proc, s);
#else
    throw std::invalid_argument {
        "Can not read phase-space points from `" +
        s.phase_space_settings.generator +
        "' because HDF5 has not been found at configuration."};
#endif
  }
  throw std::invalid_argument {"Phase Space Generator " +
                               s.phase_space_settings.generator +
                               " could not be found"};
}

void Phase_space_generator::update_weights_and_train(Event_handle& evt)
{
  update_weights(evt);
}

void Phase_space_generator::optimise() {}

void Phase_space_generator::add_training_data(Event_handle& evt)
{
  (void)evt;
}

void Phase_space_generator::stop_optimisation() { should_optimise = false; }

void Phase_space_generator::set_fixed_momenta(const std::vector<Vec4>&)
{
  ERROR("Fixed momenta are not supported by this phase-space generator "
        "interface. Abort.");
  abort();
}

void Phase_space_generator::write_to_cache(const std::string& procname)
{
  (void)procname;
}

void Phase_space_generator::read_from_cache(const std::string& procname)
{
  (void)procname;
}
