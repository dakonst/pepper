// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_UTILITIES_H
#define PEPPER_UTILITIES_H

#include <sstream>
#include <string>
#include <vector>

bool ends_with(const std::string& str, const std::string& end);
bool case_insensitive_ends_with(const std::string& str, const std::string& end);

bool begins_with(const std::string& str, const std::string& begin);
bool case_insensitive_begins_with(const std::string& str, const std::string& begin);

std::size_t replace_all(std::string& inout, std::string_view what,
                        std::string_view with);

// Cf. https://stackoverflow.com/questions/236129/how-do-i-iterate-over-the-words-of-a-string
template <typename Out> void split(const std::string& s, char delim, Out result)
{
  std::istringstream iss(s);
  std::string item;
  while (std::getline(iss, item, delim)) {
    *result++ = item;
  }
}
inline std::vector<std::string> split(const std::string& s, char delim)
{
  std::vector<std::string> elems;
  split(s, delim, std::back_inserter(elems));
  return elems;
}

// Cf. https://stackoverflow.com/questions/9277906/stdvector-to-string-with-custom-delimiter
template <typename T> std::string join(const T& v, const std::string& delim)
{
  std::ostringstream s;
  for (const auto& i : v) {
    if (&i != &v[0]) {
      s << delim;
    }
    s << i;
  }
  return s.str();
}

bool case_insensitive_equal(const std::string&, const std::string&);

// trim from start (in place)
void ltrim(std::string&);

// trim from end (in place)
void rtrim(std::string&);

// trim from both ends (in place)
void trim(std::string&);

// check if any whitespace is present
bool contains_space(const std::string&);

// transform to lower case (in place)
std::string to_lowercase(std::string);

#endif
