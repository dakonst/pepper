// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_HDF5_WRITER_H
#define PEPPER_HDF5_WRITER_H

#include "pepper/config.h"

#include "event_writer.h"
#include "highfive/H5File.hpp"
#include "highfive/H5PropertyList.hpp"
#include <string>
#include <unordered_map>

class Hdf5_writer : public Event_writer {

public:
  Hdf5_writer(const std::string& filename, const Settings&,
              const SM_parameters&);
  ~Hdf5_writer() override;

  void initialise(const Process&, const Pdf_and_alpha_s&) override;
  void finalise(const MC_result&) override;

  void write_n(const Event_handle&, int n, const MC_result&, long n_nonzero,
               long n_trials, const Process&) override;

  // This writer wants all-zero batches for two reasons:
  // - When MPI is used, we will need to do some calls within write_n
  //   collectively across all ranks, otherwise execution would freeze.
  // - A trailing all-zero batch is accounted for by adjusting the n_trials data
  //   of a previous non-zero event, so we can actually end on an all-zero batch.
  bool wants_zero_batches() const override
  {
    return true;
  }

  bool prints_leading_colour_information() const override
  {
    return leading_colour_flow_output_enabled;
  }


private:
  void write_header(const Process&, const Pdf_and_alpha_s&);
  void set_expected_n_nonzero_per_batch(double);
  void flush_cache();
  void write_cache();
  void clear_cache();
  void write_remaining_n_zero_events();

  bool initialised {false};

  // storage for relevant settings
  long batch_size;
  long n_batches;
  bool unweighting_disabled;
  bool leading_colour_flow_output_enabled;
  std::vector<int> colours;
  std::vector<int> anticolours;
  double e_cms;
  double ew_alpha;

  std::unique_ptr<HighFive::File> file;
  std::unordered_map<std::string, HighFive::DataSet> datasets;
  HighFive::DataTransferProps transfer_props;
  int n_ptcl_max {0};

  // Control when the cache is flushed, either after a number of (non-zero)
  // events, or after a number of batches. Only one of the two is expected to
  // be non-zero. Note that MPI support requires the use of the batch-based
  // cache logic.
  size_t event_cache_size {0};
  size_t batch_cache_size {0};

  size_t n_written_across_all_ranks {0};
  size_t n_cached_batches {0};
  size_t n_zero_events {0};
  std::vector<std::array<double, 10>> evt_cache;
  std::vector<std::array<double, 13>> ptcl_cache;

  mutable double write_events_duration {0.0};
};

#endif
