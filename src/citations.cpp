// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "citations.h"
#include <iomanip>

std::map<std::string, std::string> Citations::detail::citations;

void Citations::print_citations(std::ostream& out)
{
  if (!Mpi::is_main_rank())
    return;

  // check if there are any citations, otherwise return early
  if (detail::citations.empty())
    return;

  // print header
  out << "\% This file contains LaTeX-style citations for Pepper and any external\n"
      << "\% scientific software or physics results used to generate the given result with\n"
      << "\% Pepper. Upload this file to https://inspirehep.net/bibliography-generator to\n"
      << "\% generate a bibliography for your publication.\n\n";

  // print citations
  out << "\\begin{itemize}\n";
  for (auto& kv : detail::citations)
    out << "  \\item " << kv.first << " \\cite{" << kv.second << "}\n";
  out << "\\end{itemize}\n\n";

  // print footer
  out << "Thank you for using Pepper!\n";
}
