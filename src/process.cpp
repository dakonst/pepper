// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "process.h"

#include "pepper/config.h"

#include "event_handle.h"
#include "paths.h"
#include "process_kernel.h"
#include "rng.h"
#include "settings.h"
#include "utilities.h"
#include <filesystem>
#include <numeric>

namespace fs = std::filesystem;

Process::Process(Settings& s, const SM_parameters& sm)
    : max_eps {s.max_eps},
      should_write_cached_results {s.phase_space_settings.write_cached_results},
      dummy_averaging_factors_enabled {s.dummy_averaging_factors_enabled},
      dummy_symmetry_factors_enabled {s.dummy_symmetry_factors_enabled}
{
  if (s.process_specs.empty()) {
    ERROR("At least one process must be specified. Abort.");
    abort();
  }

  // if a process spec has no spaces, we'll try to find a corresponding process
  // spec file; if it has spaces, we parse the particles

  for (auto spec : s.process_specs) {
    trim(spec);
    if (contains_space(spec)) {
      // treat this as a partonic channel
      add_process_from_partonic_spec(s, spec);
    }
    else {
      // treat this as a potential process file that contains groups of partonic
      // channels; curently, this is exclusive, i.e. one can not use other
      // process specs if also a process file is given
      if (!procs.empty()) {
        ERROR("Only one process file can be used. Abort.");
        abort();
      }
      add_processes_from_file_spec(s, spec);
    }
  }

  // make sure at least one process has been initialised
  if (procs.empty()) {
    ERROR("No process has been found. Abort.");
    abort();
  }

  n = procs.front().n_ptcl();
  _n_procs = procs.size();

  Paths::create_paths(s, *this);

  for (int i{0}; i < _n_procs; ++i)
    colour_summers.emplace_back(procs[i], perms[i].n_qcd(), s);

  // validate (and potentially adjust) settings for the constructed process
  if (procs[0].n_lepton_pairs() > 0 && sm.ew.z_enabled &&
      (s.helicity_settings.summing_options &
       Helicity_summing_option::parity_symmetric)) {
    WARN_ONCE(
        "Parity symmetric helicity summation (`-O p` or "
        "`--helicity-summing-options parity_symmetric`) can not be used for "
        "processes that involve EW interactions such as \"", procs[0], "\", "
        "and will thus be ignored.");
    s.helicity_settings.summing_options ^=
        Helicity_summing_option::parity_symmetric;
  }

  if (_n_procs < 2)
    _summing_enabled = true;
  selection_weights.resize(_n_procs, 1.0 / double(_n_procs));

  // set up helicity masking
  const int n_helicities {
      Helicity_configurations {s.helicity_settings, n}.n_helicities()};
  helicities_masks.resize(_n_procs, Helicity_configurations_mask {n_helicities});
  if (s.helicity_settings.treatment == Helicity_treatment::fixed) {
    // there is no need to mask or load anything when the helicity configuration
    // is fixed
    should_optimise_active_helicities = false;
  }
  else if (s.phase_space_settings.use_cached_results_if_possible) {
    // try to load helicity mask data from the cache
    fs::path helmask_path {Paths::cache + "/helicity_mask"};
    if (fs::exists(helmask_path)) {
      VERBOSE("Will use cached helicity masks ...");
      std::ifstream file {helmask_path.string()};
      for (int p {0}; p < _n_procs; ++p) {
        for (int i {0}; i < n_helicities; ++i) {
          bool enabled {false};
          file >> enabled;
          helicities_masks[p].set_enabled(i, enabled);
        }
        VERBOSE("-> ", procs[p].uid(),
                ": #active helicities = ", helicities_masks[p].n_enabled());
      }
      should_optimise_active_helicities = false;
    }
  }
  if (should_optimise_active_helicities) {
    if (s.helicity_settings.cached_summing()) {
      for (int p {0}; p < _n_procs; ++p) {
        const int final_idx {perms[p].get_final()};
        // Disable all helicities with "-" at this position, because the
        // corresponding amplitudes are calculated along with the corresponding "+"
        // configuration at that index for cached summing, by re-using the (n -
        // 1)-current excluding the 1-current at this position (effectively doing
        // the final contraction of the BG recursion twice, without re-calculating
        // the unchanging (n - 1)-current unnecessarily).
        for (int i {0}; i < n_helicities; ++i) {
          if (calculate_helicity(i, final_idx) == Helicity::minus)
            helicities_masks[p].set_enabled(i, false);
        }
      }
    }
    if (s.helicity_settings.parity_symmetric_summing()) {
      for (int p {0}; p < _n_procs; ++p) {
        const int final_idx {perms[p].get_final()};
        int idx {0};
        // Do not use the same index as for cached summing, such that both
        // optimisations can be combined.
        while (idx == final_idx)
          idx++;
        // Disable all helicities with "-" at this position, because the
        // corresponding amplitudes are calculated along with the corresponding "+"
        // configuration at that index for parity-symmetric summing,
        // by simply making use of the fact that QCD and QED amplitudes are
        // symmetric under the parity transformation, i.e. symmetric under flipping
        // all helicities. Hence, we can calculate one half of the amplitudes, and
        // then multiply with a factor of 2.0 at the end.
        for (int i {0}; i < n_helicities; ++i) {
          if (calculate_helicity(i, idx) == Helicity::minus)
            helicities_masks[p].set_enabled(i, false);
        }
      }
    }
  }

  // the phase-space generator might not support/allow optimisation, adapt
  // settings accordingly
  if (!ps_generator().allows_optimisation()) {
    s.phase_space_settings.use_cached_results_if_possible = false;
    s.phase_space_settings.write_cached_results = false;
    should_write_cached_results = false;
    should_optimise_event_generation = false;
    should_optimise_event_unweighting = false;
  }

  // the phase-space generator might not support/allow multiple flavour
  // processes
  if (_n_procs > 1 && !ps_generator().allows_multiple_flavour_processes()) {
    ERROR("The phase-space generator `", s.phase_space_settings.generator,
          "' does not support multiple flavour processes. Abort.");
    abort();
  }

  // TODO: On MPI, we should load the data below only on the main rank, and
  // then broadcast, to reduce file I/O.
  if (s.phase_space_settings.use_cached_results_if_possible) {

    // try to load event generation data from the cache
    fs::path evtgen_path {Paths::cache + "/event_generation"};
    if (fs::exists(evtgen_path)) {
      VERBOSE("Will use cached event generation optimisation results ...");
      std::ifstream file {evtgen_path.string()};

      file >> _expected_efficiency;
      VERBOSE("-> expected overall efficiency: ", _expected_efficiency * 100, " %");

      // read partial results and set partial mean weights accordingly
      partial_results.resize(_n_procs);
      partial_abs_results.resize(_n_procs);
      partial_mean_abs_weights.resize(_n_procs);
      for (int i {0}; i < _n_procs; ++i) {
        file >> partial_results[i].raw();
        file >> partial_abs_results[i].raw();
        partial_mean_abs_weights[i] =
            partial_abs_results[i].sum / partial_abs_results[i].n_nonzero;
        VERBOSE("-> ", procs[i].uid(),
                ": w_mean = ", partial_mean_abs_weights[i]);
      }

      set_selection_weights_from_stream(file);

      for (int i {0}; i < _n_procs; ++i) {
        int n_hel_enabled {0};
        file >> n_hel_enabled;
        double sum {0.0};
        for (int j {0}; j < n_hel_enabled; ++j) {
          file >> procs[i].helicity_selection_weight(j);
          sum += procs[i].helicity_selection_weight(j);
        }
        for (int j {0}; j < n_hel_enabled; ++j) {
          procs[i].helicity_selection_weight(j) /= sum;
        }
      }

      // let the phasespace generator read in its cache (if any)
      for (int i {0}; i < _n_procs; ++i) {
        ps_generator(i).read_from_cache(procs[i].uid());
      }

      should_optimise_event_generation = false;
    }

    // try to load unweighting data from the cache, if needed
    if (!s.unweighting_disabled) {
      fs::path unweighting_path {Paths::cache + "/event_unweighting"};
      if (fs::exists(unweighting_path)) {
        VERBOSE("Will use cached event unweighting optimisation results ...");
        std::ifstream file {unweighting_path.string()};

        file >> _expected_efficiency;
        VERBOSE("-> expected overall efficiency = ", _expected_efficiency * 100,
                " %");

        // read partial results
        partial_results.resize(_n_procs);
        for (int i {0}; i < _n_procs; ++i) {
          file >> partial_results[i].raw();
        }

        // read partial max weights
        partial_max_weights.resize(_n_procs);
        for (int i {0}; i < _n_procs; ++i) {
          file >> partial_max_weights[i];
          VERBOSE("-> ", procs[i].uid(), ": w_max = ", partial_max_weights[i]);
        }

        set_selection_weights_from_stream(file);

        for (int i {0}; i < _n_procs; ++i) {
          unweighters[i].w_max = partial_max_weights[i] * sampling_factor(i);
        }

        should_optimise_event_unweighting = false;
      }
    }

  }
}

void Process::add_processes_from_file_spec(const Settings& s, std::string spec)
{
  // we allow the user to specify OL-like process specs, e.g. ppzjjj for "p p ->
  // Z j j j", translate this into our shorter filename convention, e.g. z3j.csv
  const std::vector<std::vector<std::string>> replacement_rules {{"ee", "z"},
                                                                 {"Z", "z"}};

  if (begins_with(spec, "pp")) {
    for (const auto& rule : replacement_rules) {
      auto found = spec.find(rule[0]);
      if (found != std::string::npos)
        spec.replace(found, rule[0].length(), rule[1]);
    }

    spec = spec.substr(2);
    std::stringstream spec_stream {spec};
    int n_jets {0};
    while (spec.back() == 'j') {
      spec.pop_back();
      n_jets++;
    }
    spec_stream << spec << n_jets << "j.csv";
    spec = spec_stream.str();
  }

  // search for the file, first locally, then in the data_path
  std::ifstream process_file;
  if (fs::exists(spec)) {
    process_file.open(spec);
  }
  else if (fs::exists(s.data_path / spec)) {
    process_file.open(s.data_path / spec);
  }
  else {
    ERROR("Process file `" + spec + "` not found. Abort.");
    abort();
  }

  std::string line;
  int current_flavour_process_index {-1};
  while (std::getline(process_file, line)) {
    if (line[0] == '#')
      continue;
    size_t delim_pos = line.find(',');
    int flavour_process_index = std::stoi(line.substr(0, delim_pos));
    auto channel = line.substr(delim_pos + 1);
    if (flavour_process_index != current_flavour_process_index) {
      // add new flavour process
      procs.emplace_back(channel);
      perms.emplace_back(channel);
      unweighters.emplace_back();
      ps_gens.emplace_back(create_phase_space_generator(s, channel));
      current_flavour_process_index = flavour_process_index;
      DEBUG("Adding flavour process ", channel);
    }
    else {
      // add mapping
      procs.back().add_flavour_channel(channel);
      DEBUG("-> Adding flavour channel ", channel);
    }
  }
  process_file.close();
  _uid = fs::path{spec}.stem();
}

void Process::add_process_from_partonic_spec(const Settings& s,
                                             const std::string& spec)
{
  if (!procs.empty())
    _uid.push_back(',');
  procs.emplace_back(spec);
  perms.emplace_back(spec);
  unweighters.emplace_back();
  ps_gens.emplace_back(create_phase_space_generator(s, spec));
  _uid += procs.back().uid();
}

void Process::start_active_helicity_optimisation()
{
}

void Process::stop_active_helicity_optimisation()
{
  if (should_write_cached_results && Mpi::is_main_rank()) {
    fs::path helmask_path {Paths::cache + "/helicity_mask"};
    std::ofstream file {helmask_path.string()};
    for (int i {0}; i < _n_procs; ++i) {
      int n_helicites {static_cast<int>(helicities_masks[i].size())};
      for (int j {0}; j < n_helicites; ++j) {
        file << helicities_masks[i].enabled(j);
        if (j == n_helicites - 1)
          file << '\n';
        else
          file << ' ';
      }
    }
  }
}

void Process::start_optimisation()
{
  optimising = true;
  result.clear();
  partial_results.resize(_n_procs);
  partial_abs_results.resize(_n_procs);
  partial_mean_abs_weights.resize(_n_procs, 0.0);

  // init helicity optimisation
  // TODO: this should only happen in case we are helicity sampling, but
  // currently the process does not know about that ...
  for (int i {0}; i < _n_procs; ++i) {
    const size_t active_hels = helicities_masks[i].n_enabled();
    procs[i].helicity_mc_results().resize(active_hels);
    procs[i].init_default_selection_weights(active_hels);
  }

}

void Process::stop_optimisation()
{
  optimising = false;
  should_optimise_event_generation = false;
  for (int i {0}; i < _n_procs; ++i) {
    partial_mean_abs_weights[i] =
        partial_abs_results[i].sum / partial_abs_results[i].n_nonzero;
    ps_generator(i).stop_optimisation();
  }
  phasespace_efficiency = double(result.n_nonzero) / result.n_trials;
  _expected_efficiency = phasespace_efficiency;

  for (int i {0}; i < _n_procs; ++i) {
    procs[i].helicity_mc_results().clear();
  }

  if (should_write_cached_results && Mpi::is_main_rank()) {
    fs::path evtgen_path {Paths::cache + "/event_generation"};
    std::ofstream file {evtgen_path.string()};
    file.precision(18);

    file << _expected_efficiency << '\n';

    for (int i {0}; i < _n_procs; ++i) {
      file << partial_results[i].raw() << '\n';
      file << partial_abs_results[i].raw() << '\n';
    }

    write_selection_weights_to_stream(file);

    for (int i {0}; i < _n_procs; ++i) {
      const int n_hel_enabled {helicities_masks[i].n_enabled()};
      file << n_hel_enabled << '\n';
      for (int j {0}; j < n_hel_enabled; ++j) {
        file << procs[i].helicity_selection_weight(j) << '\n';
      }
    }

    // let the phasespace generator write to its cache (if any)
    for (int i {0}; i < _n_procs; ++i) {
      ps_generator(i).write_to_cache(procs[i].uid());
    }
  }
}

void Process::start_integration()
{
  integrating = true;
  partial_max_weights.resize(_n_procs, std::numeric_limits<double>::lowest());
  result.clear();
  partial_results.clear();
  partial_abs_results.clear();
  partial_results.resize(_n_procs);
  partial_abs_results.resize(_n_procs);
}

void Process::stop_integration()
{
  // set up unweighting
  set_selection_weights_from_weight_maxima();

  const double mean_weight {result.sum / result.n_nonzero};
  const double overall_w_max {*std::max_element(partial_max_weights.cbegin(),
                                                partial_max_weights.cend())};
  const double unweighting_efficiency = mean_weight / overall_w_max;
  _expected_efficiency = phasespace_efficiency * unweighting_efficiency;

  if (should_write_cached_results && Mpi::is_main_rank()) {
    fs::path unweighting_path {Paths::cache + "/event_unweighting"};
    std::ofstream file {unweighting_path.string()};
    file.precision(18);

    file << _expected_efficiency << '\n';

    // write partial results
    for (int i {0}; i < _n_procs; ++i)
      file << partial_results[i].raw() << '\n';

    // write partial max weights
    for (int i {0}; i < _n_procs; ++i)
      file << unweighters[i].w_max / sampling_factor(i) << '\n';

    write_selection_weights_to_stream(file);
  }

  // print verbose info about weight maxima und expected unweighting
  // efficiencies
  if (Msg::verbosity >= Msg::Verbosity::verbose) {
    VERBOSE("Expected unweighting efficiencies per process:");
    for (int i {0}; i < _n_procs; ++i) {
      const auto res_i = partial_abs_results[i];
      const double mean_weight_i = res_i.sum / res_i.n_nonzero;
      VERBOSE(" -> ", procs[i].uid(),
              ": expected unweighting efficiency = ", mean_weight_i / partial_max_weights[i] * 100,
              " %, w_max = ", partial_max_weights[i]);
    }
  }

  integrating = false;
  should_optimise_event_unweighting = false;
  partial_abs_results.clear();
  partial_mean_abs_weights.clear();
}

void Process::start_iteration()
{
  if (partial_iter_results.empty())
    partial_iter_results.resize(_n_procs);
  else
    for (auto& res : partial_iter_results)
      res.clear();
  if (partial_iter_abs_results.empty())
    partial_iter_abs_results.resize(_n_procs);
  else
    for (auto& res : partial_iter_abs_results)
      res.clear();

}

void Process::stop_iteration()
{
  for (int i {0}; i < _n_procs; ++i) {
    partial_iter_results[i].all_mpi_sum();
    partial_results[i] += partial_iter_results[i];
    partial_iter_abs_results[i].all_mpi_sum();
    partial_abs_results[i] += partial_iter_abs_results[i];
  }

  iter_result.all_mpi_sum();
  result += iter_result;
  iter_result.clear();

  // adapt phase space generator
  if (optimising)
    for (int i {0}; i < _n_procs; ++i)
      ps_generator(i).optimise();
}

void Process::update_iter_stats(int n_trials, int n_passed)
{
  partial_iter_results[_current].n_trials += n_trials;
  partial_iter_results[_current].n_nonzero_after_cuts += n_passed;
  partial_iter_results[_current].n_nonzero += n_passed;

  partial_iter_abs_results[_current].n_trials += n_trials;
  partial_iter_abs_results[_current].n_nonzero_after_cuts += n_passed;
  partial_iter_abs_results[_current].n_nonzero += n_passed;

  iter_result.n_trials += n_trials;
  iter_result.n_nonzero_after_cuts += n_passed;
  iter_result.n_nonzero += n_passed;
}

void Process::update_iter_weights(double summed_weight)
{
  if (summed_weight == 0.0)
    return;

  const double weight {summed_weight / sampling_factor()};

  partial_iter_results[_current].sum += weight;
  partial_iter_results[_current].sum2 += weight * weight;

  iter_result.sum += summed_weight;
  iter_result.sum2 += summed_weight * summed_weight;

  partial_iter_abs_results[_current].sum += std::abs(weight);
  partial_iter_abs_results[_current].sum2 += weight * weight;

  if (integrating) {
    partial_max_weights[_current] =
        std::max(partial_max_weights[_current], std::abs(weight));
  }
}

void Process::update_helicity_weights(Event_handle& evt) const
{
  RUN_KERNEL(update_helicity_weights_kernel, evt);
}

void Process::add_helicity_training_data_to_flavour_process(int helicity,
                                                            double weight)
{
  procs[_current].add_helicity_training_data(helicity, weight);
}

void Process::report_run_statistics() const
{
  for (int i {0}; i < _n_procs; ++i) {
    ps_gens[i]->report_run_statistics(procs[i]);
  }
}

void Process::optimise_helicity_selection_weights() {
  for (auto& flavor_proc : procs)
    flavor_proc.set_selection_weights_from_variances();
}

int Process::max_n_perm() const
{
  int max_n {0};
  for (const auto& perm : perms)
    max_n = std::max(max_n, perm.n());
  return max_n;
}

int Process::max_n_perm_qcd() const
{
  int max_n {0};
  for (const auto& perm : perms)
    max_n = std::max(max_n, perm.n_qcd());
  return max_n;
}

int Process::max_n_active_helicities() const
{
  int max_n {0};
  for (const auto& mask : helicities_masks)
    max_n = std::max(max_n, mask.n_enabled());
  return max_n;
}

int Process::total_n_active_helicities() const
{
  int total_n {0};
  for (const auto& mask : helicities_masks)
    total_n += mask.n_enabled();
  return total_n;
}

void Process::update_particles(Event_handle& evt) const
{
  // TODO: We should try to extract `Flavour_process_data` and `Batch_data` from
  // `Event_handle`, perhaps even put them into shared memory when using
  // CUDA.
  for (int i {0}; i < evt.host.n_ptcl; ++i) {
    evt.host.ptcl(i) = procs[_current].particle_all_outgoing(i);
  }
  evt.push_particles_to_device();
}

void Process::update_enabled_helicities(Event_handle& evt) const
{
  helicities_masks[_current].update(evt);
}

void Process::update_helicity_selection_weights(Event_handle& evt) const
{
  for (int i {0}; i < n_active_helicities(); ++i) {
    evt.host.hel_selection_weights(i) = 
        procs[_current].helicity_selection_weight(i);
  }
  evt.push_helicity_selection_weights_to_device();
}

void Process::full_reset()
{
  _current = -1;
  _last = -1;
  needs_reset = false;
}

void Process::reset()
{
  _current = -1;
  needs_reset = false;
}

bool Process::advance(Event_handle& evt, Rng& rng)
{
  if (needs_reset)
    return false;
  if (_summing_enabled) {
    if (_current == _n_procs - 1) {
      needs_reset = true;
      return false;
    }
    _current++;
  }
  else {
    if (_current >= 0) {
      needs_reset = true;
      return false;
    }
    else {
      const double r {rng.d()};
      double selector {0.0};
      for (int i {0}; i < _n_procs; ++i) {
        selector += selection_weights[i];
        if (selector > r) {
          _current = i;
          break;
        }
      }
    }
  }

  if (_current != _last) {
    evt.proc_idx = _current;
    update_helicity_selection_weights(evt);
    update_enabled_helicities(evt);
    update_particles(evt);
    _last = _current;
  }
  return true;
}

void Process::set_selection_weights_from_means()
{
  for (int i {0}; i < _n_procs; ++i) {
    selection_weights[i] = std::abs(partial_results[i].mean());
  }
  const double sum {std::accumulate(selection_weights.cbegin(),
                                    selection_weights.cend(), 0.0)};
  for (int i {0}; i < _n_procs; ++i) {
    selection_weights[i] /= sum;
  }
}

void Process::set_selection_weights_from_variances()
{
  for (int i {0}; i < _n_procs; ++i) {
    selection_weights[i] =
        pow(std::sqrt(partial_results[i].variance()), 2.0 / 3.0);
  }
  const double sum {std::accumulate(selection_weights.cbegin(),
                                    selection_weights.cend(), 0.0)};
  for (int i {0}; i < _n_procs; ++i) {
    selection_weights[i] /= sum;
  }
}

void Process::set_selection_weights_from_weight_maxima()
{
  if(partial_max_weights.size() != selection_weights.size())
    ERROR("Sizes of selection weights and max weights do not match.");
  const double sum {std::accumulate(partial_max_weights.cbegin(),
                                    partial_max_weights.cend(), 0.0)};
  for (int i {0}; i < _n_procs; ++i)
    selection_weights[i] = partial_max_weights[i] / sum;

  // TODO: Rename this function to reflect that it also updates the unweighting.
  for (int i {0}; i < _n_procs; ++i) {
    unweighters[i].w_max = partial_max_weights[i] * sampling_factor(i);
  }
}

void Process::set_selection_weights_from_stream(std::istream& s)
{
  double sum {0.0};
  for (int i{0}; i < _n_procs; ++i) {
    s >> selection_weights[i];
    sum += selection_weights[i];
  }
  for (int i {0}; i < _n_procs; ++i)
    selection_weights[i] /= sum;
}

void Process::write_selection_weights_to_stream(std::ostream& s)
{
  for (int i{0}; i < _n_procs; ++i) {
    s << selection_weights[i] << '\n';
  }
}

bool Process::training_has_saturated() const
{
  for (int i {0}; i < _n_procs; ++i) {
    if(partial_abs_results[i].sum == 0.0)
      return false;
  }
  return true;
}
