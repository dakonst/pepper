// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PHASE_SPACE_MANIPULATOR_H
#define PHASE_SPACE_MANIPULATOR_H

#include <memory>
#include <string>

class Event_handle;

class Phase_space_manipulator {
public:
  virtual ~Phase_space_manipulator() = default;

  virtual void fill_biasing_factor(Event_handle&) const = 0;
};

std::unique_ptr<Phase_space_manipulator>
create_phase_space_manipulator(const std::string& spec);

class Zpt_phase_space_biaser : public Phase_space_manipulator {
public:
  void fill_biasing_factor(Event_handle&) const override;
};

class Max_htp_zpt_phase_space_biaser : public Phase_space_manipulator {
public:
  void fill_biasing_factor(Event_handle&) const override;
};

class Max_htp_ttpt_phase_space_biaser : public Phase_space_manipulator {
public:
  void fill_biasing_factor(Event_handle&) const override;
};


#endif


