// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_VEC3_H
#define PEPPER_VEC3_H

#include "Kokkos_Core.hpp"

#include <array>
#include <cmath>

struct Vec3 : std::array<double, 3> {
  KOKKOS_INLINE_FUNCTION
  double abs() const { return Kokkos::sqrt(Vec3::abs2()); };
  KOKKOS_INLINE_FUNCTION 
  double abs2() const { return (*this) * (*this); };
  KOKKOS_INLINE_FUNCTION 
  Vec3& operator/=(double rhs)
  {
    (*this)[0] /= rhs;
    (*this)[1] /= rhs;
    (*this)[2] /= rhs;
    return *this;
  };
  KOKKOS_INLINE_FUNCTION
  Vec3& operator*=(double rhs)
  {
    (*this)[0] *= rhs;
    (*this)[1] *= rhs;
    (*this)[2] *= rhs;
    return *this;
  };
  KOKKOS_INLINE_FUNCTION
  Vec3& operator+=(Vec3 rhs)
  {
    (*this)[0] += rhs[0];
    (*this)[1] += rhs[1];
    (*this)[2] += rhs[2];
    return *this;
  };
  KOKKOS_INLINE_FUNCTION
  Vec3& operator-=(Vec3 rhs)
  {
    (*this)[0] -= rhs[0];
    (*this)[1] -= rhs[1];
    (*this)[2] -= rhs[2];
    return *this;
  };

  KOKKOS_INLINE_FUNCTION 
  friend Vec3 operator/(Vec3 lhs, double rhs) { return lhs /= rhs; }
  KOKKOS_INLINE_FUNCTION 
  friend double operator*(const Vec3& lhs, const Vec3& rhs)
  {
    return lhs[0] * rhs[0] + lhs[1] * rhs[1] + lhs[2] * rhs[2];
  };
  KOKKOS_INLINE_FUNCTION
  friend Vec3 operator*(Vec3 lhs, double rhs) { return lhs *= rhs; }

  KOKKOS_INLINE_FUNCTION
  friend Vec3 operator*(double lhs, Vec3 rhs) { return rhs *= lhs; }

  KOKKOS_INLINE_FUNCTION
  friend Vec3 operator-(Vec3 lhs, Vec3 rhs) { return lhs -= rhs; };

  KOKKOS_INLINE_FUNCTION
  friend Vec3 operator+(Vec3 lhs, Vec3 rhs) { return lhs += rhs; };

  KOKKOS_INLINE_FUNCTION
  Vec3 Cross(const Vec3 rhs) {
    const double px = (*this)[1]*rhs[2] - (*this)[2]*rhs[1];
    const double py = (*this)[2]*rhs[0] - (*this)[0]*rhs[2];
    const double pz = (*this)[0]*rhs[1] - (*this)[1]*rhs[0];
    return {px,py,pz};
  };
};

#endif
