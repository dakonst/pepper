// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_VEGAS
#define PEPPER_VEGAS

#include <string>
#include <vector>
#include "event_handle.h"
#include "timing.h"
#include "Kokkos_Core.hpp"
#include "paths.h"
#include "settings.h"
#include <filesystem>

namespace fs = std::filesystem;

class Vegas {
private:
  int m_nd, m_dim;
  double m_alpha, m_nc;

  Kokkos::View<double**,Kokkos::DefaultExecutionSpace::array_layout,Kokkos::DefaultExecutionSpace> d_p_xi, d_d, d_hit;
  Kokkos::View<double**,Kokkos::DefaultExecutionSpace::array_layout,Kokkos::HostSpace> h_p_xi, h_d, h_hit;

  template<typename xi_type>
  void Rebin(const double rc, const xi_type& xi,
                    const std::vector<double>& p_r)
  {
    int i, k=-1;
    double dr=0., xn=0., xo=0.;
    std::vector<double> xin(m_nd,1.);
    for (i=0;i<m_nd-1;++i) {
      while (rc > dr) {
        dr+=p_r[++k];
        xo=xn;
        xn=xi[k];
      }
      dr-=rc;
      xin[i]=xn-(xn-xo)*dr/p_r[k];
    }
    for (i=0;i<m_nd;i++) xi[i]=xin[i];
  }
public:
  Vegas(int dim,int ndx,double alpha);
  Vegas() {
  }
  void GeneratePoint(Event_handle& evt);
  void GenerateWeight(Event_handle& evt) const;
  void AddPoint(Event_handle& evt);
  void Push_grid_to_device();
  void Optimize();
  void Reset();
  void WriteOut(const std::string &pid);
  void ReadIn(const std::string &pid);

  void DeSerialize(std::vector<double> &x);
  std::vector<double> Serialize() const;

  void Sync_grids();

  void Pull_everything_from_device();
  void Push_everything_to_device();
};// end of namespace Vegas

#endif
