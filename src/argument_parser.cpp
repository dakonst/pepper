// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2024 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "argument_parser.h"

Argument_parser::Argument_parser(std::string program_name, std::string _prefix)
    : parser {std::move(program_name), PROJECT_VERSION,
              argparse::default_arguments::none},
      prefix {std::move(_prefix)}
{
  add_argument("-h", "--help")
      .action([&](const std::string&) {
        if (Mpi::is_main_rank())
          print(parser.help().str());
        std::exit(0);
      })
      .help("show help message and exit")
      .implicit_value(true);

  // we do not want to apply our prefix to the already prefix --kokkos-help
  parser.add_argument("--kokkos-help")
      .help("show Kokkos help message and exit")
      .implicit_value(true);

  add_argument("-v", "--version")
      .action([&](const std::string&) {
        if (Mpi::is_main_rank())
          println(Msg::version_string());
        std::exit(0);
      })
      .help("show version information and exit")
      .implicit_value(true);

  add_argument("--cite")
      .action([&](const std::string&) {
        if (Mpi::is_main_rank()) {
          std::ifstream f {fs::path {PEPPER_INSTALL_DATADIR} / "CITATION.cff"};
          if (f.is_open())
            std::cout << f.rdbuf();
        }
        std::exit(0);
      })
      .help("show citation information and exit")
      .implicit_value(true);
}

std::string Argument_parser::prefixed_argument(std::string_view arg_name) const
{
  // build long-form arg, i.e. "--[prefix-]argname" or the positional
  // "[prefix_]argname"
  std::string arg;
  if (arg_name.substr(0, 2) == "--") {
    arg = "--";
    if (!prefix.empty()) {
      arg += prefix + "-";
    }
    arg += arg_name.substr(2);
  }
  else {
    arg = prefix + "_";
    arg += arg_name;
  }
  return arg;
}

void Argument_parser::add_runcard_path_argument(
    const std::string& default_value)
{
  default_runcard_path = default_value;
  if (prefix.empty()) {
    add_argument("runcard_path")
        .help("set runcard path")
        .nargs(argparse::nargs_pattern::optional)
        .default_value(default_value);
  }
  else {
    add_argument("-f", "--runcard-path").help("set runcard path");
  }
}

std::string Argument_parser::runcard_path() const
{
  if (prefix.empty())
    return get<std::string>("runcard_path");
  if (auto fn = present("--runcard-path")) {
    return *fn;
  }
  return default_runcard_path;
}

bool Argument_parser::runcard_path_is_used() const
{
  if (prefix.empty())
    return is_used("runcard_path");
  return is_used("--runcard-path");
}
