// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2024 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "mpi.h"
#include "print.h"
#include <argparse/argparse.hpp>
#include <filesystem>
#include <fstream>

namespace fs = std::filesystem;

/** \brief Wrapper class around argparse::ArgumentParser
 *
 *  This class is the command line argument parser. The actual parsing is left
 *  to the third-party argparse::ArgumentParser. We wrap it here, and add the
 *  concept of a prefix (to replace "--argument" with "--prefix-argument" if
 * needed) and some special handling for the positional runcard path argument.
 */
class Argument_parser {

public:
  explicit Argument_parser(std::string program_name, std::string _prefix = "");

  std::string prefixed_argument(std::string_view arg_name) const;

  template <typename... Targs>
  argparse::Argument& add_argument(Targs... _arg_specs)
  {
    // convert variadic arguments to an array
    std::array<std::string_view, sizeof...(Targs)> arg_specs {_arg_specs...};

    // define a lambda that allows us to apply our tuple to the templated
    // variadic ArgumentParser::add_argument, see
    // https://stackoverflow.com/questions/687490/how-do-i-expand-a-tuple-into-variadic-template-functions-arguments
    auto lambda = [&](auto&&... args) -> argparse::Argument& {
      return parser.add_argument(args...);
    };

    // compose argument tuple, begin with the required long-form version
    std::tuple<std::string> args {prefixed_argument(arg_specs.back())};

    // if there is no prefix given, and if we have a short-form arg (= flag),
    // add this first ...
    if (prefix.empty() && sizeof...(Targs) > 1)
      return std::apply(
          lambda,
          std::tuple_cat(std::make_tuple(arg_specs.front()), args));

    // ... otherwise, we simply forward our long-form arg only
    return std::apply(lambda, args);
  }

  // NOLINTNEXTLINE(cppcoreguidelines-avoid-c-arrays)
  void parse_args(int argc, const char* const argv[])
  {
    parser.parse_args(argc, argv);
  }

  template <typename T = std::string> T get(std::string_view arg_name) const
  {
    return parser.get<T>(prefixed_argument(arg_name));
  }

  template <typename T = std::string>
  auto present(std::string_view arg_name) const -> std::optional<T>
  {
    return parser.present(prefixed_argument(arg_name));
  }

  auto is_used(std::string_view arg_name) const
  {
    return parser.is_used(prefixed_argument(arg_name));
  }

  auto help() const -> std::stringstream { return parser.help(); }

  Argument_parser& add_description(std::string description)
  {
    parser.add_description(std::move(description));
    return *this;
  }

  Argument_parser& add_epilog(std::string epilog)
  {
    parser.add_epilog(std::move(epilog));
    return *this;
  }

  friend auto operator<<(std::ostream& s, const Argument_parser& p)
      -> std::ostream&
  {
    return s << p.parser;
  }

  void add_runcard_path_argument(const std::string& default_value);
  std::string runcard_path() const;
  bool runcard_path_is_used() const;

private:
  argparse::ArgumentParser parser;
  std::string prefix;
  std::string default_runcard_path;
};
