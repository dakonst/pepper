// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_PRINT_H
#define PEPPER_PRINT_H

#include "pepper/config.h"

#include "mpi.h"
#include <chrono>
#include <iostream>
#include <map>
#include <string>
#include <vector>

struct Settings;

namespace Msg {

enum class Verbosity {
  error,   // errors only
  warn,    // additionally, warnings
  info,    // additionally, some useful status updates and final run stats
  verbose, // additionally, lots of only occasionally relevant information
  debug    // additionally, information only relevant for debugging
};
Verbosity to_verbosity(const std::string&);
std::string to_string(Verbosity);

enum class Colourfulness {
  automatic, // use heuristics to determine if output is coloured
  always,
  never
};
Colourfulness to_colourfulness(const std::string&);
std::string to_string(Colourfulness);

inline Verbosity verbosity {Verbosity::info};
inline Colourfulness colourfulness {Colourfulness::automatic};
inline std::string bold {"\033[1m"};
inline std::string red {"\033[31m"};
inline std::string green {"\033[32m"};
inline std::string yellow {"\033[33m"};
inline std::string blue {"\033[34m"};
inline std::string magenta {"\033[35m"};
inline std::string cyan {"\033[36m"};
inline std::string reset {"\033[0m"};

inline unsigned long long print_counter {0};

void configure(const Settings&);

std::string version_string();
void print_banner();
void print_left_facing_bee_with_label(const std::string&);
void print_right_facing_bee();

} // namespace Msg

// Define a VAR function to print the name and the value of a single
// variable. This is not bound to a specific verbosity and can thus be used to
// quickly print extra information during development. A VAR statement should
// never appear in code on a production branch.
#define VAR(a)                                                                 \
  do {                                                                         \
    if (Mpi::is_main_rank())                                                   \
      println(Msg::blue, "[VAR] ", Msg::reset, #a, " = ", (a));                \
  } while (false)

#define VAR_ALL(a)                                                             \
  do {                                                                         \
    println(Msg::blue, "[VAR (Rank ", Mpi::rank(), ")] ", Msg::reset, #a,      \
            " = ", (a));                                                       \
  } while (false)

// Define a FUNC macro to print the name of the current function. This is
// not bound to a specific verbosity and can thus be used to quickly print
// extra information during development. A FUNC statement should never appear
// in code on a production branch.
#define FUNC()                                                                 \
  do {                                                                         \
    if (Mpi::is_main_rank())                                                   \
      println(Msg::cyan, "[FUNC] ", Msg::reset, __func__);                     \
  } while (false)

#define ERROR(...)                                                             \
  do {                                                                         \
    if (!Mpi::is_main_rank())                                                  \
      break;                                                                   \
    println(Msg::red, "ERROR: ", Msg::reset, __VA_ARGS__);                     \
  } while (false)

#define CHECK_WARN_ONCE(check, ...)                                            \
  do {                                                                         \
    if (!Mpi::is_main_rank())                                                  \
      break;                                                                   \
    static bool did_check {false};                                             \
    if (did_check)                                                             \
      break;                                                                   \
    did_check = true;                                                          \
    if (!(check)) {                                                              \
      if (Msg::verbosity >= Msg::Verbosity::warn) {                            \
        println(Msg::yellow, "WARNING: ", Msg::reset, __VA_ARGS__,             \
                " Further such warnings will be suppressed.");                 \
      }                                                                        \
    }                                                                          \
  } while (false)

#define WARN_ONCE(...)                                                         \
  do {                                                                         \
    if (!Mpi::is_main_rank())                                                  \
      break;                                                                   \
    static bool did_warn {false};                                              \
    if (did_warn)                                                              \
      break;                                                                   \
    did_warn = true;                                                           \
    if (Msg::verbosity >= Msg::Verbosity::warn) {                              \
      println(Msg::yellow, "WARNING: ", Msg::reset, __VA_ARGS__,               \
              " Further such warnings will be suppressed.");                   \
    }                                                                          \
  } while (false)

#define WARN(...)                                                              \
  do {                                                                         \
    if (!Mpi::is_main_rank())                                                  \
      break;                                                                   \
    if (Msg::verbosity >= Msg::Verbosity::warn)                                \
      println(Msg::yellow, "WARNING: ", Msg::reset, __VA_ARGS__);              \
  } while (false)

#define INFO(...)                                                              \
  do {                                                                         \
    if (!Mpi::is_main_rank())                                                  \
      break;                                                                   \
    if (Msg::verbosity >= Msg::Verbosity::info)                                \
      println(__VA_ARGS__);                                                    \
  } while (false)

// STATUS has the same verbosity of INFO, but is used to announce a new stage in
// the program execution.
#define STATUS(...)                                                            \
  do {                                                                         \
    if (!Mpi::is_main_rank())                                                  \
      break;                                                                   \
    if (Msg::verbosity >= Msg::Verbosity::info)                                \
      println(Msg::magenta, Msg::bold, __VA_ARGS__, Msg::reset);               \
  } while (false)

#define VERBOSE(...)                                                           \
  do {                                                                         \
    if (!Mpi::is_main_rank())                                                  \
      break;                                                                   \
    if (Msg::verbosity >= Msg::Verbosity::verbose)                             \
      println(__VA_ARGS__);                                                    \
  } while (false)

#define MARK_DEBUG Msg::magenta, "[DEBUG] ", Msg::reset
#define DEBUG(...)                                                             \
  do {                                                                         \
    if (!Mpi::is_main_rank())                                                  \
      break;                                                                   \
    if (Msg::verbosity >= Msg::Verbosity::debug)                               \
      println(MARK_DEBUG, __VA_ARGS__);                                        \
  } while (false)
#define DEBUG_ALL(...)                                                         \
  do {                                                                         \
    if (Msg::verbosity >= Msg::Verbosity::debug)                               \
    println(Msg::magenta, "[DEBUG (Rank ", Mpi::rank(), ") ", Msg::reset,      \
            __VA_ARGS__);                                                      \
  } while (false)


// Cf. https://cppbyexample.com/print_std_containers.html
template <typename T>
std::ostream& operator<<(std::ostream& o, const std::vector<T>& v)
{
  o << "[";
  if (v.empty()) {
    o << "]";
    return o;
  }
  // For every item except the last write "Item, "
  for (auto it = v.begin(); it != --v.end(); it++) {
    o << *it << ", ";
  }
  // Write out the last item
  o << v.back() << "]";
  return o;
}

template <typename T, size_t Size>
std::ostream& operator<<(std::ostream& o, const std::array<T, Size>& v)
{
  o << "[";
  if (v.empty()) {
    o << "]";
    return o;
  }
  // For every item except the last write "Item, "
  for (auto it = v.begin(); it + 1 != v.end(); it++) {
    o << *it << ", ";
  }
  // Write out the last item
  o << v.back() << "]";
  return o;
}

template <typename KeyT, typename ValueT>
std::ostream& operator<<(std::ostream& o, const std::map<KeyT, ValueT>& m)
{
  o << "{";
  if (m.empty()) {
    o << "}";
    return o;
  }
  // For every pair except the last write "Key: Value, "
  for (auto it = m.begin(); it != --m.end(); it++) {
    const auto& [key, value] = *it;
    o << key << ": " << value << ", ";
  }
  // Write out the last item
  const auto& [key, value] = *--m.end();
  o << key << ": " << value << "}";
  return o;
}

std::ostream& operator<<(std::ostream&, std::chrono::milliseconds);

// Cf. https://cppbyexample.com/just_print.html
template <typename... Args> void print(const Args&... args)
{
  (std::cerr << ... << args);
  Msg::print_counter++;
}

template <typename... Args> void println(const Args&... args)
{
  (std::cerr << ... << args) << "\n";
  Msg::print_counter++;
}

#endif
