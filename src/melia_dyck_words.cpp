// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "melia_dyck_words.h"
#include "sm.h"
#include <algorithm>
#include <cassert>
#include <queue>
#include <stack>

int next_dyck_word(int word);
std::vector<int> fill_bits(int n, int word);

// Generate all ways to put n indistinguishable balls into k
// distinguishable bins, or equivalently, orderings of n stars and k-1 bars,
// cf. https://en.wikipedia.org/wiki/Stars_and_bars_(combinatorics) and
// "Theorem two" therein
// Note that the number of arrangements is binom(n+k-1, k-1)
enum class Star_or_bar { Bar = 0, Star = 1 };
using Stars_and_bars = std::vector<Star_or_bar>;
using Stars_and_bars_vec = std::vector<Stars_and_bars>;
void generate_stars_bars_arrangements(int n_stars, int n_bars,
                                      Stars_and_bars_vec&,
                                      Stars_and_bars start = {});

Melia_dyck_words::Melia_dyck_words(const Flavour_process& _proc) : proc {_proc}
{
  fill_base_to_proc_perm();
  generate_words();
  generate_base();
  generate_flavoured_base();
  fill_proc_to_base_perm();
}

void Melia_dyck_words::generate_words()
{
  words.clear();

  // Generate quark dyck words (if any)
  std::vector<std::vector<int>> quark_dyck_arrays;
  if (proc.n_quark_pairs() < 2) {
    // If we have less than two quark pairs to begin with, we will not have
    // any inner quarks to build dyck words with.
    quark_dyck_arrays.emplace_back();
  }
  else {

    // Prepare iteration over integer dyck words.
    const int four_to_the_k {1 << 2 * (proc.n_quark_pairs() - 1)};
    const int last_word {four_to_the_k - (1 << (proc.n_quark_pairs() - 1))};
    const int first_word {2 * (four_to_the_k - 1) / 3};

    // Iterate over integer words, translating them to quark dyck words.
    for (int word {first_word}; word <= last_word;
         word = next_dyck_word(word)) {
      std::vector<int> word_bits =
          fill_bits(2 * (proc.n_quark_pairs() - 1), word);
      quark_dyck_arrays.push_back({});
      std::queue<int> quarks;
      for (int q {2}; q < 2 + (proc.n_quark_pairs() - 1); ++q)
        quarks.push(q);
      std::stack<int> anti_quarks;
      for (int i {0}; i < 2 * (proc.n_quark_pairs() - 1); ++i) {
        quark_dyck_arrays.back().resize((proc.n_quark_pairs() - 1) * 2);
        if (word_bits[i]) {
          // close, i.e. anti-quark
          quark_dyck_arrays.back()[i] = -anti_quarks.top();
          anti_quarks.pop();
        }
        else {
          // open, i.e. quark
          quark_dyck_arrays.back()[i] = quarks.front();
          anti_quarks.push(quarks.front());
          quarks.pop();
        }
      }
    }
  }

  // Now mix in gluons, they can be anywhere within or around the quark dyck
  // word. Use the stars and bars algorithm to find all ways to put the gluons
  // between the quarks, where the n - 2k gluons are the "Stars" and and the 2k
  // quarks are the "Bars".
  Stars_and_bars_vec orderings;
  if (proc.n_quark_pairs() == 0) {
    // no quarks at all, so 2 gluons will be on outside instead; hence we can
    // only permute n - 2 inner gluons
    orderings.emplace_back(Stars_and_bars(proc.n_qcd() - 2, Star_or_bar::Star));
  }
  else {
    generate_stars_bars_arrangements(proc.n_qcd() - 2 * proc.n_quark_pairs(),
                                     2 * proc.n_quark_pairs() - 2, orderings);
  }
  for (const auto& quark_dyck_array : quark_dyck_arrays) {
    for (const auto& ordering : orderings) {
      words.emplace_back();
      for (int i {0}, j {0}; i < proc.n_qcd() - 2; ++i) {
        words.back().resize(proc.n_qcd() - 2);
        if (ordering[i] == Star_or_bar::Bar) {
          words.back()[i] = quark_dyck_array[j];
          j++;
        }
        else {
          words.back()[i] = GLUON;
        }
      }
    }
  }

  // Lastly, add outer fermion or gluon pair.
  for (auto& array : words) {
    if (proc.n_quark_pairs() > 0) {
      array.insert(array.begin(), 1);
      array.insert(array.begin() + 1, -1);
    }
    else {
      array.insert(array.begin(), GLUON);
      array.push_back(GLUON);
    }
  }
}

void Melia_dyck_words::fill_base_to_proc_perm()
{
  // Determine base to process permutation.
  std::map<Ptcl_num, int> quark_pair_counts = proc.quark_pair_counts();
  const int n {proc.n_ptcl()};
  for (int i {0}; i < n; ++i) {
    if (proc[i] == GLUON) {
      // Gluons are at the beginning of the base Dyck word, right behind the
      // conjugated "outer" quark pair, which we insert below.
      base_to_proc_perm.push_back(i);
    }
  }
  bool did_order_front {false};
  // iterate flavours
  for (const auto& kv : quark_pair_counts) {
    int quark_idx {0}, antiquark_idx {0};
    // iterate pairs of given flavour
    for (int i {0}; i < kv.second; ++i) {
      // find next quark of the given flavour
      int idx {quark_idx};
      for (; idx < n; ++idx)
        if (kv.first == proc.particle_all_outgoing(idx))
          break;
      if (did_order_front) {
        base_to_proc_perm.insert(base_to_proc_perm.end(), idx);
      }
      else {
        base_to_proc_perm.insert(base_to_proc_perm.begin(), idx);
      }
      quark_idx = idx + 1;
      // find next anti-quark of the given flavour
      idx = antiquark_idx;
      for (; idx < n; ++idx)
        if (-kv.first == proc.particle_all_outgoing(idx))
          break;
      if (did_order_front) {
        base_to_proc_perm.insert(base_to_proc_perm.end(), idx);
      }
      else {
        base_to_proc_perm.insert(base_to_proc_perm.begin() + 1, idx);
      }
      antiquark_idx = idx + 1;
      did_order_front = true;
    }
  }
}

void Melia_dyck_words::generate_base()
{
  _base.clear();
  std::stack<int> quark_labels;
  const int n {proc.n_qcd()};
  for (int i {0}, quark_label {0}; i < n; ++i) {
    int ptcl {proc.particle_all_outgoing(base_to_proc_perm[i])};
    if (ptcl == GLUON) {
      _base.push_back(GLUON);
    }
    else if (ptcl > 0) {
      quark_label++;
      quark_labels.push(quark_label);
      _base.push_back(quark_label);
    }
    else {
      _base.push_back(-quark_labels.top());
      quark_labels.pop();
    }
  }
}

void Melia_dyck_words::generate_flavoured_base()
{
  _flavoured_base.clear();
  const int n {proc.n_qcd()};
  for (int i {0}; i < n; ++i) {
    _flavoured_base.push_back(proc.particle_all_outgoing(base_to_proc_perm[i]));
  }
}

void Melia_dyck_words::fill_proc_to_base_perm()
{
  _proc_to_base_perm.clear();
  const int n {proc.n_ptcl()};
  _proc_to_base_perm.resize(n, -1);
  for (int i {0}; i < n; ++i) {
    auto it =
        std::find(base_to_proc_perm.cbegin(), base_to_proc_perm.cend(), i);
    if (it != base_to_proc_perm.cend()) {
      _proc_to_base_perm[i] = std::distance(base_to_proc_perm.cbegin(), it);
    }
  }
}

std::vector<int> Melia_dyck_words::from_base_perm(std::vector<int> to)
{
  std::vector<int> perm;
  auto gluon_it {to.cbegin()};
  for (auto letter : base()) {
    if (letter == GLUON) {
      gluon_it = std::find(gluon_it, to.cend(), letter);
      perm.push_back(std::distance(to.cbegin(), gluon_it));
      gluon_it++;
    }
    else {
      auto it = std::find(to.cbegin(), to.cend(), letter);
      perm.push_back(std::distance(to.cbegin(), it));
    }
  }
  return perm;
}

// Cf. https://arxiv.org/pdf/1602.06426.pdf
// also for correct first/last dyck word used at caller side
int next_dyck_word(int w)
{
  const int a = w & -w;
  const int b = w + a;
  int c = w ^ b;
  c = (c / a >> 2) + 1;
  c = ((c * c - 1) & 0xaaaaaaaaaaaaaaaa) | b;
  return c;
}

// Cf. https://stackoverflow.com/a/2249738
std::vector<int> fill_bits(int n, int word)
{
  assert(word >= 0);
  std::vector<int> ret;
  for (int i {0}; i < n; ++i)
    ret.push_back((word & (1 << i)) >> i);
  return ret;
}

// Depth-first search to find all ways to arrange n_stars Stars and n_bars Bars
// (both being indistinguishable).
void generate_stars_bars_arrangements(int n_stars, int n_bars,
                                      Stars_and_bars_vec& arrs,
                                      Stars_and_bars start)
{
  if (int(start.size()) == n_stars + n_bars) {
    // all positions are occupied
    const int n_selected_stars =
        std::count(start.cbegin(), start.cend(), Star_or_bar::Star);
    if (n_selected_stars == n_stars) {
      // we have the right number of stars, store this result
      arrs.push_back(start);
      return;
    }
  }
  else {
    start.push_back(Star_or_bar::Star); // search on with a star
    generate_stars_bars_arrangements(n_stars, n_bars, arrs, start);
    start.back() = Star_or_bar::Bar; // search on with a bar
    generate_stars_bars_arrangements(n_stars, n_bars, arrs, start);
  }
}
