// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_FLOATING_POINT_COMPARISONS_H
#define PEPPER_FLOATING_POINT_COMPARISONS_H


#include "math.h"

struct is_nonzero {
  KOKKOS_INLINE_FUNCTION
  bool operator()(double x) { return x != 0.0; }
};

// This code is adapted from (also see the discussion therein):
// https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition
template <typename T>
KOKKOS_INLINE_FUNCTION bool almost_equal(T a, T b, T max_diff = 1e-16,
                                       T max_rel_diff = 1e-12)
{
  // Check if the numbers are really close -- needed
  // when comparing numbers near zero.
  T diff = std::abs(a - b);
  if (diff <= max_diff)
    return true;

  a = std::abs(a);
  b = std::abs(b);
  T largest = (b > a) ? b : a;

  return static_cast<bool>(diff <= largest * max_rel_diff);
}

template <typename T>
KOKKOS_INLINE_FUNCTION bool almost_zero(T a, T max_diff = 1e-16)
{
  return almost_equal(a, 0.0, max_diff);
}

template <typename T>
KOKKOS_INLINE_FUNCTION bool almost_zero(Complex::complex<T> a, T max_diff = 1e-16)
{
  return almost_zero(a.real(), max_diff) && almost_zero(a.imag(), max_diff);
}

#endif
