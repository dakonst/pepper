// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef VEGAS_KERNEL_H
#define VEGAS_KERNEL_H

#include "Kokkos_Core.hpp"
#include "event_data.h"
#include "compiler_warning_macros.h"
#include "kernel_macros.h"

template<typename event_data,typename pxi_type>
KOKKOS_INLINE_FUNCTION
void map_random_numbers(const event_data& evt,const int& i, 
                        const pxi_type& p_xi,
                        const int& m_nd,
                        const int& m_dim) {
  for (int j=0;j<m_dim;++j) {

    double xx = evt.r(i,j)*(double)m_nd;
    int ia = (int)xx;
    if (ia>=m_nd) ia=m_nd-1;
    if (ia==0) {
      evt.set_r(i,j,xx*p_xi(j,0));
    }
    else {
      double r_new = p_xi(j,ia-1);
      r_new += (xx-ia)*(p_xi(j,ia)-p_xi(j,ia-1));
      evt.set_r(i,j,r_new);
    }
  }
}

DEFINE_CUDA_KERNEL_NARGS_3(map_random_numbers, const pxi_type&, p_xi, const int&, m_nd, const int&, m_dim)

template<typename event_data, typename pxi_type>
KOKKOS_INLINE_FUNCTION
void generate_weight(const event_data& evt, const int& i,
                     const pxi_type& p_xi,
                     const int& m_nd, const int& m_dim,
                     const double& m_nc) {
  evt.w(i) *= m_nc;
  for (int j=0;j<m_dim;++j) {
    size_t l(0), r(m_nd-1), c((l+r)/2);
    double a(p_xi(j,c));
    while (r-l>1) {
      if (evt.r(i,j)<a) r=c;
      else l=c;
      c=(l+r)/2;
      a=p_xi(j,c);
    }
    if (evt.r(i,j)<p_xi(j,l)) r=l;
    int k(r);
    if (k==0) evt.w(i)*=p_xi(j,k);
    else evt.w(i) *=p_xi(j,k)-p_xi(j,k-1);
  }
}

DEFINE_CUDA_KERNEL_NARGS_4(generate_weight, const pxi_type&, p_xi, const int&, m_nd, const int&, m_dim,
                           const double&, m_nc)

// Since the methods below uses functions that are avaiable purely on the
// defice (the atomics) we cannot use our CUDA_CALLABLE macro but have
// to go back to __device__
template<typename event_data, typename pxi_type, typename pd_type, typename phit_type>
KOKKOS_INLINE_FUNCTION
void fill_grids_kernel(const event_data& evt,
                      const int& i,
                      const pxi_type& p_xi, const pd_type& p_d,
                      const phit_type& p_hit, const int& m_nd, const int& m_dim) {

  const double w = evt.me2(i)*evt.w(i)*evt.ps_bias(i);
  double v2=w*w;
  for (int j=0;j<m_dim;j++) {
    size_t l(0), r(m_nd-1), c((l+r)/2);
    double a(p_xi(j,c));
    while (r-l>1) {
      if (evt.r(i,j)<a) r=c;
      else l=c;
      c=(l+r)/2;
      a=p_xi(j,c);
    }
    if (evt.r(i,j)<p_xi(j,l)) r=l;
    int k(r);
    Kokkos::atomic_add(&p_d(j,k),v2);
    Kokkos::atomic_add(&p_hit(j,k),1);
  }
}
DEFINE_CUDA_KERNEL_NARGS_6(fill_grids_kernel, double*, p_xi, double*, p_d, double*, p_di, double*, p_hit, int, m_nd, int, m_dim);



#endif
