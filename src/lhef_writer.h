// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_LHEF_WRITER_H
#define PEPPER_LHEF_WRITER_H

#include "pepper/config.h"

#include "event_writer.h"
#include "sm.h"

#include "HepMC3/LHEF.h"

#include "zstr.hpp"

#include <array>

class Lhef_writer : public Event_writer {

public:
  Lhef_writer(const std::string& filepath, const Settings&,
              const SM_parameters&);
  Lhef_writer(std::ostream&, const Settings&, const SM_parameters&);

  void initialise(const Process&, const Pdf_and_alpha_s&) override;
  void finalise(const MC_result&) override;

  void write_n(const Event_handle&, int n, const MC_result&, long n_nonzero,
               long n_trials, const Process&) override;

  bool prints_leading_colour_information() const override
  {
    return leading_colour_flow_output_enabled;
  }

private:
  template <class T> void initialise_lhef_writer(T&& out)
  {
    lhef_writer = std::make_unique<LHEF::Writer>(out);
    std::vector<LHEF::XMLTag*> tags = LHEF::XMLTag::findXMLTags(
        "<generator name='" PROJECT_NAME "' version='" PROJECT_VERSION "'/>");
    assert(!tags.empty());
    lhef_writer->heprup.generators.push_back(LHEF::Generator {*tags[0]});
    LHEF::XMLTag::deleteAll(tags);
  }

  std::unique_ptr<LHEF::Writer> lhef_writer;

  std::array<Ptcl_num, 2> beams {NO_PTCL, NO_PTCL};
  bool unweighting_disabled;
  bool leading_colour_flow_output_enabled;
  std::vector<int> colours;
  std::vector<int> anticolours;
  double e_cms;
  double ew_alpha;
  size_t n_written {0};
  bool initialised {false};

  std::string filepath;
  std::ostream* stream {nullptr};
  std::unique_ptr<std::ostream> zstream;
};

#endif
