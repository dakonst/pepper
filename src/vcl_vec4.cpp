// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "vcl_vec4.h"
#include "cvec4.h"
#include "weyl.h"

Vcl_cvec4::Vcl_cvec4(const Scl_cvec4& v) : Vcl_cvec4 {v[0], v[1], v[2], v[3]} {}

Vcl_cvec4::Vcl_cvec4(const Weyl& a, const Weyl& b)
    : Vcl_cvec4 {a[0] * b[0] + a[1] * b[1], a[0] * b[1] + a[1] * b[0],
                 1i * (a[0] * b[1] - a[1] * b[0]), a[0] * b[0] - a[1] * b[1]}
{
}
