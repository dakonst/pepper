// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_PATHS_H
#define PEPPER_PATHS_H

#include <string>

struct Settings;
class Process;

namespace Paths {

extern std::string diagnostics;
extern std::string cache;
extern std::string temp;
extern std::string logs;

void create_paths(const Settings&, const Process&);

void erase_cache();

std::string path_with_rank_pattern(const std::string&);
std::string path_with_rank_info(const std::string&);

} // namespace Paths

#endif
