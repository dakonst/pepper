// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_MASSLESS_RAMBO_H
#define PEPPER_MASSLESS_RAMBO_H

#include "Kokkos_Core.hpp"
#include "phase_space.h"

class Massless_rambo : public Phase_space_generator {

public:
  Massless_rambo(const Flavour_process&, double e_cms);
  void fill_momenta_and_weights(Event_handle&) const override;
  void update_weights(Event_handle&) const override;
  int n_random_numbers() const override { return 4 * (_n_ptcl - 2); }

private:
  int _n_ptcl;
};

namespace detail {
  struct Mass_dresser_data {
    double e_cms;
    int itmax {6};
    double chi_start;
    double accuracy;
  };
}

class Mass_dresser {

public:
  Mass_dresser(const Flavour_process&, double e_cms);
  void transform_momenta(Event_handle&) const;
  void update_weights(Event_handle&) const;

private:
  Kokkos::View<detail::Mass_dresser_data> d_data;
  Kokkos::View<detail::Mass_dresser_data,Kokkos::HostSpace> h_data;
};

class Rambo : public Phase_space_generator {

public:
  Rambo(const Flavour_process&, double e_cms);

  void fill_momenta_and_weights(Event_handle&) const override;
  void update_weights(Event_handle&) const override;

  // This is only useful for testing purposes.
  void set_fixed_momenta(const std::vector<Vec4>& mom) override
  {
    fixed_momenta = mom;
  }

  int n_random_numbers() const override
  {
    return massless_rambo.n_random_numbers();
  }

private:
  void fill_fixed_momenta(Event_handle&) const;
  Massless_rambo massless_rambo;
  Mass_dresser mass_dresser;
  bool is_massive_case;
  std::vector<Vec4> fixed_momenta;
};

#endif
