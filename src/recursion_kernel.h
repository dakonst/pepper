// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_RECURSION_KERNEL_H
#define PEPPER_RECURSION_KERNEL_H

#include "event_handle.h"
#include "helicity.h"
#include "kernel_macros.h"
#include "math.h"
#include "permutations.h"
#include "recursion.h"
#include "sm.h"

template<typename Permutations_t,typename event_data,typename data_type>
KOKKOS_INLINE_FUNCTION void
prepare_momenta_kernel(const event_data& evt, int i, int perm_idx,
                       const Permutations_t& perm,
                       const int& perm_size,
                       const data_type& in_d)
{
  const auto& d = in_d();
  if (evt.w(i) == 0.0)
    return;

  // Copy external momenta into internal momentum storage re-ordered according
  // to the permutation, leaving out the momentum of the particle which takes
  // part in the final contraction only. Then calculate all partial sums.
  for (int to {0}; to < d.n - 1; ++to) {
    const int from {perm(perm_idx * perm_size + (to + 1))};
    evt.internal_e(i, to, evt.e(i, from));
    evt.internal_px(i, to, evt.px(i, from));
    evt.internal_py(i, to, evt.py(i, from));
    evt.internal_pz(i, to, evt.pz(i, from));
  }

  // Calculate internal momenta sums.
  for (int diag {0}, from2 {1}, to {d.n - 1}; diag < d.n - 2; ++diag) {
    // diag gives the diagonal of the "current matrix", with 0 being the main
    // diagonal, 1 the first side diagonal, etc.
    // Pick first index from main diagonal.
    for (int from1 {0}; from1 < d.n - 2 - diag; from1++, from2++, to++) {
      evt.internal_e(i, to, evt.internal_e(i, from1) + evt.internal_e(i, from2));
      evt.internal_px(i, to, evt.internal_px(i, from1) + evt.internal_px(i, from2));
      evt.internal_py(i, to, evt.internal_py(i, from1) + evt.internal_py(i, from2));
      evt.internal_pz(i, to, evt.internal_pz(i, from1) + evt.internal_pz(i, from2));
    }
    from2++;
  }
}
DEFINE_CUDA_KERNEL_NARGS_4(prepare_momenta_kernel, int, perm_idx,
                           const Permutations_t&, perm,
                           const int&, perm_size,
                           const event_data&, in_d)

template<typename Permutations_t,typename event_data,typename data_type>
KOKKOS_INLINE_FUNCTION void
prepare_currents_kernel(const event_data& evt, const int& i,const int& perm_idx,
                        const bool& should_resolve_helicity,
                        const Permutations_t& perm,
                        const int& perm_size,
                        const data_type& in_d,
                        const sm_properties& smprops)
{
  const auto& d = in_d();
  if (evt.w(i) == 0.0)
    return;

  // Copy external helicity states into internal current storage re-ordered
  // according to the permutation, leaving out the state of the particle which
  // takes part in the final contraction only. This is very similar to copying
  // the external momenta. The only difference is that in case of helicity
  // summing, we must copy the correct state according to the current helicity
  // configuration. Also copy the particle type.
  const int block {i / HELICITY_BLOCK_SIZE};
  for (int to {0}; to < d.n - 1; ++to) {
    const int from {perm(perm_idx * perm_size + (to + 1))};
    const Helicity hel {evt.helicity(block, from)};
    evt.internal_helicity(block, to, hel);
    Helicity h_for_access {hel};
    if (!should_resolve_helicity) {
      // To access currents with sampled helicities, we need to use unset.
      h_for_access = Helicity::unset;
    }
    const Ptcl_num ptcl {evt.ptcl(from)};
    evt.internal_ptcl(block, to, ptcl);
    if (ptcl == GLUON) {
      evt.set_internal_c(i, to, evt.c(i, from, h_for_access));
    }
    else {
      // For efficiency reasons, massless non-zero spinor components are
      // always stored in the first two components (then we do never need to
      // access the other components).
      evt.internal_c0(i, to, evt.c0(i, from, h_for_access));
      evt.internal_c1(i, to, evt.c1(i, from, h_for_access));
      if (smprops.mass(ptcl) == 0.0) {
        evt.internal_c2(i, to, 0.0);
        evt.internal_c3(i, to, 0.0);
      }
      else {
        evt.internal_c2(i, to, evt.c2(i, from, h_for_access));
        evt.internal_c3(i, to, evt.c3(i, from, h_for_access));
      }
    }
  }
}

DEFINE_CUDA_KERNEL_NARGS_6(prepare_currents_kernel, const int&, perm_idx,
                           const bool&, should_resolve_helicity, const Permutations_t&,
                           perm, const int&, perm_size, const data_type&, in_d,
                           const sm_properties&, smprops)

template<typename event_data, typename data_type>
KOKKOS_INLINE_FUNCTION void reset_internal_currents_kernel(const event_data& evt, const int& i,
                                           const data_type& in_d)
{
  const auto& d = in_d();
  if (evt.w(i) == 0.0)
    return;

  for (int to {d.n - 1}; to < evt.n_internal_states; ++to) {
    evt.internal_c0(i, to, 0.0);
    evt.internal_c1(i, to, 0.0);
    evt.internal_c2(i, to, 0.0);
    evt.internal_c3(i, to, 0.0);
  }
}
DEFINE_CUDA_KERNEL_NARGS_1(reset_internal_currents_kernel,
                           const data_type&, in_d)


template<typename event_data, typename data_type>
KOKKOS_INLINE_FUNCTION void reset_internal_particles_kernel(const event_data& evt,const int& b,
                                            const data_type& in_d)
{
  const auto& d = in_d();
  constexpr auto _NO_PTCL = NO_PTCL;
  for (int to {d.n - 1}; to < d.n * (d.n - 1) / 2; ++to) {
    evt.internal_ptcl(b, to, _NO_PTCL);
  }
}
DEFINE_CUDA_KERNEL_NARGS_1(reset_internal_particles_kernel,
                           const data_type&, in_d)

template<typename event_data,typename data_type>
KOKKOS_INLINE_FUNCTION void reset_internal_currents_kernel(const event_data& evt, int i,
                                           const data_type& in_d,
                                           int min_row, int min_column)
{
  const auto& d = in_d();
  if (evt.w(i) == 0.0)
    return;
  
  // Iterate diagonals of the current matrix.
  for (int m {1}; m < d.n; m++) {

    // Iterate entries on the mth diagonal.
    for (int k {0}; k < d.n - m - 1; k++) {
      if (is_readonly(m, k, d.n, min_row, min_column))
        continue;
      const int current {index(k, k + m, d.n)};
      evt.internal_c0(i, current, 0.0);
      evt.internal_c1(i, current, 0.0);
      evt.internal_c2(i, current, 0.0);
      evt.internal_c3(i, current, 0.0);
    }
  }
}
DEFINE_CUDA_KERNEL_NARGS_3(reset_internal_currents_kernel,
                           const data_type&, in_d, int, min_row, int,
                           min_column)
  

template<typename event_data,typename data_type>
KOKKOS_INLINE_FUNCTION void reset_internal_particles_kernel(const event_data& evt, const int& b,
                                            const data_type& in_d,
                                            int min_row, int min_column)
{
  const auto& d = in_d();
  constexpr auto _NO_PTCL = NO_PTCL;
  for (int m {1}; m < d.n; m++) {
    for (int k {0}; k < d.n - m - 1; k++) {
      if (is_readonly(m, k, d.n, min_row, min_column))
        continue;
      const int current {index(k, k + m, d.n)};
      evt.internal_ptcl(b, current, _NO_PTCL);
    }
  }
}
DEFINE_CUDA_KERNEL_NARGS_3(reset_internal_particles_kernel,
                           const data_type&, in_d, int, min_row, int,
                           min_column)

template<typename event_data>
KOKKOS_INLINE_FUNCTION void evaluate_ggg_vertex_kernel(const event_data& evt, int i, int i1,
                                       int i2, int i3)
{
  CVec4 ja {evt.internal_c(i, i1)};
  CVec4 jb {evt.internal_c(i, i2)};
  Vec4 pa {evt.internal_p(i, i1)};
  Vec4 pb {evt.internal_p(i, i2)};

  constexpr auto _GLUON = GLUON;
  Vec4 pab {pa + pb};
  CVec4 jc =
      (ja * (pb + pab)) * jb + (ja * jb) * (pa - pb) - (jb * (pa + pab)) * ja;
  jc *= -1.0;

  evt.internal_c0(i, i3, evt.internal_c0(i, i3) + jc[0]);
  evt.internal_c1(i, i3, evt.internal_c1(i, i3) + jc[1]);
  evt.internal_c2(i, i3, evt.internal_c2(i, i3) + jc[2]);
  evt.internal_c3(i, i3, evt.internal_c3(i, i3) + jc[3]);

  const int block {i / HELICITY_BLOCK_SIZE};
  evt.internal_helicity(block, i3, Helicity::unset);
  evt.internal_ptcl(block, i3, _GLUON);
}

template<typename event_data>
KOKKOS_INLINE_FUNCTION void evaluate_fzf_vertex_kernel(const event_data& evt, int i, int i1,
                                       int i2, int i3, Ptcl_num p, C coupling)
{
  const int block {i / HELICITY_BLOCK_SIZE};
  const Helicity h {evt.internal_helicity(block, i1)};

  // Obtain fermion spinor s and Z-boson 4-vector j
  const C s0 {evt.internal_c0(i, i1)};
  const C s1 {evt.internal_c1(i, i1)};
  CVec4 j {evt.internal_c(i, i2)};

  // Calculate Eq. (55)+(57) of arXiv:0808.3674v1
  const C j_plus {j.p_plus()};
  const C j_minus {j.p_minus()};
  const C j_perp {j[1] + C(0,1) * j[2]};
  const C j_perp_star {j[1] - C(0,1) * j[2]};
  C upper, lower;
  if (p > 0) {
    // ubar-j vertex
    if (h == Helicity::plus) {
      upper = s0 * j_minus - s1 * j_perp;
      lower = -s0 * j_perp_star + s1 * j_plus;
    }
    else {
      upper = s0 * j_plus + s1 * j_perp;
      lower = s0 * j_perp_star + s1 * j_minus;
    }
  }
  else {
    // j-v vertex
    if (h == Helicity::plus) {
      upper = s0 * j_plus + s1 * j_perp_star;
      lower = s0 * j_perp + s1 * j_minus;
    }
    else {
      upper = s0 * j_minus - s1 * j_perp_star;
      lower = -s0 * j_perp + s1 * j_plus;
    }
  }

  upper *= coupling;
  lower *= coupling;
  evt.internal_c0(i, i3, evt.internal_c0(i, i3) + upper);
  evt.internal_c1(i, i3, evt.internal_c1(i, i3) + lower);

  evt.internal_helicity(block, i3, h);
  evt.internal_ptcl(block, i3, p);
}

template<typename event_data>
KOKKOS_INLINE_FUNCTION void evaluate_massless_qgq_vertex_kernel(const event_data& evt, int i,
                                                int i1, int i2, int i3,
                                                Ptcl_num p, C coupling)
{
  const int block {i / HELICITY_BLOCK_SIZE};
  const Helicity h {evt.internal_helicity(block, i1)};

  // Obtain quark spinor s and gluon 4-vector j
  const C s0 {evt.internal_c0(i, i1)};
  const C s1 {evt.internal_c1(i, i1)};
  CVec4 j {evt.internal_c(i, i2)};

  // Calculate Eq. (55)+(57) of arXiv:0808.3674v1
  const C j_plus {j.p_plus()};
  const C j_minus {j.p_minus()};
  const C j_perp {j[1] + C(0,1) * j[2]};
  const C j_perp_star {j[1] - C(0,1) * j[2]};
  C upper, lower;
  if (p > 0) {
    // ubar-j vertex
    if (h == Helicity::plus) {
      upper = s0 * j_minus - s1 * j_perp;
      lower = -s0 * j_perp_star + s1 * j_plus;
    }
    else {
      upper = s0 * j_plus + s1 * j_perp;
      lower = s0 * j_perp_star + s1 * j_minus;
    }
  }
  else {
    // j-v vertex
    if (h == Helicity::plus) {
      upper = s0 * j_plus + s1 * j_perp_star;
      lower = s0 * j_perp + s1 * j_minus;
    }
    else {
      upper = s0 * j_minus - s1 * j_perp_star;
      lower = -s0 * j_perp + s1 * j_plus;
    }
  }
  upper *= coupling;
  lower *= coupling;
  evt.internal_c0( i, i3, evt.internal_c0( i, i3) + upper);
  evt.internal_c1( i, i3, evt.internal_c1( i, i3) + lower);
  evt.internal_helicity( block, i3, evt.internal_helicity(block, i1));
  evt.internal_ptcl( block, i3, p);
}

template<typename event_data>
KOKKOS_INLINE_FUNCTION void evaluate_massive_qgq_vertex_kernel(const event_data& evt, int i,
                                               int i1, int i2, int i3,
                                               Ptcl_num p, C coupling)
{
  // Obtain quark spinor s and gluon 4-vector j
  CVec4 s {evt.internal_c(i, i1)};
  CVec4 j {evt.internal_c(i, i2)};

  // Calculate Eq. (55)+(57) of arXiv:0808.3674v1
  const C j_plus {j.p_plus()};
  const C j_minus {j.p_minus()};
  const C j_perp {j[1] + C(0,1) * j[2]};
  const C j_perp_star {j[1] - C(0,1) * j[2]};
  Scl_cvec4 s_new;
  if (p > 0) {
    s_new[0] = s[2] * j_plus + s[3] * j_perp;
    s_new[1] = s[2] * j_perp_star + s[3] * j_minus;
    s_new[2] = s[0] * j_minus - s[1] * j_perp;
    s_new[3] = -s[0] * j_perp_star + s[1] * j_plus;
  }
  else {
    s_new[0] = s[2] * j_minus - s[3] * j_perp_star;
    s_new[1] = -s[2] * j_perp + s[3] * j_plus;
    s_new[2] = s[0] * j_plus + s[1] * j_perp_star;
    s_new[3] = s[0] * j_perp + s[1] * j_minus;
  }
  s_new *= coupling;
  evt.internal_c0(i, i3, evt.internal_c0(i, i3) + s_new[0]);
  evt.internal_c1(i, i3, evt.internal_c1(i, i3) + s_new[1]);
  evt.internal_c2(i, i3, evt.internal_c2(i, i3) + s_new[2]);
  evt.internal_c3(i, i3, evt.internal_c3(i, i3) + s_new[3]);

  const int block {i / HELICITY_BLOCK_SIZE};
  evt.internal_helicity(block, i3, Helicity::unset);
  evt.internal_ptcl(block, i3, p);
}

template<typename event_data>
KOKKOS_INLINE_FUNCTION void evaluate_ffz_vertex_kernel(const event_data& evt, int i, int i1,
                                       int i2, int i3, C coupling)
{
  const int block {i / HELICITY_BLOCK_SIZE};
  const Helicity h1 {evt.internal_helicity(block, i1)};

  const C u0 {evt.internal_c0(i, i1)};
  const C u1 {evt.internal_c1(i, i1)};
  const C v0 {evt.internal_c0(i, i2)};
  const C v1 {evt.internal_c1(i, i2)};

  // Calculate Eq. (56)+(58) of arXiv:0808.3674v1
  const C u0v0 {u0 * v0};
  const C u1v1 {u1 * v1};
  const C u0v1 {u0 * v1};
  const C u1v0 {u1 * v0};
  Scl_cvec4 j;
  if (h1 == Helicity::plus) {
    // right-handed
    j[0] = u0v0 + u1v1;
    j[1] = u0v1 + u1v0;
    j[2] = C(0,1) * (u1v0 - u0v1);
    j[3] = u0v0 - u1v1;
  }
  else {
    // left-handed
    j[0] = u0v0 + u1v1;
    j[1] = -(u0v1 + u1v0);
    j[2] = C(0,1) * (u0v1 - u1v0);
    j[3] = u1v1 - u0v0;
  }

  j *= coupling;

  evt.internal_c0(i, i3, evt.internal_c0(i, i3) + j[0]);
  evt.internal_c1(i, i3, evt.internal_c1(i, i3) + j[1]);
  evt.internal_c2(i, i3, evt.internal_c2(i, i3) + j[2]);
  evt.internal_c3(i, i3, evt.internal_c3(i, i3) + j[3]);

  evt.internal_helicity(block, i3, Helicity::unset);
}

template<typename event_data>
KOKKOS_INLINE_FUNCTION void evaluate_massless_ffv_vertex_kernel(const event_data& evt, int i,
                                                int i1, int i2, int i3,
                                                Ptcl_num p3)
{
  const int block {i / HELICITY_BLOCK_SIZE};
  const Helicity h1 {evt.internal_helicity(block, i1)};

  const C u0 {evt.internal_c0(i, i1)};
  const C u1 {evt.internal_c1(i, i1)};
  const C v0 {evt.internal_c0(i, i2)};
  const C v1 {evt.internal_c1(i, i2)};

  // Calculate Eq. (56)+(58) of arXiv:0808.3674v1
  const C u0v0 {u0 * v0};
  const C u1v1 {u1 * v1};
  const C u0v1 {u0 * v1};
  const C u1v0 {u1 * v0};
  Scl_cvec4 j;
  if (h1 == Helicity::plus) {
    j[0] = u0v0 + u1v1;
    j[1] = u0v1 + u1v0;
    j[2] = C(0,1) * (u1v0 - u0v1);
    j[3] = u0v0 - u1v1;
  }
  else {
    j[0] = u0v0 + u1v1;
    j[1] = -(u0v1 + u1v0);
    j[2] = C(0,1) * (u0v1 - u1v0);
    j[3] = u1v1 - u0v0;
  }

  // NOTE: No additional coupling factor present.

  evt.internal_c0(i, i3, evt.internal_c0(i, i3) + j[0]);
  evt.internal_c1(i, i3, evt.internal_c1(i, i3) + j[1]);
  evt.internal_c2(i, i3, evt.internal_c2(i, i3) + j[2]);
  evt.internal_c3(i, i3, evt.internal_c3(i, i3) + j[3]);

  evt.internal_helicity(block, i3, Helicity::unset);
  evt.internal_ptcl(block, i3, p3);
}

template<typename event_data>
KOKKOS_INLINE_FUNCTION void evaluate_massive_ffv_vertex_kernel(const event_data& evt, int i,
                                               int i1, int i2, int i3, Ptcl_num p3)
{
  const CVec4 u {evt.internal_c(i, i1)};
  const CVec4 v {evt.internal_c(i, i2)};

  // Cf. Sherpa/METOOLS/Vertices/FFV_LC.C
  const C u3v1 {u[3] * v[1]};
  const C u2v0 {u[2] * v[0]};
  const C u2v1 {-u[2] * v[1]};
  const C u3v0 {-u[3] * v[0]};

  const C u0v2 {u[0] * v[2]};
  const C u1v3 {u[1] * v[3]};
  const C u0v3 {u[0] * v[3]};
  const C u1v2 {u[1] * v[2]};

  Scl_cvec4 j;
  j[0] = u3v1 + u2v0 + u0v2 + u1v3;
  j[1] = u2v1 + u3v0 + u0v3 + u1v2;
  j[2] = -C(0,1) * (u2v1 - u3v0 + u0v3 - u1v2);
  j[3] = u3v1 - u2v0 + u0v2 - u1v3;

  // NOTE: No additional coupling factor present.

  evt.internal_c0(i, i3, evt.internal_c0(i, i3) + j[0]);
  evt.internal_c1(i, i3, evt.internal_c1(i, i3) + j[1]);
  evt.internal_c2(i, i3, evt.internal_c2(i, i3) + j[2]);
  evt.internal_c3(i, i3, evt.internal_c3(i, i3) + j[3]);

  const int block {i / HELICITY_BLOCK_SIZE};
  evt.internal_helicity(block, i3, Helicity::unset);
  evt.internal_ptcl(block, i3, p3);
}

template<typename event_data>
KOKKOS_INLINE_FUNCTION void evaluate_gggg_vertex_kernel(const event_data& evt, int i, int i1,
                                        int i2, int i3, int i4)
{
  CVec4 ja {evt.internal_c(i, i1)};
  CVec4 jb {evt.internal_c(i, i2)};
  CVec4 jc {evt.internal_c(i, i3)};

  CVec4 j {2.0 * (ja * jc) * jb - (ja * jb) * jc - (jc * jb) * ja};

  // NOTE: No additional coupling factor present.

  evt.internal_c0(i, i4, evt.internal_c0(i, i4) + j[0]);
  evt.internal_c1(i, i4, evt.internal_c1(i, i4) + j[1]);
  evt.internal_c2(i, i4, evt.internal_c2(i, i4) + j[2]);
  evt.internal_c3(i, i4, evt.internal_c3(i, i4) + j[3]);

  constexpr auto _GLUON = GLUON;
  const int block {i / HELICITY_BLOCK_SIZE};
  evt.internal_helicity(block, i4, Helicity::unset);
  evt.internal_ptcl(block, i4, _GLUON);
}

template<typename event_data>
KOKKOS_INLINE_FUNCTION void evaluate_massless_boson_propagator_kernel(const event_data& evt,
                                                                      int i, int i1,
                                                                      int i1prime
)
{  // Evaluate propagator of the i'th event, for the i1'th internal momentum, and
  // multiply the result to the i1prime'th internal current. Usually, i1 =
  // i1prime, but we use i1 != i1prime for the simultaneous evaluation of the
  // Z/boson propagator, where i1prime is the auxiliary photon current index.
  Vec4 mom {evt.internal_p(i, i1)};
  double prop {1.0 / mom.abs2()};
  evt.internal_c0(i, i1prime, evt.internal_c0(i, i1prime) * prop);
  evt.internal_c1(i, i1prime, evt.internal_c1(i, i1prime) * prop);
  evt.internal_c2(i, i1prime, evt.internal_c2(i, i1prime) * prop);
  evt.internal_c3(i, i1prime, evt.internal_c3(i, i1prime) * prop);
}

template<typename event_data>
KOKKOS_INLINE_FUNCTION void evaluate_massless_boson_propagator_kernel(const event_data& evt,
                                                                      int i, int i1)
{
  evaluate_massless_boson_propagator_kernel(evt, i, i1, i1);
}


template<typename event_data>
KOKKOS_INLINE_FUNCTION void evaluate_massive_boson_propagator_kernel(const event_data& evt,
                                                     int i, int i1,
                                                     C complex_mass2)
{
  const Vec4 mom {evt.internal_p(i, i1)};
  const C prop {1.0 / (mom.abs2() - complex_mass2)};
  evt.internal_c0(i, i1, evt.internal_c0(i, i1) * prop);
  evt.internal_c1(i, i1, evt.internal_c1(i, i1) * prop);
  evt.internal_c2(i, i1, evt.internal_c2(i, i1) * prop);
  evt.internal_c3(i, i1, evt.internal_c3(i, i1) * prop);
}

template<typename event_data>
KOKKOS_INLINE_FUNCTION void evaluate_massless_quark_propagator_kernel(const event_data& evt,
                                                      int i, int i1, Ptcl_num p)
{
  const int block {i / HELICITY_BLOCK_SIZE};
  const Helicity h {evt.internal_helicity(block, i1)};

  const Vec4 mom {evt.internal_p(i, i1)};
  const C prefac {C(0,1) / mom.abs2()};
  const double p_plus {mom.p_plus()};
  const double p_minus {mom.p_minus()};
  const C p_perp {mom[1] + C(0,1) * mom[2]};
  const C p_perp_star {Complex::conj(p_perp)};
  const C s0 {evt.internal_c0(i, i1)};
  const C s1 {evt.internal_c1(i, i1)};
  C upper, lower;
  if (p > 0) {
    // ubar * pslash
    if (h == Helicity::plus) {
      upper = s0 * p_plus + s1 * p_perp;
      lower = s0 * p_perp_star + s1 * p_minus;
    }
    else {
      upper = s0 * p_minus - s1 * p_perp;
      lower = -s0 * p_perp_star + s1 * p_plus;
    }
  }
  else {
    // (-pslash) * v
    if (h == Helicity::plus) {
      upper = -s0 * p_minus + s1 * p_perp_star;
      lower = s0 * p_perp - s1 * p_plus;
    }
    else {
      upper = -s0 * p_plus - s1 * p_perp_star;
      lower = -s0 * p_perp - s1 * p_minus;
    }
  }
  evt.internal_c0(i, i1,prefac * upper);
  evt.internal_c1(i, i1,prefac * lower);
  evt.internal_c2(i, i1,0.0);
  evt.internal_c3(i, i1,0.0);
}

template<typename event_data>
KOKKOS_INLINE_FUNCTION void evaluate_massive_quark_propagator_kernel(const event_data& evt,
                                                     int i, int i1, Ptcl_num p,
                                                     C complex_mass2)
{
  const C complex_mass {Complex::sqrt(complex_mass2)};

  const Vec4 mom {evt.internal_p(i, i1)};
  const C prefac {C(0,1) / (mom.abs2() - complex_mass2)};
  const double p_plus {mom.p_plus()};
  const double p_minus {mom.p_minus()};
  const C p_perp {mom[1] + C(0,1) * mom[2]};
  const C p_perp_star {Complex::conj(p_perp)};
  CVec4 s {evt.internal_c(i, i1)};
  Scl_cvec4 j;
  if (p > 0) {
    j[0] = s[2] * p_plus + s[3] * p_perp;
    j[1] = s[2] * p_perp_star + s[3] * p_minus;
    j[2] = s[0] * p_minus - s[1] * p_perp;
    j[3] = -s[0] * p_perp_star + s[1] * p_plus;
  }
  else {
    j[0] = -s[2] * p_minus + s[3] * p_perp_star;
    j[1] = s[2] * p_perp - s[3] * p_plus;
    j[2] = -s[0] * p_plus - s[1] * p_perp_star;
    j[3] = -s[0] * p_perp - s[1] * p_minus;
  }
  evt.set_internal_c(i, i1, (j + complex_mass * s) * prefac);
}

template<typename event_data,typename data_type>
KOKKOS_INLINE_FUNCTION void update_matrix_element_kernel(const event_data& evt, int i,
                                         int qcd_perm_idx, double perm_sign,
                                         int final_idx, Ptcl_num pb,
                                         bool should_resolve_helicity,
                                         const data_type& in_d,
                                         const sm_properties& smprops)
{
  const auto& d = in_d();
  if (evt.w(i) == 0.0)
    return;

  const int block {i / HELICITY_BLOCK_SIZE};
  const Helicity final_h {evt.helicity(block, final_idx)};
  Helicity h_for_access {final_h};
  if (!should_resolve_helicity) {
    // To access currents with sampled helicities, we need to use unset.
    h_for_access = Helicity::unset;
  }

  const int last_idx {d.n * (d.n - 1) / 2 - 1};
  const Helicity last_h {evt.internal_helicity(block, last_idx)};

  const CVec4 ja {evt.internal_c(i, last_idx)};
  const CVec4 jb {evt.c(i, final_idx, h_for_access)};
  C me {0.0};
  if (pb != GLUON) {
    if (smprops.mass(pb) == 0.0) {
      if (final_h != last_h)
        me = ja[0] * jb[0] + ja[1] * jb[1];
    }
    else {
      me = ja[0] * jb[0] + ja[1] * jb[1] + ja[2] * jb[2] + ja[3] * jb[3];
    }
  }
  else {
    me = ja * jb;
  }
  evt.me_real(i, qcd_perm_idx) = evt.me_real(i, qcd_perm_idx) + perm_sign * me.real();
  evt.me_imag(i, qcd_perm_idx) = evt.me_imag(i, qcd_perm_idx) + perm_sign * me.imag();
}
DEFINE_CUDA_KERNEL_NARGS_6(update_matrix_element_kernel, int, qcd_perm_idx,
                           double, perm_sign, int, final_idx, Ptcl_num, pb,
                           bool, should_resolve_helicity,
                           const data_type&, in_d)

template<typename event_data,typename data_type>
KOKKOS_INLINE_FUNCTION void update_matrix_element_with_cached_summing_kernel(
    const event_data& evt, int i, int qcd_perm_idx, double perm_sign,
    int final_idx, Ptcl_num pb,
    const data_type& in_d,
    const sm_properties& smprops)
{
  const auto d = in_d();
  if (evt.w(i) == 0.0)
    return;

  const int block {i / HELICITY_BLOCK_SIZE};
  const int last_idx {d.n * (d.n - 1) / 2 - 1};
  const Helicity last_h {evt.internal_helicity(block, last_idx)};

  const CVec4 ja {evt.internal_c(i, last_idx)};
  const CVec4 jb_minus {evt.c(i, final_idx, Helicity::minus)};
  const CVec4 jb_plus {evt.c(i, final_idx, Helicity::plus)};
  C me_minus {0.0, 0.0};
  C me_plus {0.0, 0.0};
  if (pb != GLUON) {
    if (smprops.mass(pb) == 0.0) {
      if (Helicity::minus != last_h) {
        me_minus = ja[0] * jb_minus[0] + ja[1] * jb_minus[1];
      }
      else {
        me_plus = ja[0] * jb_plus[0] + ja[1] * jb_plus[1];
      }
    }
    else {
      me_plus = ja[0] * jb_plus[0] + ja[1] * jb_plus[1] +
                ja[2] * jb_plus[2] + ja[3] * jb_plus[3];
      me_minus = ja[0] * jb_minus[0] + ja[1] * jb_minus[1] +
                 ja[2] * jb_minus[2] + ja[3] * jb_minus[3];
    }
  }
  else {
    me_plus = ja * jb_plus;
    me_minus = ja * jb_minus;
  }
  evt.me_real(i, qcd_perm_idx) = (evt.me_real(i, qcd_perm_idx) + perm_sign * me_plus.real());
  evt.me_imag(i, qcd_perm_idx) = (evt.me_imag(i, qcd_perm_idx) + perm_sign * me_plus.imag());
  evt.alt_me_real(i, qcd_perm_idx, evt.alt_me_real(i, qcd_perm_idx) + perm_sign * me_minus.real());
  evt.alt_me_imag(i, qcd_perm_idx, evt.alt_me_imag(i, qcd_perm_idx) + perm_sign * me_minus.imag());
}
DEFINE_CUDA_KERNEL_NARGS_6(update_matrix_element_with_cached_summing_kernel,
                           int, qcd_perm_idx, double, perm_sign, int, final_idx,
                           Ptcl_num, pb, const data_type&, in_d,
                           const sm_properties&, smprops)

template<typename event_data>
KOKKOS_INLINE_FUNCTION void evaluate_fzf_vertex(const event_data& evt, int i,
                                              int i1, int i2, int i3,
                                              Ptcl_num p,
                                              const detail::BG_recursion_data& d,
                                              const sm_properties& smprops)
{
  const int block {i / HELICITY_BLOCK_SIZE};
  const Helicity hel {evt.internal_helicity(block, i1)};
  const double p_charge {smprops.charge(std::abs(p))};
  C coupling;
  if ((p > 0) ^ (hel == Helicity::minus)) {
    // right-handed current
    coupling = -C(0,1) * p_charge * d.stripped_g_ew_right;
  }
  else {
    // left-handed current
    const double weak_isospin {p_charge < 0 ? -0.5 : 0.5};
    coupling = C(0,1) * (p_charge * d.stripped_g_ew_left_1 +
                     weak_isospin * d.stripped_g_ew_left_2);
  }
  evaluate_fzf_vertex_kernel(evt, i, i1, i2, i3, p, coupling);
}

template<typename event_data>
KOKKOS_INLINE_FUNCTION void evaluate_fvf_vertex(const event_data& evt, int i, int i1, int i2,
                                int i3, Ptcl_num p1, Ptcl_num p2,
                                const detail::BG_recursion_data& d,
                                const sm_properties& smprops)
{
  // For additional minus sign depending on the ordering, see (A.1c) in
  // arXiv:1507.00332v3. For photons, there is no such minus sign, since it is
  // an artifact of colour ordering.
  C coupling {0.0, 1.0};
  bool should_swap {false};
    if (p2 == GLUON) {
    if (p1 > 0)
      coupling *= -1.0;
  }
  else if (p1 == GLUON) {
    should_swap = true;
    if (p2 < 0)
      coupling *= -1.0;
  }
  else if (is_neutral_gauge_boson(p1)) {
    should_swap = true;
  }
  if (should_swap) {
    // Reorder to put the vector boson at position 2
    Complex::swap(i1, i2);
    Complex::swap(p1, p2);
  }
  if (p2 == PHOTON_AND_Z) {
    // TODO: Extend to massive fermions. This is trivial for the photon vertex,
    // but we are missing an implementation for the Z vertex.

    // First treat the Z, as usual.
    evaluate_fzf_vertex(evt, i, i1, i2, i3, p1, d, smprops);

    // Now treat the photon, where i2prime is used instead of i2. At i2prime we
    // stored the ffv * propagator current of the photon in evaluate_ffv_vertex.
    // Note that the same i3 is used as for the Z, i.e. the result is added to
    // the same outgoing index.
    const int i2prime {evt.auxiliary_photon_current_idx()};
    coupling *= smprops.charge(std::abs(p1));
    evaluate_massless_qgq_vertex_kernel(evt, i, i1, i2prime, i3, p1, coupling);
  }
  else if (p2 == Z) {
    evaluate_fzf_vertex(evt, i, i1, i2, i3, p1, d, smprops);
  }
  else {
    if (p2 == PHOTON) {
      // TODO: Should we not multiply by charge(p1) here, to take into account
      // the opposite charge of antiparticles? But then the result is not
      // correct anymore.
      coupling *= smprops.charge(std::abs(p1));
    }
    const double m {smprops.mass(p1)};
    if (m == 0.0) {
      evaluate_massless_qgq_vertex_kernel(evt, i, i1, i2, i3, p1, coupling);
    }
    else {
      evaluate_massive_qgq_vertex_kernel(evt, i, i1, i2, i3, p1, coupling);
  }
  }
}

template<typename event_data>
KOKKOS_INLINE_FUNCTION void evaluate_ffz_vertex(const event_data& evt, int i,
                                              int i1, int i2, int i3,
                                              const detail::BG_recursion_data& d)
{ 
  constexpr auto _NO_PTCL = NO_PTCL;
  const int block {i / HELICITY_BLOCK_SIZE};
  const Helicity h1 {evt.internal_helicity(block, i1)};
  const Helicity h2 {evt.internal_helicity(block, i2)};
  if (h1 == h2) {
    evt.internal_ptcl(block, i3, _NO_PTCL);
    return;
  }

  const double charge {1};
  C coupling;
  if (h1 == Helicity::plus) {
    // right-handed current
    coupling = -1.0 * charge * d.stripped_g_ew_right;
  }
  else {
    // left-handed current
    const double weak_isospin {0.5};
    coupling = 1.0 * (charge * d.stripped_g_ew_left_1 +
                      weak_isospin * d.stripped_g_ew_left_2);
  }

  evaluate_ffz_vertex_kernel(evt, i, i1, i2, i3, coupling);
}

template<typename event_data>
KOKKOS_INLINE_FUNCTION void evaluate_massless_ffv_vertex(const event_data& evt,
                                                       int i, int i1, int i2,
                                                       int i3, Ptcl_num p3)
{
  constexpr auto _NO_PTCL = NO_PTCL;
  const int block {i / HELICITY_BLOCK_SIZE};
  const Helicity h1 {evt.internal_helicity(block, i1)};
  const Helicity h2 {evt.internal_helicity(block, i2)};
  if (h1 == h2) {
    evt.internal_ptcl(block, i3, _NO_PTCL);
    return;
  }
  evaluate_massless_ffv_vertex_kernel(evt, i, i1, i2, i3, p3);
}

template<typename event_data>
KOKKOS_INLINE_FUNCTION void
evaluate_ffv_vertex(const event_data& evt, int i, int i1, int i2, int i3,
                    Ptcl_num p, Ptcl_num active_neutral_gauge_boson,
                    const detail::BG_recursion_data& d,
                    const sm_properties& smprops)
{
  Ptcl_num p3 {is_quark(p) ? GLUON : active_neutral_gauge_boson};
  constexpr auto _PHOTON_AND_Z = PHOTON_AND_Z;
  if (p3 == _PHOTON_AND_Z) {
    // We use the standard i3 current to evaluate the Z vertex; the Z propagator
    // will be added as usual after calculating all vertices.
    const int block {i / HELICITY_BLOCK_SIZE};
    const Helicity h1 {evt.internal_helicity(block, i1)};
    const Helicity h2 {evt.internal_helicity(block, i2)};
    if (h1 != h2) {
      evaluate_ffz_vertex(evt, i, i1, i2, i3, d);
      evt.internal_ptcl(block, i3, _PHOTON_AND_Z);

      // We use the auxiliary i3prime current to evaluate the photon vertex; as
      // this auxiliary storage is outside the normal recursion, we have to add
      // the propagator manually, we do it here immediately after the vertex
      // evaluation; the auxiliary i3prime current will be added to the Z current
      // (stored in i3) when the recursion reaches the corresponding fvf vertex.
      const int i3prime {evt.auxiliary_photon_current_idx()};
      evaluate_massless_ffv_vertex(evt, i, i1, i2, i3prime, PHOTON);
      evaluate_massless_boson_propagator_kernel(evt, i, i3, i3prime);
    } else {
      constexpr auto _NO_PTCL = NO_PTCL;
      evt.internal_ptcl(block, i3, _NO_PTCL);
    }
  }
  else if (p3 == Z) {
    evaluate_ffz_vertex(evt, i, i1, i2, i3, d);

    // We break the logic here and set the particle type here instead of in the
    // lowest level vertex. The reason is a race condition when the lowest level
    // vertex sets the particle type to Z and the code above sets the same
    // particle to PHOTON_AND_Z. For more details cf:
    //   https://gitlab.com/spice-mc/pepper/-/issues/24
    // or the corresponding MR.
    constexpr auto _Z = Z;
    const int block {i / HELICITY_BLOCK_SIZE};
    evt.internal_ptcl(block, i3, _Z);

  }
  else {
    const double m {smprops.mass(p)};
    if (m == 0.0) {
      evaluate_massless_ffv_vertex(evt, i, i1, i2, i3, p3);
    }
    else {
      evaluate_massive_ffv_vertex_kernel(evt, i, i1, i2, i3, p3);
    }
  }
}

template<typename event_data>
KOKKOS_INLINE_FUNCTION void evaluate_3_vertex(const event_data& evt, int i, int i1, int i2,
                              int i3, const Ptcl_num active_neutral_gauge_boson,
                              const detail::BG_recursion_data& d,
                              const sm_properties& smprops)
{
  const int block {i / HELICITY_BLOCK_SIZE};
  Ptcl_num p1 {evt.internal_ptcl(block, i1)};
  Ptcl_num p2 {evt.internal_ptcl(block, i2)};

  if (p1 == GLUON && p2 == GLUON) {
    evaluate_ggg_vertex_kernel(evt, i, i1, i2, i3);
  }
  else if ((p1 == GLUON && is_quark(p2)) || (p2 == GLUON && is_quark(p1)) ||
           (is_neutral_gauge_boson(p1) && is_fermion(p2)) ||
           (is_neutral_gauge_boson(p2) && is_fermion(p1))) {
    evaluate_fvf_vertex(evt, i, i1, i2, i3, p1, p2, d, smprops);
  }
  else if (p1 > 0 && p1 == -p2) {
    evaluate_ffv_vertex(evt, i, i1, i2, i3, p1, active_neutral_gauge_boson, d, smprops);
  }
}

template<typename event_data>
KOKKOS_INLINE_FUNCTION void evaluate_4_vertex(const event_data& evt, int i,
                                            int i1, int i2, int i3, int i4)
{
  const int block {i / HELICITY_BLOCK_SIZE};
  const Ptcl_num p1 {evt.internal_ptcl(block, i1)};
  const Ptcl_num p2 {evt.internal_ptcl(block, i2)};
  const Ptcl_num p3 {evt.internal_ptcl(block, i3)};

  if (p1 == GLUON && p2 == GLUON && p3 == GLUON) {
    // Gluon-gluon-gluon vertex
    evaluate_gggg_vertex_kernel(evt, i, i1, i2, i3, i4);
  }
}

template<typename event_data>
KOKKOS_INLINE_FUNCTION void evaluate_propagator(const event_data& evt, int i,
                                              int i1,
                                              const sm_properties& smprops)
{
  const int block {i / HELICITY_BLOCK_SIZE};
  Ptcl_num p {evt.internal_ptcl(block, i1)};
  if (p == PHOTON_AND_Z)
    // For the photon/Z case, the photon ffv * propagator term is already
    // evaluated together in evaluate_ffv_vertex and written to an auxiliary
    // current. Hence, we only need to care about the Z part here.
    p = Z;
  const double m {smprops.mass(p)};
  if (m == 0.0) {
    if (is_boson(p)) {
      evaluate_massless_boson_propagator_kernel(evt, i, i1);
    }
    else {
      evaluate_massless_quark_propagator_kernel(evt, i, i1, p);
    }
  }
  else {
    const C complex_mass2 {m * m, -m * smprops.width(p)};
    if (is_boson(p)) {
      evaluate_massive_boson_propagator_kernel(evt, i, i1, complex_mass2);
    }
    else {
      evaluate_massive_quark_propagator_kernel(evt, i, i1, p, complex_mass2);
    }
  }
}
template<typename event_data,typename data_type>
KOKKOS_INLINE_FUNCTION void perform_kernel(const event_data& evt, const int& i,
                           const data_type& in_d, const int& min_row,
                           const int& min_column, const Ptcl_num& active_neutral_gauge_boson,
                           const sm_properties& smprops)
{
  const auto& d = in_d();
  if (evt.w(i) == 0.0)
    return;

  // Iterate diagonals of the current matrix.
  for (int m {1}; m < d.n; m++) {

    // Iterate entries on the mth diagonal.
    for (int k {0}; k < d.n - m - 1; k++) {

      // Ignore currents that should be left intact.
      if (is_readonly(m, k, d.n, min_row, min_column))
        continue;

      const int current {index(k, k + m, d.n)};

      // Iterate possible three vertices.
      for (int i1 {k}; i1 < k + m; i1++) {
        evaluate_3_vertex(evt, i, index(k, i1, d.n), index(i1 + 1, k + m, d.n),
                          current, active_neutral_gauge_boson, d, smprops);
        
        // Iterate possible four vertices.
        for (int i2 {i1 + 1}; i2 < k + m; i2++) {
        evaluate_4_vertex(evt, i, index(k, i1, d.n), index(i1 + 1, i2, d.n),
                            index(i2 + 1, k + m, d.n), current);
        }
      }

      const int block {i / HELICITY_BLOCK_SIZE};
      if (evt.internal_ptcl(block, current) == NO_PTCL)
        continue;
      if (m < d.n - 2)
        evaluate_propagator(evt, i, current, smprops);
    }
  }
}
DEFINE_CUDA_KERNEL_NARGS_5(perform_kernel, const data_type&, in_d, int,
                           min_row, int, min_column, Ptcl_num, active_neutral_gauge_boson,
                           const sm_properties&, smprops)

#endif
