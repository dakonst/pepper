// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_PDF_AND_ALPHA_S_INTERFACE_H
#define PEPPER_PDF_AND_ALPHA_S_INTERFACE_H

#include "event_handle.h"
#include "print.h"
#include "settings.h"
#include "timing.h"

// LHAPDF
#if LHAPDF_FOUND
#include "compiler_warning_macros.h"
PEPPER_SUPPRESS_COMMON_WARNINGS_PUSH
#include "LHAPDF/LHAPDF.h"
#include "LHAPDF/GridPDF.h"
#if KOKKKOS_LHAPDF_FOUND
#include <thrust/device_vector.h>
#include <thrust/zip_function.h>
#endif
PEPPER_SUPPRESS_COMMON_WARNINGS_POP
#endif

class Process;
class KKPDF;

void validate_and_sync_particle_properties_with_pdf_info(const Settings&);

class Pdf_and_alpha_s {
public:
  Pdf_and_alpha_s(const Settings&, const SM_parameters&);
  ~Pdf_and_alpha_s();
  bool pdf_enabled() const { return _pdf_enabled; }
  void set_fixed_mu2(double mu2);
  void update_couplings(Event_handle&, Process&) const;
  void update_pdf(Event_handle&, Process&) const;
  void update_weights(Event_handle&, Process&) const;

  int lhapdf_id() const
  {
#if LHAPDF_FOUND
    if (!dummy_pdf_enabled)
      return gridpdf->lhapdfID();
#endif
    return -1;
  }
  void reset_timing() const {
    fill_pdf_duration = 0.0;
    update_couplings_duration = 0.0;
  }

  // Declare whether the PDF/AlphaS weight updates will happen on the device.
  // We expose this because Generator::evaluate_points is responsible for making
  // sure that the weights are on the host or on the device, as needed.
  bool updates_weights_on_device() const;

private:

  void enable_dummy_pdf();
  void set_fixed_alpha_s(Event_handle& , Process& ) const;
  void update_pdf_weights(Event_handle&, Process&) const;
  void update_xfx(Ptcl_num, double, double, std::vector<double>&) const;

  /// (always) constant coupling factors
  double g_em;
  /// if fixed_mu2 is zero, event-specific mu2 will be read from event data
  double fixed_mu2 {-1.0};
  /// if fixed_alpha_s is zero, it is evaluated dynamically using LHAPDF
  double fixed_alpha_s {-1.0};
  /// are PDFs enabled? (as determined from user settings and LHAPDF_FOUND)
  bool _pdf_enabled;
  /// when using a dummy PDF, we will use x_i f(x_i, mu2) = x_i
  bool dummy_pdf_enabled {false};

#if KOKKOS_LHAPDF_FOUND
  // TODO: KKPDF should be in LHAPDF namespace
  KKPDF *kk_pdf;
  std::unique_ptr<LHAPDF::GridPDF> gridpdf;
#elif LHAPDF_FOUND
  std::unique_ptr<LHAPDF::GridPDF> gridpdf;
#endif

#if KOKKOS_LHAPDF_FOUND
  mutable Kokkos::View<double*>  xfx_products, abs_xfx_products;
#else
  mutable std::vector<double> xfx1, xfx2, xfx_products;
#endif

  mutable double fill_pdf_duration {0};
  mutable double update_couplings_duration {0.0};
};

#endif
