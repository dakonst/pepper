// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_HELICITY_H
#define PEPPER_HELICITY_H

#include "Kokkos_Core.hpp"

#include <cassert>
#include <iostream>

// This uses the COLOR_SUMMED_MELIA convention; for the Sherpa convention,
// simply remove the "= 1" and the "= 0". The special value "unset" can be
// used in cases where the caller of a method does not prescribe a certain
// helicity (perhaps because it is chosen randomly or fixed anyway).
enum class Helicity { unset = -1, plus = 1, minus = 0};
std::ostream& operator<<(std::ostream& os, Helicity);

KOKKOS_INLINE_FUNCTION Helicity flip_helicity(Helicity h)
{
  assert(h == Helicity::plus || h == Helicity::minus);
  return static_cast<Helicity>(!static_cast<int>(h));
}

KOKKOS_INLINE_FUNCTION Helicity calculate_helicity(int conf_idx, int ptcl_idx)
{
  auto h = static_cast<Helicity>((conf_idx & (1 << ptcl_idx)) >> ptcl_idx);
  if (ptcl_idx < 2)
    h = flip_helicity(h);
  return h;
}

#endif
