// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "melia_dyck_permutator.h"
#include "sm.h"

#include <algorithm>
#include <numeric>

Melia_dyck_permutator::Melia_dyck_permutator(int n, int k,
                                 const std::vector<int>& _dyck_array,
                                 const std::vector<Ptcl_num>& _flavour_dyck_array)
    : dyck_array {_dyck_array}, n_inner_quark_pairs {std::max(0, k - 1)},
      n_gluons {n - 2 * std::max(1, k)}, gluon_only {k == 0}
{
  // collect indizes for quark pairs and gluons
  dyck_array.erase(dyck_array.begin());
  if (gluon_only) {
    dyck_array.erase(dyck_array.end() - 1);
  }
  else {
    dyck_array.erase(dyck_array.begin());
  }
  gluon_indizes.reserve(n_gluons);
  quark_indizes.reserve(n_inner_quark_pairs);
  antiquark_indizes.reserve(n_inner_quark_pairs);
  for (unsigned int i {0}; i < dyck_array.size(); ++i) {
    if (dyck_array[i] == GLUON)
      gluon_indizes.push_back(i);
    else if (dyck_array[i] > 0)
      quark_indizes.push_back(i);
    else
      antiquark_indizes.push_back(i);
  }

  // prepare permutations for quark pairs and gluons
  quark_pair_ordering.resize(n_inner_quark_pairs);
  std::iota(quark_pair_ordering.begin(), quark_pair_ordering.end(), 0);
  gluon_ordering.resize(n_gluons);
  std::iota(gluon_ordering.begin(), gluon_ordering.end(), 0);

  // collect indizes for identical flavours
  for (unsigned int i {0}; i < _flavour_dyck_array.size(); ++i) {
    if (_flavour_dyck_array[i] < 0)
      identical_antiquark_indizes[_flavour_dyck_array[i]].push_back(i);
  }

  for (unsigned int i {0}; i < dyck_array.size(); ++i) {
    if (dyck_array[i] == GLUON)
      continue;
    else if (dyck_array[i] > 0)
      fermion_lines[std::abs(dyck_array[i])].push_back(i);
    else
      fermion_lines[std::abs(dyck_array[i])].push_back(i);
  }

  for (auto keyval : identical_antiquark_indizes) {
    identical_antiquark_orderings[keyval.first].resize(keyval.second.size());
    std::iota(identical_antiquark_orderings[keyval.first].begin(),
              identical_antiquark_orderings[keyval.first].end(), 0);
  }
}

bool Melia_dyck_permutator::advance()
{
  bool res {false};
  res = std::next_permutation(gluon_ordering.begin(), gluon_ordering.end());
  if (res)
    return res;
  res = std::next_permutation(quark_pair_ordering.begin(),
                              quark_pair_ordering.end());
  if (res)
    return res;
  for (auto& keyval : identical_antiquark_orderings) {
    res = std::next_permutation(keyval.second.begin(), keyval.second.end());
    if (res)
      return res;
  }
  return res;
}

std::vector<int> Melia_dyck_permutator::current()
{
  // first build a permutation from gluon and quark pair orderings
  std::vector<int> perm;
  perm.reserve(dyck_array.size() + 2);
  perm.push_back(0);
  int offset;
  if (gluon_only) {
    offset = 1;
  }
  else {
    perm.push_back(1);
    offset = 2;
  }
  int gluon_counter {0};
  for (unsigned int i {0}; i < dyck_array.size(); ++i) {
    if (dyck_array[i] == GLUON) {
      perm.push_back(gluon_indizes[gluon_ordering[gluon_counter]] + offset);
      gluon_counter++;
    }
    else if (dyck_array[i] > 0) {
      perm.push_back(
          fermion_lines
              .find(quark_pair_ordering[std::abs(dyck_array[i]) - 2] + 2)
              ->second[0] +
          offset);
    }
    else {
      perm.push_back(
          fermion_lines
              .find(quark_pair_ordering[std::abs(dyck_array[i]) - 2] + 2)
              ->second[1] +
          offset);
    }
  }
  if (gluon_only)
    perm.push_back(perm.size());

  // use a second perm to apply identically flavoured antiquark orderings
  auto perm2 {perm};
  for (auto& keyval : identical_antiquark_orderings) {
    for (unsigned int i {0}; i < keyval.second.size(); ++i) {
      perm2[identical_antiquark_indizes[keyval.first][i]] =
          perm[identical_antiquark_indizes[keyval.first][keyval.second[i]]];
    }
  }
  return perm2;
}
