// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "pepper/config.h"

#include "citations.h"
#include "generator.h"
#include "kokkos.h"
#include "mpi.h"
#include "paths.h"
#include "print.h"
#include "run_statistics.h"
#include "settings.h"
#include "sm.h"
#include "timing.h"
#include <cstdlib>
#include <fstream>

using namespace Timing;

void run_generator(Settings& s)
{
  STATUS("Initialise processes and event generation ...");
  INFO("Process: ", s.process_specs);
  Timer timer;

  validate_and_sync_particle_properties_with_pdf_info(s);

  SM_parameters sm;
  Generator gen {s, sm};

  timer.register_elapsed({Task::Initialisation});
  VERBOSE("Done");

  timer.reset();
  gen.optimise();
  timer.register_elapsed({Task::Optimisation});
  timer.reset();
  if (!s.unweighting_disabled)
    gen.setup_and_optimise_event_unweighting();
  timer.register_elapsed({Task::Unweighting_setup});

  timer.reset();
  gen.generate_batches(s.n_batches);
#if PEPPER_TIMING_ENABLED
  if (s.batch_size * s.n_batches > 0) {
    const double event_gen_time {timer.elapsed()};
    register_duration({Task::Event_generation}, event_gen_time);
  }
#endif
}

int main(int argc, char* argv[])
{
  Mpi::initialise_and_register_finalise();
  Pepper_kokkos::initialise_and_register_finalise(argc, argv);
  Timing::initialise();
  Timer timer;

  Settings s;
  s.read(argc, argv);
  Msg::configure(s);

  if (Mpi::is_main_rank())
    Msg::print_banner();

  STATUS("Determine paths ...");
  if (!s.ini_path.empty())
    INFO("Runcard: ", s.ini_path);
  INFO("Data: ", s.data_path);
  VERBOSE("");

  STATUS("Prepare parallel execution ...");
  Kokkos::DefaultExecutionSpace{}.print_configuration(std::cerr);
  const int n_ranks {Mpi::n_ranks()};
  if (n_ranks > 1)
    INFO(n_ranks, " MPI ranks join the swarm");
  if (Kokkos::device_id() != -1)
    INFO("Kokkos device #", Kokkos::device_id(), " joins the swarm");
  VERBOSE("Done");
  VERBOSE("");

  run_generator(s);

  Timing::finalise(timer.elapsed());

  if (Mpi::is_main_rank()) {
    Citations::register_citation("Pepper v" PROJECT_VERSION,
                                 PEPPER_INSPIRE_TEXKEY);
    std::ofstream citations_file {"pepper_citations.tex"};
    Citations::print_citations(citations_file);
    if (Msg::verbosity >= Msg::Verbosity::info)
      Run_statistics::print_occurrences(std::cerr);
    if (s.diagnostic_output_enabled) {
      s.write_resolved_settings();
      std::ofstream file {Paths::diagnostics + "/timers.csv"};
      Timing::print_durations(file);
    }
  }

  return EXIT_SUCCESS;
}
