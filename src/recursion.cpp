// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "recursion.h"
#include "cvec4.h"
#include "event_handle.h"
#include "helicity.h"
#include "helicity_configurations.h"
#include "permutations.h"
#include "print.h"
#include "recursion_kernel.h"
#include "timing.h"
#include "vec4.h"
#include "Kokkos_Core.hpp"

BG_recursion::BG_recursion(int n_ptcl, const SM_parameters& sm)
    : d_data(Kokkos::ViewAllocateWithoutInitializing("d_gb_data")),
      h_data(Kokkos::create_mirror_view(d_data)),
      active_neutral_gauge_boson {sm.ew.z_enabled ? PHOTON_AND_Z : PHOTON}
{
  detail::BG_recursion_data tmp {n_ptcl, sm.ew.g_right() / sm.ew.g(), sm.ew.g_left_1() / sm.ew.g(),
        sm.ew.g_left_2() / sm.ew.g()};
  h_data() = tmp;
  Kokkos::deep_copy(d_data,h_data);
}

BG_recursion::~BG_recursion()
{
  using namespace Timing;
  register_duration(
      {Task::Event_generation, Task::Recursion, Task::Momenta_preparation},
      prepare_momenta_duration);
  register_duration(
      {Task::Event_generation, Task::Recursion, Task::Currents_preparation},
      prepare_currents_duration);
  register_duration(
      {Task::Event_generation, Task::Recursion, Task::Internal_currents_reset},
      reset_internal_currents_duration);
  register_duration({Task::Event_generation, Task::Recursion,
                     Task::Internal_particle_information_reset},
                    reset_internal_particle_information_duration);
  register_duration(
      {Task::Event_generation, Task::Recursion, Task::Calculate_currents},
      perform_duration);
  register_duration({Task::Event_generation, Task::Recursion, Task::ME_reset},
                    reset_me_duration);
  register_duration({Task::Event_generation, Task::Recursion, Task::ME_update},
                    update_me_duration);
}

void BG_recursion::prepare(Event_handle& evt,
                           const Helicity_configurations& hel_conf,
                           const Permutations& perm, const int& perm_idx) const
{
  Kokkos::Profiling::pushRegion("BG_recursion::prepare");
  Timer timer;
  const bool should_resolve_helicity {hel_conf.settings().treatment ==
                                      Helicity_treatment::summed};
  
  const auto& _perm = perm.h_or_d();
  const auto& perm_size = perm.perm_size();
  const auto& dd = d_data;
  const auto& smprops = sm_properties::getInstance();
  RUN_KERNEL(prepare_momenta_kernel, evt, perm_idx, _perm, perm_size, dd);
  Kokkos::fence("BG_recursion::prepare:prepare_momenta_kernel::fence");
  prepare_momenta_duration += timer.elapsed();

  timer.reset();
  RUN_KERNEL(prepare_currents_kernel, evt, perm_idx,
             should_resolve_helicity, _perm, perm_size, dd, smprops);
  Kokkos::fence("BG_recursion::prepare:prepare_currents_kernel::fence");
  prepare_currents_duration += timer.elapsed();
  Kokkos::Profiling::popRegion();
}

void BG_recursion::reset_internal_currents(Event_handle& evt)
{
  Kokkos::Profiling::pushRegion("BG_recursion::reset_internal_currents");
  Timer currents_timer;
  const auto& dd = d_data;
  RUN_KERNEL(reset_internal_currents_kernel, evt, dd);
  reset_internal_currents_duration += currents_timer.elapsed();

  Timer ptcl_timer;
  RUN_HELICITY_BLOCK_KERNEL(reset_internal_particles_kernel, evt, dd);
  reset_internal_particle_information_duration += ptcl_timer.elapsed();

  Kokkos::fence("BG_recursion::reset_internal_currents");

  // After resetting everything to zero, there will be nothing left to re-use.
  min_row = 0;
  min_column = 0;
  Kokkos::Profiling::popRegion();
}

void BG_recursion::perform(Event_handle& evt) const
{
  Timer timer;
  const auto& dd = d_data;
  const auto& smprops = sm_properties::getInstance();
  const auto& _min_row = min_row;
  const auto& _min_column = min_column;
  const auto& _active_neutral_gauge_boson = active_neutral_gauge_boson;
  RUN_KERNEL(perform_kernel, evt, dd, _min_row, _min_column,
             _active_neutral_gauge_boson,smprops);
  Kokkos::fence(__func__);

  perform_duration += timer.elapsed();
}

void BG_recursion::reset_matrix_elements(
    Event_handle& evt, const Helicity_configurations& hel_conf,
    int qcd_perm_idx) const
{
  Kokkos::Profiling::pushRegion(__func__);
  Timer timer;

  if (hel_conf.settings().cached_summing()) {
    evt.reset_alt_me(qcd_perm_idx);
  }
  evt.reset_me(qcd_perm_idx);

  reset_me_duration += timer.elapsed();
  Kokkos::Profiling::popRegion();
}

void BG_recursion::update_matrix_elements(
    Event_handle& evt, const Helicity_configurations& hel_conf,
    const Permutations& perm, int qcd_perm_idx) const
{
  Kokkos::Profiling::pushRegion("BG_recursion::update_matrix_elements");
  Timer timer;

  const int final_idx {perm.get_final()};
  const Ptcl_num pb {evt.host.ptcl(final_idx)};
  const double perm_sign {perm.sign(qcd_perm_idx)};

  if (hel_conf.settings().cached_summing()) {
    const auto& dd = d_data;
    const auto& smprops = sm_properties::getInstance();
    RUN_KERNEL(update_matrix_element_with_cached_summing_kernel, evt,
               qcd_perm_idx, perm_sign, final_idx, pb, dd, smprops);
  }
  else {
    const bool should_resolve_helicity {hel_conf.settings().treatment ==
                                        Helicity_treatment::summed};
    const auto& dd = d_data;
    const auto& smprops = sm_properties::getInstance();
    RUN_KERNEL(update_matrix_element_kernel, evt, qcd_perm_idx, perm_sign,
               final_idx, pb, should_resolve_helicity, dd, smprops);
  }

  Kokkos::fence("BG_recursion::update_matrix_elements::fence");
  update_me_duration += timer.elapsed();
  Kokkos::Profiling::popRegion();
}
