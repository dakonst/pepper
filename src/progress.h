// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_PROGRESS_H
#define PEPPER_PROGRESS_H

#include "math.h"
#include "print.h"

#include <chrono>
#include <limits>

using namespace std::chrono_literals;

namespace Progress {

using Time_unit = std::chrono::milliseconds;
constexpr inline auto Unknown_duration = Time_unit {-1};

struct Estimator {
private:
  using Clock = std::chrono::steady_clock;

public:
  Estimator(int _n_steps_total, double _growth_factor)
      : n_steps_total {_n_steps_total},
        growth_factor {_growth_factor}
  {
  }

  std::tuple<Time_unit, Time_unit> elapsed_and_left(int n_steps_done) const;

  Time_unit elapsed() const
  {
    return std::chrono::duration_cast<Time_unit>(Clock::now() - m_beg);
  }

  std::chrono::time_point<Clock> m_beg {Clock::now()};
  int n_steps_total {0};
  double growth_factor {1.0};
};

template <bool should_update_existing_output, bool increases_print_interval,
          bool prints_step>
class Observer {
public:
  Observer(int n_steps_total, double growth_factor)
      : estimator {n_steps_total, growth_factor}
  {
  }

  void print_info(MC_result& result, int n_steps_done)
  {
    if ((n_steps_done % print_interval) != 0 &&
        n_steps_done != estimator.n_steps_total)
      return;
    // calculate remaining duration
    const auto [elapsed, left] = estimator.elapsed_and_left(n_steps_done);
    // remove last output (if we did not print something different,
    // e.g. a warning, since that last output)
    bool overwrite_last_print {should_update_existing_output &&
                               last_print_id == Msg::print_counter};
    if (overwrite_last_print &&
        Msg::colourfulness != Msg::Colourfulness::never) {
      print("\033[1A");
      print("\033[2K");
      print("\033[1A");
      print("\033[2K");
      print("\033[1A");
      print("\033[2K");
      print("\r");
    }
    // display current MC result
    if (prints_step) {
      INFO("Step ", n_steps_done, "/", estimator.n_steps_total, ": ", result);
    }
    else {
      INFO(result);
    }
    // display elapsed and estimated remaining time
    const auto last_precision {std::cerr.precision(1)};
    if (left == Unknown_duration)
      INFO(elapsed, " elapsed, unknown left");
    else if (left == Time_unit {0})
      INFO(elapsed, " elapsed");
    else
      INFO(elapsed, " elapsed, about ", left, " left");
    std::cerr.precision(last_precision);
    last_print_id = Msg::print_counter;
    // increase print interval
    if (increases_print_interval && n_steps_done == 10 * print_interval)
      print_interval *= 10;
  }

  Time_unit elapsed() const { return estimator.elapsed(); }

  double elapsed_s() const
  {
    return std::chrono::duration_cast<
               std::chrono::duration<double, std::ratio<1, 1>>>(elapsed())
        .count();
  }

private:
  Estimator estimator;
  int print_interval {1};
  unsigned long long last_print_id {
      std::numeric_limits<unsigned long long>::max()};
};

using Event_generation_observer = Observer<true, true, false>;
using Optimisation_observer = Observer<false, false, true>;

} // namespace Progress

#endif
