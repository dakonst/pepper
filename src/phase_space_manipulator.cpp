// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "phase_space_manipulator.h"
#include "event_handle.h"
#include "math.h"
#include "phase_space_manipulator_kernel.h"
#include "sm.h"

/// Phase space biasing factory function {{{

std::unique_ptr<Phase_space_manipulator>
create_phase_space_manipulator(const std::string& spec)
{
  if (spec == "Zpt") {
    return std::make_unique<Zpt_phase_space_biaser>();
  }
  // corresponds to Sherpa notation:
  // VAR { max ( pow ( sqrt ( H_T2 ) - PPerp ( p [2]) - PPerp ( p [3]) ,2) ,
  //       PPerp2 ( p [2]+ p [3]) ) /400.0}
  if (spec == "Max(Htp2,Zpt2)") {
    return std::make_unique<Max_htp_zpt_phase_space_biaser>();
  }
  // corresponds to Sherpa notation:
  // VAR { pow ( max ( sqrt ( H_T2 ) - PPerp ( p [2]) - PPerp ( p [3]) ,
  //       ( PPerp( p [2]) + PPerp ( p[3]) ) /2) /30.0 ,2) }
  if (spec == "Max(Htp2,ttpt2)") {
    return std::make_unique<Max_htp_ttpt_phase_space_biaser>();
  }

  throw std::invalid_argument {"Unknown phase space biaser: " + spec};
}

void Zpt_phase_space_biaser::fill_biasing_factor(Event_handle& evt) const
{
  RUN_KERNEL(zpt_phase_space_bias, evt);
}

void Max_htp_zpt_phase_space_biaser::fill_biasing_factor(Event_handle& evt) const
{
  RUN_KERNEL(max_htp_zpt_phase_space_bias, evt);
}

void Max_htp_ttpt_phase_space_biaser::fill_biasing_factor(Event_handle& evt) const
{
  RUN_KERNEL(max_htp_ttpt_phase_space_bias, evt);
}
