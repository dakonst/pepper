// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2024 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_KOKKOS_H
#define PEPPER_KOKKOS_H

#include "Kokkos_Core.hpp"

namespace Pepper_kokkos {

void initialise_and_register_finalise(int& argc, char* argv[]);

} // namespace Pepper_kokkos

#endif
