// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef PEPPER_SM_H
#define PEPPER_SM_H

#include <cmath>
#include <string>
#include <vector>

#include "pepper/config.h"

#include "Kokkos_Core.hpp"
#include "math.h"
#include "print.h"

constexpr inline int QCD_NC {3};

inline double alpha_to_g(double alpha) { return std::sqrt(4.0 * M_PI * alpha); }

class EW_parameters {
public:
  EW_parameters() { update_g(); }
  double alpha() const noexcept { return _alpha; }
  double g() const noexcept { return _g; }
  const C& g_right() const noexcept { return _g_right; }
  const C& g_left_1() const noexcept { return _g_left_1; }
  const C& g_left_2() const noexcept { return _g_left_2; }
  void set_alpha(double a) noexcept
  {
    _alpha = a;
    update_g();
  }
  bool z_enabled {true};

private:
  void update_g() noexcept
  {
    _g = alpha_to_g(_alpha);
    _g_right = 1.0 / std::sqrt(cos2_theta_w) * std::sqrt(sin2_theta_w) * _g;
    _g_left_1 = -1.0 / std::sqrt(cos2_theta_w) * std::sqrt(sin2_theta_w) * _g;
    _g_left_2 = 1.0 / (std::sqrt(cos2_theta_w) * std::sqrt(sin2_theta_w)) * _g;
  }
  double _alpha {1.0 / 128.802223295};
  double sin2_theta_w {0.23155};
  double cos2_theta_w {1.0 - 0.23155};
  double _g {0.0};
  C _g_right;
  C _g_left_1;
  C _g_left_2;
};

struct SM_parameters {
  EW_parameters ew;
};

// For now, we will use the Sherpa 2.x defaults, for easy comparisons.
// Later, we might want to pull the latest PDG numbers.
/*
 List of Particle Data 
  IDName     kfc  MASS[<kfc>]   WIDTH[<kfc>]  STABLE[<kfc>] MASSIVE[<kfc>]  ACTIVE[<kfc>]  YUKAWA[<kfc>]
       d       1         0.01              0              1              0              1              0
       u       2        0.005              0              1              0              1              0
       s       3          0.2              0              1              0              1              0
       c       4         1.42              0              1              0              1              0
       b       5          4.8              0              1              0              1              0
       t       6       173.21              2              0              1              1         173.21
      e-      11     0.000511              0              1              0              1              0
      ve      12            0              0              1              0              1              0
     mu-      13        0.105              0              1              0              1              0
     vmu      14            0              0              1              0              1              0
    tau-      15        1.777    2.26735e-12              0              0              1              0
    vtau      16            0              0              1              0              1              0
       G      21            0              0              1              0              1              0
       P      22            0              0              1              0              1              0
       Z      23      91.1876         2.4952              0              1              1        91.1876
      W+      24       80.385          2.085              0              1              1         80.385
      h0      25          125        0.00407              0              1              1            125
 */

// Particle numbers are assigned according to the Monte-Carlo numbering scheme,
// doi:10.1007/BF02683426
using Ptcl_num = int;

// Translate between particle numbers and string representations
Ptcl_num particle_from_string(const std::string&);
std::string string_from_particle(Ptcl_num);
std::string particles_to_process_spec_n(const std::vector<Ptcl_num>&, int n);

// Technical (pseudo-)particle codes
// Only non-zero numbers are used by the Monte-Carlo numbering scheme,
// so we can use zero to denote "no particle".
constexpr inline Ptcl_num NO_PTCL           {0};
// The numbers 81-100 are reserved for MC-internal use, so we can use them for
// Pepper-internal technical purposes.
// Pseudoparticle used for the computation of combined Z/photon currents:
constexpr inline Ptcl_num PHOTON_AND_Z      {81};

// Quarks
constexpr inline Ptcl_num DOWN_QUARK        {1};
constexpr inline Ptcl_num UP_QUARK          {2};
constexpr inline Ptcl_num STRANGE_QUARK     {3};
constexpr inline Ptcl_num CHARM_QUARK       {4};
constexpr inline Ptcl_num BOTTOM_QUARK      {5};
constexpr inline Ptcl_num TOP_QUARK         {6};

// Leptons
constexpr inline Ptcl_num ELECTRON          {11};
constexpr inline Ptcl_num ELECTRON_NEUTRINO {12};
constexpr inline Ptcl_num MUON              {13};
constexpr inline Ptcl_num MUON_NEUTRINO     {14};
constexpr inline Ptcl_num TAUON             {15};
constexpr inline Ptcl_num TAU_NEUTRINO      {16};

// Gauge and Higgs bosons
constexpr inline Ptcl_num GLUON             {21};
constexpr inline Ptcl_num PHOTON            {22};
constexpr inline Ptcl_num Z                 {23};

// NOTE: remember to update LAST_PTCL below after adding a new particle above

// Mark largest particle identifier (not counting pseudoparticles), such that it
// can be used to size arrays/vectors.
constexpr inline Ptcl_num LAST_PTCL {Z};


// Query vertices
std::vector<Ptcl_num> possible_mothers(Ptcl_num j, Ptcl_num k) noexcept;
struct Particle_combiner {
  std::vector<Ptcl_num> operator()(Ptcl_num j, Ptcl_num k) const
  {
    return possible_mothers(j, k);
  }
};


struct sm_properties {

  Kokkos::View<double*,Kokkos::DefaultExecutionSpace::array_layout,Kokkos::DefaultExecutionSpace::memory_space> MASSES;
  Kokkos::View<double*,Kokkos::DefaultExecutionSpace::array_layout,Kokkos::DefaultExecutionSpace::memory_space> WIDTHS;
  Kokkos::View<double*,Kokkos::DefaultExecutionSpace::array_layout,Kokkos::DefaultExecutionSpace::memory_space> CHARGES;
  Kokkos::View<double*,Kokkos::DefaultExecutionSpace::array_layout,Kokkos::HostSpace> H_MASSES;
  Kokkos::View<double*,Kokkos::DefaultExecutionSpace::array_layout,Kokkos::HostSpace> H_WIDTHS;
  Kokkos::View<double*,Kokkos::DefaultExecutionSpace::array_layout,Kokkos::HostSpace> H_CHARGES;
  static bool instanceFlag;
  static sm_properties *single;
  static sm_properties& getInstance()
  {
      if(! instanceFlag)
      {
        single = new sm_properties();
        instanceFlag = true;
        return *single;
      }
      else
      {
        return *single;
      }
  }
  // ~sm_properties(){instanceFlag=false;};
  sm_properties():
    MASSES(make_masses()),
    WIDTHS(make_widths()),
    CHARGES(make_charges()),
    H_MASSES(Kokkos::create_mirror_view_and_copy(Kokkos::HostSpace(),MASSES)),
    H_WIDTHS(Kokkos::create_mirror_view_and_copy(Kokkos::HostSpace(),WIDTHS)),
    H_CHARGES(Kokkos::create_mirror_view_and_copy(Kokkos::HostSpace(),CHARGES)) {};

  Kokkos::View<double*> make_masses()
  {
    Kokkos::View<double*> masses(Kokkos::ViewAllocateWithoutInitializing("masses"),LAST_PTCL + 1);
    Kokkos::View<double*,Kokkos::HostSpace> h_masses("h_masses",LAST_PTCL + 1);
    // std::vector<double> masses(LAST_PTCL + 1, 0.0);
    h_masses[TOP_QUARK] = 173.21;
    h_masses[Z] = 91.1876;
    Kokkos::deep_copy(masses,h_masses);
    return masses;
  }

  Kokkos::View<double*> make_widths()
  {
    Kokkos::View<double*> widths(Kokkos::ViewAllocateWithoutInitializing("widths"),LAST_PTCL + 1);
    Kokkos::View<double*,Kokkos::HostSpace> h_widths("h_widths",LAST_PTCL + 1);
    // NOTE: This choice for the top quark has been made for testing purposes,
    // where top quarks appear as external states. Reconsider if this makes sense
    // as a default.
    h_widths[TOP_QUARK] = 0.0;
    h_widths[Z] = 2.4952;
    Kokkos::deep_copy(widths,h_widths);
    return widths;
  }

  Kokkos::View<double*> make_charges()
  {
    Kokkos::View<double*> charges(Kokkos::ViewAllocateWithoutInitializing("charges"),LAST_PTCL + 1);
    Kokkos::View<double*,Kokkos::HostSpace> h_charges("h_charges",LAST_PTCL + 1);
    h_charges[DOWN_QUARK]    = -1.0 / 3.0;
    h_charges[UP_QUARK]      =  2.0 / 3.0;
    h_charges[STRANGE_QUARK] = -1.0 / 3.0;
    h_charges[CHARM_QUARK]   =  2.0 / 3.0;
    h_charges[BOTTOM_QUARK]  = -1.0 / 3.0;
    h_charges[TOP_QUARK]     =  2.0 / 3.0;
    h_charges[ELECTRON]      = -1.0;
    h_charges[MUON]          = -1.0;
    h_charges[TAUON]         = -1.0;
    Kokkos::deep_copy(charges,h_charges);
    return charges;
  }

  KOKKOS_INLINE_FUNCTION double mass(Ptcl_num p) const 
  {
    return MASSES[std::abs(p)];
  }

  double hmass(Ptcl_num p) const 
  {
    return H_MASSES[std::abs(p)];
  }

  inline void set_mass(Ptcl_num p, double m) noexcept
  {
    INFO(" -> ", string_from_particle(p), " mass = ", m);
    H_MASSES[std::abs(p)] = m;
    Kokkos::deep_copy(MASSES,H_MASSES);
  }


  KOKKOS_INLINE_FUNCTION double width(Ptcl_num p) const 
  {
    return WIDTHS[std::abs(p)];
  }

  double hwidth(Ptcl_num p) const 
  {
    return H_WIDTHS[std::abs(p)];
  }

  KOKKOS_INLINE_FUNCTION double charge(Ptcl_num p) const
  {
    if (p < 0)
      return -CHARGES[-p];
    else
      return CHARGES[p];
  }

  double hcharge(Ptcl_num p) const
  {
    if (p < 0)
      return -H_CHARGES[-p];
    else
      return H_CHARGES[p];
  }

};

// Query particle properties
bool is_antiparticle(Ptcl_num) noexcept;
KOKKOS_INLINE_FUNCTION bool is_quark(Ptcl_num p) noexcept
{
  const Ptcl_num abs_p {std::abs(p)};
  return (abs_p >= DOWN_QUARK && abs_p <= TOP_QUARK);
}
KOKKOS_INLINE_FUNCTION bool is_massless_quark(Ptcl_num p) noexcept
{
  const Ptcl_num abs_p {std::abs(p)};
  return (abs_p >= DOWN_QUARK && abs_p <= BOTTOM_QUARK);
}
KOKKOS_INLINE_FUNCTION bool is_jet(Ptcl_num p) noexcept
{
  return (is_massless_quark(p) || p == GLUON);
}
KOKKOS_INLINE_FUNCTION bool is_lepton(Ptcl_num p) noexcept
{
  const Ptcl_num abs_p {std::abs(p)};
  return (abs_p >= ELECTRON && abs_p <= TAU_NEUTRINO);
}
KOKKOS_INLINE_FUNCTION bool is_fermion(Ptcl_num p) noexcept
{
  return (is_lepton(p) || is_quark(p));
}
KOKKOS_INLINE_FUNCTION bool is_boson(Ptcl_num p) noexcept
{
  return (p == GLUON || p == PHOTON || p == Z || p == PHOTON_AND_Z);
}
KOKKOS_INLINE_FUNCTION bool is_neutral_gauge_boson(Ptcl_num p) noexcept
{
  return (p == PHOTON || p == Z || p == PHOTON_AND_Z);
}

bool is_colour_charged(Ptcl_num) noexcept;
double n_colour_states(Ptcl_num) noexcept;
double spin(Ptcl_num) noexcept;


// Transform particles
Ptcl_num antiparticle(Ptcl_num) noexcept;
int pdf_id(Ptcl_num) noexcept;

// Translate between particle numbers and string representations
Ptcl_num particle_from_string(const std::string&);
std::string string_from_particle(Ptcl_num);
std::string particles_to_process_spec_n(const std::vector<Ptcl_num>&,int);

#endif
