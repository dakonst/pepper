// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#include "cuts.h"

#include "pepper/config.h"

#include "cuts_kernel.h"
#include "event_handle.h"
#include "kernel_macros.h"
#include "timing.h"

Cuts::Cuts(detail::Cuts_data _data):
    d_data(Kokkos::ViewAllocateWithoutInitializing("cuts_data")),
    h_data(Kokkos::create_mirror_view(d_data)) {
  h_data() = _data;
  Kokkos::deep_copy(d_data,h_data);
}

Cuts::~Cuts()
{
  using namespace Timing;
  register_duration({Task::Event_generation, Task::Phase_space, Task::Cuts},
                    kernel_duration);
}

void Cuts::fill(Event_handle& evt) const
{
  Timer timer;
  const auto& data = d_data;
  RUN_KERNEL(cuts_kernel, evt, data);
  Kokkos::fence(__func__);
  kernel_duration += timer.elapsed();
}
