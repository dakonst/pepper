// This file is part of the portable parton-level event generator Pepper.
// Copyright (C) 2023 The Pepper Collaboration
// Pepper is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic usage guidelines, see GUIDELINES.

#ifndef SCALE_SETTER_KERNEL_H
#define SCALE_SETTER_KERNEL_H


#include "event_handle.h"
#include "kernel_macros.h"

template<typename event_data>
KOKKOS_INLINE_FUNCTION void fixed_scale_setter_kernel(const event_data& evt, int i, double mu2)
{
  evt.mu2(i) = mu2;
}
DEFINE_CUDA_KERNEL_NARGS_1(fixed_scale_setter_kernel, double, mu2)

template<typename event_data>
KOKKOS_INLINE_FUNCTION void htp2_scale_setter_kernel(const event_data& evt, int i)
{
  if (evt.w(i) == 0.0)
    return;
  double h_tp {0.0};
  Vec4 z_mom {0.0, 0.0, 0.0, 0.0};
  for (int p {2}; p < evt.n_ptcl; p++) {
    if (is_lepton(evt.ptcl(p)))
      z_mom += evt.p(i, p);
    else
      h_tp += evt.p(i, p).p_perp();
  }
  h_tp += z_mom.m_perp();
  evt.mu2(i) = h_tp * h_tp;
}
DEFINE_CUDA_KERNEL_NARGS_0(htp2_scale_setter_kernel)

template<typename event_data>
KOKKOS_INLINE_FUNCTION
void htp2_2_scale_setter_kernel(const event_data& evt, int i)
{
  if (evt.w(i) == 0.0)
    return;
  double h_tp {0.0};
  Vec4 z_mom {0.0, 0.0, 0.0, 0.0};
  for (int p {2}; p < evt.n_ptcl; p++) {
    if (is_lepton(evt.ptcl(p)))
      z_mom += evt.p(i, p);
    else
      h_tp += evt.p(i, p).p_perp();
  }
  h_tp += z_mom.m_perp();
  evt.mu2(i) = h_tp * h_tp / 2.0;
}
DEFINE_CUDA_KERNEL_NARGS_0(htp2_2_scale_setter_kernel)

template<typename event_data>
KOKKOS_INLINE_FUNCTION
void ht2_2_scale_setter_kernel(const event_data& evt, int i)
{
  if (evt.w(i) == 0.0)
    return;
  double h_tp {0.0};
  for (int p {2}; p < evt.n_ptcl; p++) {
    if (is_quark(p) || p == GLUON)
      h_tp += evt.p(i, p).p_perp();
  }
  evt.mu2(i) = h_tp * h_tp / 2.0;
}
DEFINE_CUDA_KERNEL_NARGS_0(ht2_2_scale_setter_kernel)

template<typename event_data>
KOKKOS_INLINE_FUNCTION
void htm2_2_scale_setter_kernel(const event_data& evt, int i)
{
  if (evt.w(i) == 0.0)
    return;
  double h_tm {0.0};
  const int n_ptcl {evt.n_ptcl};
  for (int p {2}; p < n_ptcl; p++) {
    h_tm += evt.p(i, p).m_perp();
  }
  evt.mu2(i) = h_tm * h_tm / 2.0;
}
DEFINE_CUDA_KERNEL_NARGS_0(htm2_2_scale_setter_kernel)

#endif
