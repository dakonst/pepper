#include "doctest.h"
#include "src/permutations.h"

void check_permutation(const std::string& proc_str,
                       const std::vector<int>& expected)
{
  const Flavour_process proc {proc_str, sm};
  const Permutations perm {proc};
  DOCTEST_CHECK(perm.get() == expected);
}

DOCTEST_TEST_CASE("Melia permutations")
{
  check_permutation("d u -> d u",
                    {2, 0, 3, 1});
  check_permutation("d d -> d d",
                    {2, 0, 3, 1,
                     2, 1, 3, 0});
  check_permutation("d u -> g d u s sb",
                    {3, 0, 2, 4, 1, 5, 6,
                     3, 0, 2, 5, 6, 4, 1,
                     3, 0, 4, 2, 1, 5, 6,
                     3, 0, 5, 2, 6, 4, 1,
                     3, 0, 4, 1, 2, 5, 6,
                     3, 0, 5, 6, 2, 4, 1,
                     3, 0, 4, 1, 5, 2, 6,
                     3, 0, 5, 6, 4, 2, 1,
                     3, 0, 4, 1, 5, 6, 2,
                     3, 0, 5, 6, 4, 1, 2,
                     3, 0, 2, 4, 5, 6, 1,
                     3, 0, 2, 5, 4, 1, 6,
                     3, 0, 4, 2, 5, 6, 1,
                     3, 0, 5, 2, 4, 1, 6,
                     3, 0, 4, 5, 2, 6, 1,
                     3, 0, 5, 4, 2, 1, 6,
                     3, 0, 4, 5, 6, 2, 1,
                     3, 0, 5, 4, 1, 2, 6,
                     3, 0, 4, 5, 6, 1, 2,
                     3, 0, 5, 4, 1, 6, 2});
  check_permutation("g g -> g g g",
                    {0, 1, 2, 3, 4,
                     0, 1, 3, 2, 4,
                     0, 2, 1, 3, 4,
                     0, 3, 1, 2, 4,
                     0, 2, 3, 1, 4,
                     0, 3, 2, 1, 4});
}
