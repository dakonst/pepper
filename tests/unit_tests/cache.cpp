#include "pepper/config.h"

#include "tests.h"

#include "Kokkos_Core.hpp"
#include "src/generator.h"
#include "src/paths.h"

MC_result generate_result(Settings& s)
{
  Generator gen {s, sm};
  gen.optimise();
  if (!s.unweighting_disabled)
    gen.setup_and_optimise_event_unweighting();
  return gen.generate_batches(s.n_batches);
}

void run_cache_test(Settings& s)
{
  s.process_specs = {"d u -> d u"};

  // event output is not needed for the test, instead we check the MC_result of
  // the event generation
  s.event_output_disabled = true;

  s.n_batches = 1;
  if(Kokkos::device_id() != -1)
    s.batch_size = 9216;
  else
    s.batch_size = 100;


  // run making sure that there is no cache to be read in
  Paths::erase_cache();
  const MC_result result_without_cache {generate_result(s)};

  // run again, this will now use the cache produced previously
  const MC_result result_with_cache {generate_result(s)};

  // make sure there are enough nonzero events available for a meaningful test
  if (result_with_cache.n_nonzero < 5)
    DOCTEST_FAIL("There are not enough non-zero events to test. Increase "
                 "n_batches or batch_size for this test case.");

  DOCTEST_CHECK(result_without_cache.sum ==
                doctest::Approx(result_with_cache.sum));
  DOCTEST_CHECK(result_without_cache.sum2 ==
                doctest::Approx(result_with_cache.sum2));
  DOCTEST_CHECK_EQ(result_without_cache.n_trials, result_with_cache.n_trials);
  DOCTEST_CHECK_EQ(result_without_cache.n_nonzero_after_cuts,
                   result_with_cache.n_nonzero_after_cuts);
  DOCTEST_CHECK_EQ(result_without_cache.n_nonzero, result_with_cache.n_nonzero);
}

// make sure that the event generation result are the same, regardless of
// whether there are cached results available or not (using Rambo)
DOCTEST_TEST_CASE("cache test (Rambo)")
{
  Settings s {settings};
  s.phase_space_settings.generator = "Rambo";
  // Rambo's unweighting efficiency is terrible, so let's just disable it
  s.unweighting_disabled = true;
  // Rambo does not optimise in any case, so disable it to save time
  s.phase_space_settings.optimisation.n_iter = 0;
  // Rambo is not compatible with PDF usage
  s.pdf_disabled = true;
  run_cache_test(s);
}

#if Chili_FOUND
// make sure that the event generation result are the same, regardless of
// whether there are cached results available or not (using Chili)
DOCTEST_TEST_CASE("cache test (Chili)")
{
  Settings s {settings};
  s.phase_space_settings.generator = "Chili";
  s.phase_space_settings.optimisation.n_iter = 3;
  run_cache_test(s);
}
#endif

// make sure that the event generation result are the same, regardless of
// whether there are cached results available or not (using Chili(basic))
DOCTEST_TEST_CASE("cache test (Chili(basic))")
{
  Settings s {settings};
  s.phase_space_settings.generator = "Chili(basic)";
  s.phase_space_settings.optimisation.n_iter = 3;
  run_cache_test(s);
}
