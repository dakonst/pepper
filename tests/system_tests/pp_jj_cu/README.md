This test is equivalent to the `pp_jj` one, but uses higher values for
`n_nonzero_min` that seem to make more sense when using the higher `batch_size`
on the GPU. Note that this is not completely understood yet.  The fact that we
do not pass the test with less statistics seems to indicate that our errors
seem to be under-estimated ...
